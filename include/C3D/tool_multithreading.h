﻿////////////////////////////////////////////////////////////////////////////////
/**
\file
\brief \ru Управление параллельной обработкой данных.
       \en Managing of parallel data processing. \~
\details \ru Управление параллельной обработкой данных.\n
        \en Managing of parallel data processing. \n \~
*/
////////////////////////////////////////////////////////////////////////////////
#ifndef __TOOL_MULTITHREADING_H
#define __TOOL_MULTITHREADING_H

#include <system_dependency.h>
#include <tool_mutex.h>

//------------------------------------------------------------------------------
/**
\brief \ru Режимы многопоточных вычислений.
       \en Multithreading modes. \~
\details \ru Режимы многопоточных вычислений. \n
         \en Multithreading modes. \n \~
\ingroup Data_Structures
*/
//---
enum MbeMultithreadedMode {
  // \ru Многопоточность ядра отключена. \en Kernel multithreading is off. 
  mtm_Off         = 0,
  // \ru Включена многопоточность ядра при обработке независимых объектов (без общих данных).
  // \en Kernel multithreading is ON for independent objects (without common data).
  mtm_Standard    = 1,
  // \ru Обеспечивается потокобезопасность объектов типа MbItem. Выключена многопоточность ядра при обработке объектов, имеющих общие данные.
  // \en Ensured thread-safety of objects MbItem. Kernel multithreading is OFF for objects with shared data.
  mtm_SafeItems   = 2,
  // \ru Обеспечивается потокобезопасность объектов типа MbItem. Включена многопоточность ядра при обработке объектов с общими данными.
  // \en Ensured thread-safety of objects MbItem. Kernel multithreading is ON for objects with shared data.
  mtm_Items       = 3,
  // \ru Включена максимальная многопоточность ядра. \en Maximal kernel multithreading is ON.
  mtm_Max         = 31
};

//------------------------------------------------------------------------------
/**
  \brief \ru Базовый класс для объектов, требующих сборки мусора.
         \en Base class for objects which require a garbage collection. \~
\details \ru Базовый класс для объектов, требующих сборки мусора.
         \en Base class for objects which require a garbage collection. \~
\ingroup Base_Tools
*/
//---
class MATH_CLASS CacheCleaner
{
public:
  CacheCleaner();
  ~CacheCleaner();

  /** \brief \ru Очистить кэшированные данные. Возвращает true, если объект был отписан от сборки мусора.
             \en Reset cached data. Return true if the object was unsubscribed from garbage collection.\~
  */
  virtual bool ResetCacheData() = 0;

  /** \brief \ru Подписаться на сборку мусора.
             \en Subscribe for garbage collection. \~
  */
  void SubcribeOnCleaning();

  /** \brief \ru Отписаться от сборки мусора.
             \en Unsubscribe from garbage collection. \~
  */
  void UnsubcribeOnCleaning();
};

//------------------------------------------------------------------------------
/**
  \brief \ru Сборщик мусора для объектов, использующих кэширование данных.
         \en Garbage collector for objects which use data caching. \~
\details \ru Сборщик мусора. По требованию очищает кэши зарегистрированных объектов CacheCleaner. \n
         \en Garbage collector. At request clears caches of registered CacheCleaner objects. \n \~
\ingroup Base_Tools
*/
//---
class MATH_CLASS MbGarbageCollection
{
public:
  /** \brief \ru Подписать объект на сборку мусора.
             \en Subscribe the object for garbage collection. \~
  */
  static void Subscribe( CacheCleaner * obj );

  /** \brief \ru Отписать объект от сборки мусора.
             \en Unsubscribe the object from garbage collection. \~
  */
  static void Unsubscribe( CacheCleaner * obj );

  /** \brief \ru Выполнить сборку мусора.
             Должна вызываться в последовательном участке кода. При вызове в параллельном регионе ничего не делает.
             \en Perform garbage collection.
             Should be called in sequential code. When called in a parallel region, does nothing. \~
  */
  static void Run();
};

//------------------------------------------------------------------------------
/**
\brief \ru Родительский класс данных для менеджера параллельной обработки.
       \en Parent class of data for manager of parallel processing. \~
\details \ru Родительский класс для данных, которые могут обрабатываться параллельно
         с помощью менеджера кэшей.
         \en Parent class for data which could be processed in parallel using the cache manager. \~
  \ingroup Base_Tools
*/
// ---
class AuxiliaryData {
public:
  AuxiliaryData() {}
  AuxiliaryData( const AuxiliaryData & ) {}
  virtual ~AuxiliaryData() {}
};


//------------------------------------------------------------------------------
/**
\brief \ru Менеджер параллельной обработки данных (менеджер кэшей).
       \en Manager for parallel data processing (the cache manager). \~
\details \ru Менеджер кэшей представляет шаблон, содержащий:
       longTerm - данные, которые используются при последовательном выполнении и
       для инициализации данных, обрабатываемых в других потоках;
       tcache - данные для потоков, которые используются при параллельном выполнении.
       Каждый поток по идентификатору threadKey использует только свою копию данных.
       Для многопоточной обработки зависимых (имеющих общие данные) объектов должен
       использоваться режим многопоточных вычислений mtm_Items. \n
       \en The cache manager is a template which contains:
       longTerm - data used in sequential execution and for initialization of thread data;
       tcache - data for threads which is used in parallel execution. 
       Each thread uses its own copy of data according to threadKey.
       For multithreaded processing of dependent (with shared data) objects,
       the mtm_Items multithreading mode should be used. \n \~
*/
// ---
template<class T>
class CacheManager : public CacheCleaner {
  struct List
  {
    unsigned int _id;
    T*           _data;
    List*        _next;
    List( unsigned int id, T* data ) :
      _id( id ),
      _data( data != NULL ? data : new T() ), // Always: _data != NULL
      _next( NULL ) {}
    ~List() { delete _data; }
  private:
    List() : _id( 0 ), _data( NULL ), _next( NULL ) {}
  };

private:
  T*            longTerm;
  List*         tcache;
  CommonMutex*  lock;

public:
  CacheManager();
  CacheManager( const CacheManager & );
  ~CacheManager();

  /** \brief \ru Получить указатель на кэш (данные) текущего потока. Всегда возвращает ненулевое значение.
      \en Get a pointer to the cache (data) of the current thread. Always returns non-null value. \~
  */
  T *   Cache    ();

  /** \brief \ru Оператор (). Возвращает указатель на кэш (данные) текущего потока. Всегда возвращает ненулевое значение.
      \en Operator (). Returns a pointer to the cache (data) of the current thread. Always returns non-null value. \~
  */
  T *   operator ()();

  /** \brief \ru Удалить кэши. Если resetLongTerm==true, также удалить кэш главного потока.
             \en Delete caches. If resetLongTerm==true, also delete the main thread cache. \~
  */
  void  Reset    ( bool resetLongTerm = false );

  /** \brief \ru Получить указатель на кэш (данные) главного потока. Всегда возвращает ненулевое значение.
                 Все операции с кэшем главного потока должны быть защищены блокировкой кэша.
             \en Get a pointer to cache (data) of the main thread. Always returns non-null value.
                 All operations with the main thread cache should be protected by the cache lock. \~
  */
  T *   LongTerm ();

  /** \brief \ru Получить указатель на блокировку для операций с кэшем главного потока, учитывая, исполняется ли код параллельно
             Может возвращать нулевое значение (удобно для использования с ScopedLock).
             \en Get a pointer to the lock for operations with the main thread cache, considering whether the code runs in parallel.
             Can return null value (good for use with ScopedLock). \~
  */
  CommonMutex* GetLock() { if ( IsInParallel() ) return GetLockHard();  return lock; }
  
  /** \brief \ru Получить указатель на блокировку для операций с кэшем главного потока. Всегда возвращает ненулевое значение.
  \en Get a pointer to the lock for operations with the main thread cache. Always returns non-null value. \~
  */
  CommonMutex* GetLockHard() { if ( lock == NULL ) { GetGlobalLock()->lock(); if ( lock == NULL ) lock = new CommonMutex(); GetGlobalLock()->unlock(); } return lock; }

  /** \brief \ru Функция очистки, используемая сборщиком мусора.
             \en Cleaning function, used by the garbage collector. \~
  */
  virtual bool ResetCacheData() { Reset(); return true; }

private:
  CacheManager & operator = ( const CacheManager & );
};


//------------------------------------------------------------------------------
// \ru Конструктор. \en Constructor. 
// ---
template<class T>
inline CacheManager<T>::CacheManager()
  : longTerm  ( NULL )
  , tcache    ( NULL ) 
  , lock      ( NULL )
{
}


#define C3D_NULLKEY 0

//------------------------------------------------------------------------------
// \ru Конструктор. \en Constructor. 
// ---
template<class T>
inline CacheManager<T>::CacheManager( const CacheManager<T> & item )
  : longTerm  ( item.longTerm ? new T( *item.longTerm ) : NULL )
  , tcache    ( NULL ) 
  , lock      ( NULL )
{
}


//------------------------------------------------------------------------------
// \ru Деструктор. \en Destructor. 
// ---
template<class T>
inline CacheManager<T>::~CacheManager()
{ 
  Reset( true ); 
  if ( lock != NULL )
    delete lock;
  lock = NULL;
}

//------------------------------------------------------------------------------
// \ru Получить указатель на кэш главного потока. Всегда возвращает ненулевое значение.
//     Все операции с кэшем главного потока должны быть защищены блокировкой кэша.
// \en Get a pointer to the main thread cache. Always returns non-null value.
//     All operations with the main thread cache should be protected by the cache lock.
// ---
template<class T>
inline T* CacheManager<T>::LongTerm ()
{
  if ( longTerm == NULL )
    longTerm = new T();
  return longTerm;
}

//------------------------------------------------------------------------------
// \ru Получить указатель на кэш текущего потока. Всегда возвращает ненулевое значение.
// \en Get a pointer to the current thread cache. Always returns non-null value.
// ---
template<class T>
inline T * CacheManager<T>::Cache()
{
#define INIT_BY_LONGTERM ( longTerm != NULL ? new T( *longTerm ) : new T() )

  if ( !IsSafeMultithreading()
    || !IsInParallel()
    ) {
    if ( longTerm == NULL ) {
      longTerm = new T();
    }
    Reset( false ); // \ru Оптимизация памяти (удалить неиспользуемые кэши). \en Memory optimization (delete unused caches).
    return longTerm;
  }

  T * res = NULL;
  unsigned int threadKey = GetThreadKey();
  if ( tcache == NULL )
    SubcribeOnCleaning(); // \ru Подписаться на сборку мусора, так как используются многопоточные кэши. \en Subscribe on garbage collection because using multithreaded caches.

  {
    ScopedLock sl( GetLock() );

    if ( tcache == NULL ) 
    {
      tcache = new List( threadKey, INIT_BY_LONGTERM );
      res = tcache->_data;
    }

    if ( res == NULL ) {
      List* entry = tcache;
      while( entry != NULL ) {
        if( entry->_id == threadKey )
        {
          res = entry->_data;
          break;
        }
        // \ru Если кэш не найден в списке, 'entry' содержит последний (на данный момент) элемент в списке.
        // \en If cache not found in the list, 'entry' contains the last element in the list (at that point).
        if ( entry->_next == NULL)
          break; 
        entry = entry->_next;
      }
      if ( res == NULL ) {
          entry->_next = new List( threadKey, INIT_BY_LONGTERM );
          res = entry->_next->_data;
      }
    }
  } // ScopedLock

  return res;

}


//------------------------------------------------------------------------------
// \ru Оператор (). Возвращает указатель на кэш текущего потока (всегда ненулевое значение).
// \en Operator (). Returns a pointer to the current thread cache (always non-null value).
// ---
template<class T>
inline T * CacheManager<T>::operator()() 
{
  return Cache();
}


//------------------------------------------------------------------------------
// \ru Удалить кэши. Если resetLongTerm==true, удалить также кэш главного потока.
// \en Delete caches. If resetLongTerm==true, also delete the main thread cache.
// ---
template<class T>
inline void CacheManager<T>::Reset( bool resetLongTerm )
{
  if ( resetLongTerm && longTerm != NULL ) {
    ScopedLock sl( GetLock() );
    if ( longTerm != NULL )
      delete longTerm;
    longTerm = NULL;
  }
  if ( tcache != NULL ) {
    ScopedLock sl( GetLock() );
    List* entry = tcache;
    while ( entry != NULL ) {
      tcache = entry->_next;
      delete entry;
      entry = tcache;
    }
    tcache = NULL;
  }
  UnsubcribeOnCleaning(); // \ru Отписаться от сборки мусора, так как кэши удалены. \en Unsubscribe from garbage collection because caches are reset.
}

#endif // __TOOL_MULTITHREADING_H
