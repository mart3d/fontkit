﻿////////////////////////////////////////////////////////////////////////////////
/** 
  \file
  \brief \ru Методы построения полигональных геометрических объектов.
         \en Functions for construction of the polygonal geometric object. \~
  \details \ru Полигональные геометрические объекты могут быть построены по набору точек или на базе других объектов.
           \en Polygonal geometric objects can be constructed using a set of point or on the basis of other objects. \~ 

*/
////////////////////////////////////////////////////////////////////////////////

#ifndef __ACTION_MESH_H
#define __ACTION_MESH_H


#include <mb_cart_point.h>
#include <mb_cart_point3d.h>
#include <mesh.h>
#include <mb_enum.h>
#include <mb_operation_result.h>
#include <m2b_mesh_curvature.h>
#include <vector>


class  MATH_CLASS  MbPlacement3D;
class  MATH_CLASS  MbMesh;
class  MATH_CLASS  MbCurve3D;
class  MATH_CLASS  MbSurface;
class  MATH_CLASS  MbSolid;
class  MATH_CLASS  MbPlaneItem;
class  MATH_CLASS  MbSNameMaker;
class  MATH_CLASS  MbFace;
class  MATH_CLASS  MbCollection;


//------------------------------------------------------------------------------
  /** \brief \ru Расчет полигона кривой.
             \en Calculation of polygon of curve. \~
  \details \ru Расчет трехмерного полигона двумерной кривой в плоскости XOY локальная системы координат.
           \en Calculation of three-dimensional polygon of two-dimensional curve located in the XOY-plane of a local coordinate system. \~
  \param[in] curve - \ru Двумерная кривая.
                     \en A two-dimensional curve. \~
  \param[in] plane - \ru Локальная система координат.
                     \en Local coordinate system. \~
  \param[in] sag - \ru Максимальное допустимое отклонение полигона от оригинала по прогибу или по углу между соседними элементами.
                   \en Maximum allowable deviation of polygon from the original by sag or by angle between neighboring elements. \~
  \param[out] polygon - \ru Рассчитанный полигон.
                        \en Calculated polygon. \~
  \ingroup Algorithms_3D
*/
// ---
MATH_FUNC (void) CalculatePolygon( const MbCurve & curve, 
                                   const MbPlacement3D & plane,
                                         double sag, 
                                         MbPolygon3D & polygon );


//------------------------------------------------------------------------------
/** \brief \ru Построить полигональный двухмерный объект.
           \en Create a polygonal two-dimensional object. \~
  \details \ru Построить полигональный объект для двумерного объекта в плоскости XOY 
           локальной системы координат.
           \en Create a polygonal object for two-dimensional object in the XOY-plane 
           of the local coordinate system. \~
  \param[in] obj - \ru Двумерный объект (если NULL, то объект не создаётся).
                   \en Two-dimensional object (if NULL, object isn't created). \~
  \param[in] plane - \ru Локальная система координат.
                     \en A local coordinate system. \~
  \param[in] sag - \ru Максимальное отклонение полигонального объекта от оригинала по прогибу.
                   \en The maximum deviation of polygonal object from the original object by sag. \~
  \param[out] mesh - \ru Полигональный объект.
                     \en Polygonal object. \~
  \ingroup Polygonal_Objects
*/
// ---
MATH_FUNC (void) CalculateWire( const MbPlaneItem & obj, 
                                const MbPlacement3D & plane, 
                                      double sag, 
                                      MbMesh & mesh );


//------------------------------------------------------------------------------
/** \brief \ru Построить икосаэдр в виде полигональной модели.
           \en Construct an icosahedron mesh. \~
  \details \ru Построить икосаэдр в виде полигональной модели. \n
           \en Construct an icosahedron mesh. \n \~
  \param[in] place - \ru Местная система координат.
                     \en Local placement. \~
  \param[in] radius - \ru Радиус описанной сферы.
                      \en The radius of the sphere. \~
  \param[in] fn - \ru Способ построения полигонального объекта.
                  \en Way for polygonal object constructing. \~
  \param[out] result - \ru Результат построения.
                       \en The resulting mesh. \~
  \return  \ru Возвращает код результата операции.
           \en Returns operation result code. \~
  \ingroup Polygonal_Objects
*/
// ---
MATH_FUNC (MbResultType) CreateIcosahedron( const MbPlacement3D & place, 
                                                  double radius, 
                                                  const MbFormNote & fn,
                                                  MbMesh *& result );


//------------------------------------------------------------------------------
// .
/** \brief \ru Построить полигональную сферу.
           \en Construct an spherical mesh. \~
  \details \ru Построить аппроксимацию сферы выпуклым многогранником. \n
           \en Construct an approximation of the sphere by a convex polyhedron. \n \~
  \param[in] place - \ru Местная система координат.
                     \en Local placement. \~
  \param[in] radius - \ru Радиус сферы.
                      \en The radius of the sphere. \~
  \param[in] epsilon - \ru Параметр аппроксимации сферы.
                       \en The approximation parameter. \~
  \param[out] result - \ru Результат построения.
                       \en The resulting mesh. \~
  \return  \ru Возвращает код результата операции.
           \en Returns operation result code. \~
  \ingroup Polygonal_Objects
*/
// ---
MATH_FUNC (MbResultType) CreateSpherePolyhedron( const MbPlacement3D & place, 
                                                       double radius, 
                                                       double & epsilon, 
                                                       MbMesh *& result );


//------------------------------------------------------------------------------
/** \brief \ru Вычислить выпуклую оболочку для множества точек.
           \en Calculate a convex hull of a point set. \~
  \details \ru Вычислить сетку, представляющую выпуклой оболочку для множества точек.
           \en Calculate mesh being a convex hull of a point set. \~
  \return  \ru Возвращает код результата операции.
           \en Returns operation result code. \~
  \ingroup Polygonal_Objects
*/
// ---
MATH_FUNC (MbResultType) CreateConvexPolyhedron( const SArray<MbFloatPoint3D> & points,
                                                       MbMesh *& result );


//------------------------------------------------------------------------------
/** \brief \ru Построить выпуклую оболочку для триангуляционной сетки.
           \en Construct the convex hull of triangulation grid. \~
  \details \ru Построить сетку, представляющую собой выпуклую оболочку для тела,
    заданного его триангуляционной сеткой. По заданному объекту MbMesh
    строится охватывающая его вершины выпуклая триангуляционная сетка.
    Расстояние offset задает смещение точек результирующей сетки относительно
    заданной вдоль нормалей к её граням. Если offset = 0, то результирующая сетка
    будет в точности охватывать все вершины заданной. Смещение по нормали может
    быть как положительным, так и отрицательным (внутрь сетки). Используется для
    определения пересечения с некоторым допуском (offset). \n
           \en Construct the convex hull of triangulation grid. \n \~
  \param[in] mesh - \ru Исходная триангуляционная сетка.
                    \en Initial triangulated mesh. \~
  \param[in] offset - \ru Отступ по нормали для результирующей сетки.
                      \en The offset along a normal for the resulting grid. \~
  \param[out] resMesh - \ru Результирующая выпуклая триангуляционная сетка.
                        \en The resulting triangulation convex grid. \~
  \return  \ru Возвращает код результата операции.
           \en Returns operation result code. \~
  \ingroup Polygonal_Objects
*/
// ---
MATH_FUNC (MbResultType) CreateConvexPolyhedron( const MbMesh &  mesh, 
                                                       double    offset,
                                                       MbMesh *& result );


//------------------------------------------------------------------------------
/** \brief \ru Определить, пересекаются ли данные выпуклые сетки.
           \en Whether there is intersection of convex grids. \~
  \details \ru Определить, пересекаются ли данные выпуклые оболочки, заданные
    триангуляционными сетками. Пересечение определяется по алгоритму
    Гильберта-Джонсона-Керти (Gilbert-Johnson-Keerthi). Заданные сетки
    равноправны, их последовательность в алгоритме не важна. Сложность
    алгоритма линейная, зависит от количества вершин сеток. \n
           \en Whether there is intersection of convex grids. \n \~
  \param[in] mesh1 - \ru Первая выпуклая триангуляционная сетка.
                     \en The first convex grid. \~
  \param[in] mesh2 - \ru Вторая выпуклая триангуляционная сетка.
                     \en The second convex grid. \~
  \return \ru true - Выпуклые триангуляционные сетки пересекаются.
              false - Выпуклые триангуляционные сетки не пересекаются.
          \en true - true - there is an intersection, 
              false - there are no intersections. \~
  \ingroup Polygonal_Objects
*/
// ---
MATH_FUNC (bool) AreIntersectConvexPolyhedrons( const MbMesh & mesh1, 
                                                const MbMesh & mesh2 );


//------------------------------------------------------------------------------
/** \brief \ru Отрезать часть полигонального объекта плоскостью.
           \en Cut a part of a polygonal object by a plane. \~
  \details \ru Отрезать часть полигонального объекта плоскостью XY локальной системы координат. \n
    part =  1 - оставляем часть объекта, расположенную сверху плоскости XY локальной системы координат, \n
    part = -1 - оставляем часть объекта, расположенную снизу  плоскости XY локальной системы координат. \n
           \en Cut a part of a polygonal object off by a plane XY of local coordinate system. \n
    part =  1 - a part of polygonal object above the XY plane is to be retained. \n
    part = -1 - a part of polygonal object below the XY plane is to be retained. \n \~
  \param[in] solid - \ru Исходный полигональный объект.
                     \en The source polygonal object. \~
  \param[in] sameShell - \ru Режим копирования исходного объекта.
                         \en The mode of copying of the source polygonal object. \~
  \param[in] surface - \ru Секущая плоскость.
                       \en A cutting plane. \~
  \param[in] part - \ru Направление отсечения.
                    \en The direction of cutting off. \~
  \param[in] names - \ru Именователь.
                     \en An object for naming the new objects. \~
  \param[in] closed - \ru Флаг режима отсечения: true - сечем как тело, false - сечем как оболочку.
                      \en The flag of the cutting off mode: true - cut as a solid, false - cut as a shell. \~
  \param[out] result - \ru Построенный полигональный объект.
                       \en The resultant polygonal object. \~
  \return \ru Возвращает код результата операции.
          \en Returns operation result code. \~
  \ingroup Polygonal_Objects
*/
// ---
MATH_FUNC (MbResultType) MeshCutting(        MbMesh &        mesh, 
                                             MbeCopyMode     sameShell,
                                       const MbPlacement3D & place, 
                                             int             part, 
                                       const MbSNameMaker &  names,
                                             bool            closed, 
                                             MbMesh *&       result );


//------------------------------------------------------------------------------
/** \brief \ru Построить триангуляцию по облаку точек на основе алгоритма поворотного шара.
            \en Build a triangulation by point cloud with Ball Pivoting algorithm. \~
  \param[in]  collection - \ru Коллекция трехмерных элементов.
                           \en Collection of 3d elements. \~
  \param[in]  radius     - \ru Радиус поворотного шара, если radius==0 будет предпринята попытка его автоопределения.
                           \en Radius of the pivoting ball, if radius==0 an autoguess for the ball pivoting radius is attempted \~
  \param[in]  radiusMin  - \ru Радиус кластеризации ( в % от радиуса поворотного шара ).
                           \en Clusterization radius ( % from radius value). \~
  \param[in]  angle      - \ru Максимальный угол между двумя соседними элементами сетки.
                           \en Max angle between two mesh faces \~
  \param[out] result -     \ru Построенный полигональный объект.
                           \en The resultant polygonal object. \~
  \return \ru Возвращает код результата операции.
          \en Returns operation result code. \~
  \ingroup Polygonal_Objects
*/
// ---
MATH_FUNC (MbResultType) CalculateBallPivotingGrid( const MbCollection & collection, 
                                                          double         radius,
                                                          double         radiusMin,
                                                          double         angle,
                                                          MbMesh *&      result );


//------------------------------------------------------------------------------
/** \brief \ru Класс для создания тела в граничном представлении по полигональной сетке.
           \en Class for creating a BRep solid by polygonal mesh. \~
  \details \ru Предоставить интерфейс для управления преобразованием сетки в
               оболочку в граничном представлении. \n
           \en Provide an interface for managing of "mesh to Brep" conversion. \n \~
  \ingroup Polygonal_Objects
*/
// ---
class MATH_CLASS MbMeshProcessor : public MbRefItem
{
protected:
  /// \ru Конструктор. \en Constructor. 
  MbMeshProcessor();

public:
  /** \brief \ru Создать экземпляр процессора по коллекции.
             \en Create mesh processor by collection. \~
      \details \ru Создать экземпляр процессора по коллекции. Пользователь должен сам удалить объект.
               \en Create mesh processor by collection. User must delete created object. \~
      \param[in] collection - \ru Входная коллекция, содержащая треугольную сетку. \n
                              \en Input collection containing triangle mesh. \~
      \return \ru Возвращает указатель на созданный объект.
              \en Returns pointer to created object. \~
    \ingroup Polygonal_Objects
  */
  static MbMeshProcessor * Create( const MbCollection & collection );

  /// \ru Деструктор. \en Destructor. 
  virtual ~MbMeshProcessor();

  /** \brief \ru Установить относительную точность.
             \en Set relative tolerance. \~
      \details \ru Установить относительную точность по габаритам текущей сетки.
               \en Set relative tolerance by current mesh box. \~
      \param[in] tolerance - \ru Относительная точность. \n
                             \en Relative tolerance to set. \~
    \ingroup Polygonal_Objects
  */
  virtual void SetRelativeTolerance( double tolerance ) = 0;

  /** \brief \ru Установить точность.
             \en Set tolerance. \~
      \details \ru Установить точность распознавания поверхностей и расширения сегментов сетки.
                   Метод должен быть вызван перед вызовом SegmentMesh.
                   Точность по умолчанию равна 0.1.
               \en Set tolerance of surface reconstruction and segments extension.
                   Method should be called before call to SegmentMesh.
                   Default tolerance is 0.1. \n \~
      \param[in] tolerance - \ru Точность. \n
                             \en Tolerance to set. \~
    \ingroup Polygonal_Objects
  */
  virtual void SetTolerance( double tolerance ) = 0;

  /** \brief \ru Получить точность.
             \en Get tolerance. \~
      \details \ru Получить текущую точность, используемую при распознавании поверхностей и расширения сегментов сетки.
               \en Get current tolerance used in surface reconstruction and segments extension. \~
      \return \ru Возвращает абсолютную точность.
              \en Returns absolute tolerance. \~
    \ingroup Polygonal_Objects
  */
  virtual double GetTolerance() const = 0;

  /** \brief \ru Получить исправленную (упрощенную) копию входной полигональной сетки.
             \en Get fixed (simplified) copy of the input mesh. \~
      \details \ru Получить исправленную копию входной сетки, на которой выполняются операции MbMeshProcessor:
                   подсчет кривизн, сегментация, построение оболочки. Все индексы в выходных данных соответствуют
                   индексам вершин и треугольников упрощенной сетки, возвращаемой данным методом. \n
               \en Get fixed copy of the input mesh. All further operations of MbMehsProcessor are
                   performed for simplified mesh: curvature calculation, segmentation, shell creation.
                   All indices in the output of these operations corresponds to indices of vertices and
                   triangles of the simplified mesh returned from this function. \n \~
      \return \ru Возвращает исправленную версию входной полигональной сетки.
              \en Returns a fixed version of the input mesh. \~
    \ingroup Polygonal_Objects
  */
  virtual const MbCollection & GetSimplifiedMesh() = 0;

  /** \brief \ru Получить сегментированную копию входной полигональной сетки.
             \en Get segmented copy of the input mesh. \~
      \details \ru Получить сегменитрованную копию входной сетки, на которой выполняются операции MbMeshProcessor:
                   подсчет кривизн, сегментация, построение оболочки.
                   Сегментация доступна внутри коллекции. \n
               \en Get segmented copy of the input mesh. All further operations of MbMehsProcessor are
                   performed for simplified mesh: curvature calculation, segmentation, shell creation.
                   Segmentation is stored inside collection. \n \~
      \return \ru Возвращает сегментированную версию входной полигональной сетки.
              \en Returns a segmented version of the input mesh. \~
    \ingroup Polygonal_Objects
  */
  virtual const MbCollection & GetSegmentedMesh() = 0;

  /** \brief \ru Рассчитать главные кривизны и главные направления изменения кривизн в точках сетки.
             \en Calculate the principal curvatures and principal curvature directions at mesh points. \~
    \details \ru Рассчитать главные кривизны и главные направления изменения кривизн в точках сетки. \n
             \en Calculate the principal curvatures and principal curvature directions at mesh points. \n \~
    \return \ru Возвращает главные кривизны и главные направления в точках сетки.
            \en Returns principal curvatures and principal curvature directions at mesh points. \~
    \ingroup Polygonal_Objects
  */
  virtual const std::vector<MbCurvature> & CalculateCurvatures() = 0;

  /** \brief \ru Сегментровать полигональную сетку.
             \en Segment a polygonal mesh. \~
    \details \ru Выполнить сегментацию полигональной сетки. \n
             \en Perform segmentation of a polygonal mesh. \n \~
    \return  \ru Возвращает код результата операции.
             \en Returns operation result code. \~
    \param[in] createSurfaces - \ru Создавать ли поверхности на сегментах.
                                \en Create surfaces on segments or not. \~
    \ingroup Polygonal_Objects
  */
  virtual MbResultType SegmentMesh( bool createSurfaces = true ) = 0;

  /** \brief \ru Создать оболочку.
             \en Create shell. \~
    \details \ru Создать оболочку в граничном представлении, соответствующее модели, заданной полигональной сеткой.
                 Используется текущая сегментация.
                 Если сегментация не была вычислена, но вычисляется автоматическая сегментация (с параметрами по умолчанию). \n
             \en Create BRep shell that represents input mesh model.
                 Current segmentation is used.
                 If segmentation is not computed yet, then automatic segmentation is performed (with default paramters). \n \~
    \param[out] pShell - \ru Указатель на созданную оболочку.
                          \en The pointer to created shell. \~
    \return  \ru Возвращает код результата операции.
             \en Returns operation result code. \~
    \ingroup Polygonal_Objects
  */
  virtual MbResultType CreateBRepShell( MbFaceShell *& pShell ) = 0;

  /** \brief \ru Вписать поверхность.
             \en Fit surface to segment . \~
    \details \ru Распознать поверхность по сегменту сетки с заданным индексом.
                 Распознанная поверхность может быть получена с помощью метода GetSegmentSurface. \n
             \en Recognize surface for mesh segment with a given index.
                 Recognized surface is available through GetSegmentSurface method. \n \~
    \param[in] idxSegment - \ru Индекс сегмента полигональной сетки.
                            \en Index of a mesh segment. \~
    \ingroup Polygonal_Objects
  */
  virtual void FitSurfaceToSegment( size_t idxSegment ) = 0;

  /** \brief \ru Вписать поверхность заданного типа.
             \en Fit surface of a given type to a segment. \~
    \details \ru Построить поверхность заданного типа, аппроксимирующиую сегмент сетки с заданным индексом.
                 Распознанная поверхность может быть получена с помощью метода GetSegmentSurface. \n
             \en Find surface of a given type approximating mesh segment with a given index.
                 Recognized surface is available through GetSegmentSurface method. \n \~
    \param[in] idxSegment  - \ru Индекс сегмента полигональной сетки.
                             \en Index of a mesh segment. \~
    \param[in] surfaceType - \ru Тип вписываемой поверхности.
                             \en Type of fitted surface. \~
    \ingroup Polygonal_Objects
  */
  virtual void FitSurfaceToSegment( size_t idxSegment, MbeSpaceType surfaceType ) = 0;

  /** \brief \ru Получить поверхность для сегмента.
             \en Get surface of segment. \~
    \details \ru Получить поверхность, вписанную в сегмент.
                 Чтобы поверхность была определена предварительно должны быть вызваны методы
                 SegmentMesh или FitSurfaceToSegment.
                 Распознанная поверхность с помощью метода GetSegmentSurface. \n
             \en Get surface that approximates segment.
                 To fit surface use corresponding methods SegmentMesh or FitSurfaceToSegment. \n \~
    \param[in] idxSegment - \ru Индекс сегмента полигональной сетки.
                            \en Index of a mesh segment. \~
    \return  \ru Возвращает указатель на поверхность для сегмента, если поверхность определена, иначе - NULL.
             \en Returns pointer to segment surface if it exists, else - NULL. \~
    \ingroup Polygonal_Objects
  */
  virtual const MbSurface * GetSegmentSurface( size_t idxSegment ) const = 0;

  /** \brief \ru Очистить сегментацию полигональной сетки.
             \en Reset segmentation of the polygonal mesh. \~
    \details \ru Очистить сегментацию полигональной сетки, хранящуюся внутри MbMeshProcessor. \n
             \en Reset segmentation of the polygonal mesh stored inside MbMeshProcessor. \n \~
    \ingroup Polygonal_Objects
  */
  virtual void ResetSegmentation() = 0;

  /** \brief \ru Найти ближайший путь между двумя вершинами коллекции.
             \en Find shortest path between two vertices. \~
    \details \ru Найти ближайший путь, проходящий по вершинам и ребрам коллекции, соединяющий две заданные вершины. \n
             \en Find shortest path between two vertices. The path should pass through collection vertices and edges. \n \~
    \param[in]  v1   - \ru Индекс первой вершины.
                       \en The index of first vertex. \~
    \param[in]  v2   - \ru Индекс второй вершины.
                       \en The index of second vertex. \~
    \param[out] path - \ru Путь из первой вершины во вторую.
                           Массив содержит последовательные индексы всех вершин пути.
                       \en The path from the first vertex to the second one.
                           The array contains successive indices of path vertices. \~
    \return  \ru Возвращает код результата операции.
             \en Returns operation result code. \~
    \ingroup Polygonal_Objects
  */
  virtual bool FindShortestVertexPath( uint v1, uint v2, std::vector<uint> & path ) = 0;

private: // UNDER DEVELOPMENT
  /** \} */
  /** \ru \name Функции для работы с разбиением сетки на сегменты и распознаванием поверхностей для сегментов.
      \en \name Functions for editting of mesh segmentation and reconstruction of surfaces for the segments.
      \{ */

  /** \brief \ru Объединить два сегмента в текущей сегментации.
             \en Unite two segments in current segmentation. \~
    \details \ru Объединение сегментов в текущей сегментации.
                 Результат объединения доступен через коллекцию, возвращаемую методом GetSegmentedMesh. \n
             \en Union of segments in current mesh segmentation.
                 Result segmentation is available through collection returned by GetSegmentedMesh. \n \~
    \param[in] firstSegmentIdx - \ru Индекс первого сегмента для объединения. \n
                                 \en Index of the first segment for union. \~
    \param[in] secondSegmentIdx - \ru Индекс второго сегмента для объединения. \n
                                  \en Index of the second segment for union. \~
    \ingroup Polygonal_Objects
  */
  virtual void UniteSegments( size_t firstSegmentIdx, size_t secondSegmentIdx ) = 0;

  /** \brief \ru Сегментровать полигональную сетку по разделителям сегментов.
             \en Segment a polygonal mesh by segment separators. \~
    \details \ru Выполнить сегментацию полигональной сетки по заданным разделителям сегментов. \n
             \en Perform segmentation of a polygonal mesh by segment separators. \n \~
    \param[in]  separators  - \ru Массив разделителей.
                                  Каждый разделитель содержит путь по вершинам сетки, ребра которого разделяют сегменты.
                              \en The array of segment separators.
                                  Each separator contains a path by mesh vertices. Edges of that path split mesh to segments. \~
    \return  \ru Возвращает код результата операции.
             \en Returns operation result code. \~
    \ingroup Polygonal_Objects
  */
  virtual MbResultType SegmentMeshBySeparators( const std::vector<std::vector<uint>> & separators ) = 0;

  OBVIOUS_PRIVATE_COPY( MbMeshProcessor )
};

//------------------------------------------------------------------------------
/** \brief \ru Создать оболочку по полигональной сетке c автоматическим
               распознаванием поверхностей.
           \en Create shell from mesh with automatic surface reconstruction. \~
    \details \ru Создать оболочку в граничном представлении, соответствующее модели, заданной полигональной сеткой.
                 Алгоритм в автоматическом режиме распознает и реконструирует грани, соответствующие элементарным
                 поверхностям (плоскость, цилиндр, сфера, конус, тор). \n
             \en Create BRep shell that represents input mesh model.
                 Algorithm automatically detect and reconstruct faces based on elementary surfaces (plane, cylinder, sphere, cone, torus). \n \~
    \param[in]  mesh      - \ru Входная сетка.
                            \en The input mesh. \~
    \param[out] shell     - \ru Указатель на созданную оболочку.
                            \en The pointer to created shell. \~
    \param[in]  tolerance - \ru Точность распознавания поверхностей(по умолчанию равна 0.1).
                            \en Surface reconstruction tolerance (default 0.1). \~
    \return  \ru Возвращает код результата операции.
             \en Returns operation result code. \~
    \ingroup Polygonal_Objects
*/
// ---
MATH_FUNC( MbResultType ) ConvertMeshToShell( MbMesh & mesh, MbFaceShell *& shell, double tolerance = 0.1 );


//------------------------------------------------------------------------------
/** \brief \ru Создать оболочку по коллекции, содержащей полигональную сетку c автоматическим
               распознаванием поверхностей.
           \en Create shell from collection with automatic surface reconstruction. \~
    \details \ru Создать оболочку в граничном представлении, соответствующую модели, заданной полигональной сеткой.
                 Алгоритм в автоматическом режиме распознает и реконструирует грани, соответствующие элементарным
                  поверхностям (плоскость, цилиндр, сфера, конус, тор). \n
    \en Create BRep shell that represents input mesh model from collection.
         Algorithm automatically detect and reconstruct faces based on elementary surfaces (plane, cylinder, sphere, cone, torus). \n \~
    \param[in]  collection  - \ru Коллекция, содержащая входную сетку.
                              \en The input collection. \~
    \param[out] shell       - \ru Указатель на созданную оболочку.
                              \en The pointer to created shell. \~
    \param[in]  tolerance   - \ru Точность распознавания поверхностей(по умолчанию равна 0.1).
                              \en Surface reconstruction tolerance (default 0.1). \~
    \return  \ru Возвращает код результата операции.
             \en Returns operation result code. \~
    \ingroup Polygonal_Objects
*/
// ---
MATH_FUNC( MbResultType ) ConvertCollectionToShell( MbCollection & collection, MbFaceShell *& shell, double tolerance = 0.1 );

#endif // __ACTION_MESH_H
