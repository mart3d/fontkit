﻿#ifndef __VSN_ITERATOR_H
#define __VSN_ITERATOR_H

#include "vsn_global.h"

VSN_BEGIN_NAMESPACE

/// Итератор с произвольным доступом
template <class T> struct RandomAccessIterator
{
    /// \ru Конструктор по умолчанию. \en Default constructor.
	RandomAccessIterator() : m_ptr(0) {}
	/// конструктор с указателем на объект.
	explicit RandomAccessIterator(T* ptr) : m_ptr(ptr) {}
	/// Указатель на объект.
	T* operator ->() const { return m_ptr; }

	/// ссылка на обект
	T& operator *() const { return *m_ptr; }

	/// Преинкремент указателя.
	RandomAccessIterator<T>& operator ++()
	{
		++m_ptr;
		return *this;
	}

	/// Постинкремент указателя.
	RandomAccessIterator<T> operator ++(int)
	{
		RandomAccessIterator<T> it = *this;
		++m_ptr;
		return it;
	}

	/// Предекремент указателя
	RandomAccessIterator<T>& operator --()
	{
		--m_ptr;
		return *this;
	}
	/// Постдекремент указателя.
	RandomAccessIterator<T> operator --(int)
	{
		RandomAccessIterator<T> it = *this;
		--m_ptr;
		return it;
	}
	/// Добавьте смещение к указателю.
	RandomAccessIterator<T>& operator +=(int value)
	{
		m_ptr += value;
		return *this;
	}
	/// Subtract an offset from the pointer.
	RandomAccessIterator<T>& operator -=(int value)
	{
		m_ptr -= value;
		return *this;
	}
	/// Добавьте смещение к указателю.
	RandomAccessIterator<T> operator +(int value) const { return RandomAccessIterator<T>(m_ptr + value); }
	/// Subtract an offset from the pointer.
	RandomAccessIterator<T> operator -(int value) const { return RandomAccessIterator<T>(m_ptr - value); }
	/// Calculate offset to another iterator.
	int operator -(const RandomAccessIterator& rhs) const { return (int)(m_ptr - rhs.m_ptr); }
	/// Test for equality with another iterator.
	bool operator ==(const RandomAccessIterator& rhs) const { return m_ptr == rhs.m_ptr; }
	/// Test for inequality with another iterator.
	bool operator !=(const RandomAccessIterator& rhs) const { return m_ptr != rhs.m_ptr; }
	/// Test for less than with another iterator.
	bool operator <(const RandomAccessIterator& rhs) const { return m_ptr < rhs.m_ptr; }
	/// Test for greater than with another iterator.
	bool operator >(const RandomAccessIterator& rhs) const { return m_ptr > rhs.m_ptr; }
	/// Test for less than or equal with another iterator.
	bool operator <=(const RandomAccessIterator& rhs) const { return m_ptr <= rhs.m_ptr; }
	/// Test for greater than or equal with another iterator.
	bool operator >=(const RandomAccessIterator& rhs) const { return m_ptr >= rhs.m_ptr; }
	T* m_ptr; ///< \ru Указатель. \en Pointer.
};

/// Итератор с произвольным доступом.
template <class T> struct RandomAccessConstIterator
{
	/// Construct.
	RandomAccessConstIterator() : m_ptr(0) {}
	/// Construct with an object pointer.
	explicit RandomAccessConstIterator(T* ptr) : m_ptr(ptr) {}
	/// Construct from a non-const iterator.
	RandomAccessConstIterator(const RandomAccessIterator<T>& rhs) : m_ptr(rhs.m_ptr) {}
	/// Assign from a non-const iterator.
	RandomAccessConstIterator<T>& operator =(const RandomAccessIterator<T>& rhs)
	{
		m_ptr = rhs.m_ptr;
		return *this;
	}
	/// Указатель на объект.
	const T* operator ->() const { return m_ptr; }
	/// Dereference the object.
	const T& operator *() const { return *m_ptr; }
	/// Preincrement the pointer.
	RandomAccessConstIterator<T>& operator ++()
	{
		++m_ptr;
		return *this;
	}
	/// Postincrement the pointer.
	RandomAccessConstIterator<T> operator ++(int)
	{
		RandomAccessConstIterator<T> it = *this;
		++m_ptr;
		return it;
	}
	/// Predecrement the pointer.
	RandomAccessConstIterator<T>& operator --()
	{
		--m_ptr;
		return *this;
	}
	/// Postdecrement the pointer.
	RandomAccessConstIterator<T> operator --(int)
	{
		RandomAccessConstIterator<T> it = *this;
		--m_ptr;
		return it;
	}
	/// Добавьте смещение к указателю.
	RandomAccessConstIterator<T>& operator +=(int value)
	{
		m_ptr += value;
		return *this;
	}
	/// Subtract an offset from the pointer.
	RandomAccessConstIterator<T>& operator -=(int value)
	{
		m_ptr -= value;
		return *this;
	}
	/// Add an offset to the pointer.
	RandomAccessConstIterator<T> operator +(int value) const { return RandomAccessConstIterator<T>(m_ptr + value); }
	/// Subtract an offset from the pointer.
	RandomAccessConstIterator<T> operator -(int value) const { return RandomAccessConstIterator<T>(m_ptr - value); }
	/// Calculate offset to another iterator.
	int operator -(const RandomAccessConstIterator& rhs) const { return (int)(m_ptr - rhs.m_ptr); }
	/// Test for equality with another iterator.
	bool operator ==(const RandomAccessConstIterator& rhs) const { return m_ptr == rhs.m_ptr; }
	/// Test for inequality with another iterator.
	bool operator !=(const RandomAccessConstIterator& rhs) const { return m_ptr != rhs.m_ptr; }
	/// Test for less than with another iterator.
	bool operator <(const RandomAccessConstIterator& rhs) const { return m_ptr < rhs.m_ptr; }
	/// Test for greater than with another iterator.
	bool operator >(const RandomAccessConstIterator& rhs) const { return m_ptr > rhs.m_ptr; }
	/// Test for less than or equal with another iterator.
	bool operator <=(const RandomAccessConstIterator& rhs) const { return m_ptr <= rhs.m_ptr; }
	/// Test for greater than or equal with another iterator.
	bool operator >=(const RandomAccessConstIterator& rhs) const { return m_ptr >= rhs.m_ptr; }
	/// указатель на объект.
	T* m_ptr;
};

VSN_END_NAMESPACE

#endif // __VSN_ITERATOR_H

