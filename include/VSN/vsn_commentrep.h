﻿#ifndef __VSN_COMMENTREP_H
#define __VSN_COMMENTREP_H

#include "vsn_global.h"
#include "vsn_geometryrep.h"
#include "vsn_wireframegeometry.h"

#include <string>

VSN_BEGIN_NAMESPACE


class CommentGeometry;
//------------------------------------------------------------------------------
/** \brief \ru Отображение комментария на выносной линии.
           \en NO TRANSLATION. \~
    \ingroup Vision_Geometry
*/
// ---
class VSN_CLASS CommentRep : public GeometryRep
{
    VSN_OBJECT(CommentRep)
public:
    /// \ru Конструктор. \en Constructor.
    CommentRep( Node* pParent = nullptr );
    /// \ru Деструктор. \en Destructor. 
    virtual ~CommentRep();
public:
    /// \ru Изменить положение комментария. \en NO TRANSLATION.
    void Init(const MbCartPoint3D& first, const MbCartPoint3D& second);
    /// \ru Задать текст. \en NO TRANSLATION.
    void SetText(const std::wstring & text);
    /// \ru Очистить текст. \en NO TRANSLATION.
    void ClearText();
    /// \ru Изменить размер текста. \en NO TRANSLATION.
    void SetFontSize(int);
private:
    VSN_DISABLE_COPY(CommentRep)
};

VSN_END_NAMESPACE

#endif /* __VSN_COMMENT_H */
