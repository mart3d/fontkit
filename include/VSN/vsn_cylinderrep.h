﻿#ifndef __VSN_CYLINDERREP_H
#define __VSN_CYLINDERREP_H

#include "vsn_geometryrep.h"

VSN_BEGIN_NAMESPACE
//------------------------------------------------------------------------------
/** \brief \ru Цилиндр.
           \en A cylinder. \~
    \ingroup Vision_Geometry
*/
// ---
class VSN_CLASS CylinderRep : public GeometryRep
{
    VSN_OBJECT(CylinderRep)
    VSN_PROP_READ_WRITE(polyStep, GetPolygonStep, SetPolygonStep)
    VSN_PROP_READ_WRITE_NOTIFY(radius, GetRadius, SetRadius, RadiusModified)
    VSN_PROP_READ_WRITE_NOTIFY(height, GetHeight, SetHeight, HeightModified)
public:
    explicit CylinderRep(Node* pParent = nullptr);
    virtual ~CylinderRep();
public:
    /// \ru Выдать высоту конуса. \en Get height.
    double GetHeight() const;
    /// \ru Вернуть радиус конуса. \en Set radius.
    double GetRadius() const;
    /// \ru Выдать шаг полигона. \en Get polygon step.
    int GetPolygonStep() const;
    /// \ru Задать шаг полигона (должен быть больше нуля). \en Set the polygon step (must be greater than zero).
    // устновить шаг поигона должен быть больше 0
    void SetPolygonStep(int polyStep);
public:
    /// \ru Задать радиус конуса (должен быть больше нуля). \en Set the radius (must be greater than zero).
    VSN_SLOT(Public, SetRadius, void SetRadius(double radius))
    /// \ru Задать высоту конуса (должна быть больше нуля). \en Set the height (must be greater than zero).
    VSN_SLOT(Public, SetHeight, void SetHeight(double height))
public:
    VSN_SIGNAL(Public, RadiusModified, void RadiusModified(double radius), radius)
    VSN_SIGNAL(Public, HeightModified, void HeightModified(double height), height)
private:
    VSN_DISABLE_COPY(CylinderRep)
};

VSN_END_NAMESPACE

#endif /* __VSN_CYLINDERREP_H */

