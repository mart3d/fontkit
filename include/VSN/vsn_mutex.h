﻿///////////////////////////////////////////////////////////////////////////////
/**
	\file
	\brief \ru Класс Mutex - этот класс представляет как нерекурсивную семантику владения, так и рекурсивную.
               Класс MutexLocker - это класс для удобства, который упрощает блокировку и разблокировку мьютексов.
		   \en NO TRANSLATION. \~
*/
///////////////////////////////////////////////////////////////////////////////
#ifndef __VSN_MUTEX_H
#define __VSN_MUTEX_H

#include "vsn_global.h"

VSN_BEGIN_NAMESPACE
class MutexPrivate;
//----------------------------------------------------------------------------
//
/** \brief \ru Класс Mutex является примитивом синхронизации, который может использоваться для
               защиты разделяемых данных от одновременного доступа нескольких потоков. В случае
               recursive класс Mutex может быть использован для защиты общих данных от одновременного 
               доступа нескольких потоков.
		   \en NO TRANSLATION. \~
  \details \ru Если нерекурсивная семантика владения (recursive равно false):
               * Вызывающий поток владеет мьютексом со времени успешного вызова Lock или Trylock и до момента вызова Unlock.
               * Пока поток владеет мьютексом, все остальные потоки при попытке завладения им блокируются на вызове Lock или получают значение false при вызове Trylock.
               * Вызывающий поток не должен владеть мьютексом до вызова Lock или Trylock.

               Если рекурсивная семантика владения (recursive равно true):
               * Вызывающий поток владеет recursive_mutex за определенный период времени, который начинается, 
                 когда он успешно вызывает либо Lock или Trylock. В течение этого периода поток может сделать 
                 дополнительные вызовы Lock или Trylock. Период владения заканчивается, когда поток делает 
                 соответствующее количество обращений к Unlock.
               * Когда поток владеет mutex, все остальные потоки будут блокированы (при вызове Lock) или возвращаться значение false при вызове Trylock. \n
           \en NO TRANSLATION. \n \~
    \ingroup Vision_Thread
*/
// ---
class VSN_CLASS Mutex
{
public:
	enum RecursionMode { NonRecursive, Recursive };
    /// \ru Конструктор по умолчанию. \en NO TRANSLATION.
	explicit Mutex(RecursionMode mode = NonRecursive);
    /// \ru Деструктор. \en Destructor.
	~Mutex();
public:
    /// \ru Вернуть . \en NO TRANSLATION.
	bool IsRecursive();
    /// \ru Блокировать мьютекс - выполнение останавливается, если мьютекс недоступен. \en NO TRANSLATION.
	void Lock();
    /// \ru Пытается заблокировать мьютекс, возвращается, если мьютекс недоступен. \en NO TRANSLATION.
	bool TryLock();
    /// \ru Разблокировать мьютекс. \en NO TRANSLATION.
	void Unlock();
private:
    std::shared_ptr<MutexPrivate> m_ptrData;

};

//----------------------------------------------------------------------------
//
/** \brief \ru Класс MutexLocker является оболочкой мьютекса, которая обеспечивает удобный механизм владением 
               мьютексом на протяжении области видимости.
           \en NO TRANSLATION. \~
  \details \ru Когда MutexLocker создан, он берет на себя ответственность мьютекса. Когда управление покидает область, 
               в которой MutexLocker был создан, MutexLocker разрушается и мьютекс освобождается. \n
           \en NO TRANSLATION. \n \~
  \ingroup Vision_Thread
*/
// ---
class VSN_CLASS MutexLocker
{
public:
    /// \ru Конструктор по умолчанию. Блокирует мьютекс. \en NO TRANSLATION.
    inline explicit MutexLocker(Mutex* pMutex) : m_pMutex(pMutex) { Relock(); }
    /// \ru Деструктор. \en Destructor.
    inline ~MutexLocker() { Unlock(); }
public:
    /// \ru Разблокировать мьютекс. \en NO TRANSLATION.
    inline void Unlock() { m_pMutex->Unlock(); }
    /// \ru Блокировать мьютекс - выполнение останавливается, если мьютекс недоступен. \en NO TRANSLATION.
    inline void Relock() { m_pMutex->Lock(); }
    /// \ru Вернуть указатель на мьютекс. \en NO TRANSLATION.
    inline Mutex* GetMutex() const { return m_pMutex; }
private:
    VSN_DISABLE_COPY(MutexLocker)
    Mutex* m_pMutex;
};

VSN_END_NAMESPACE


#endif // QMUTEX_H
