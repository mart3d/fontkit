﻿////////////////////////////////////////////////////////////////////////////////
/**
\file
    \brief \ru Класс Image обеспечивает аппаратно - независимое изображение.
           \en NOT TRANSLATED. \~
*/
////////////////////////////////////////////////////////////////////////////////
#ifndef __VSN_IMAGE_H
#define __VSN_IMAGE_H

#include "vsn_global.h"
#include <iostream>
#include <string>

VSN_BEGIN_NAMESPACE

class ImagePrivate;
//------------------------------------------------------------------------------
/** \brief  \ru Класс Image предоставляет инструменты для загрузки/сохранения разных форматов изображений.
            \en NOT TRANSLATED. \~
    \details \ru Есть возможность прямого доступа к пиксильным данным. \n
             \en NOT TRANSLATED. \n \~
    \ingroup Vision_Image
*/
// ---
class VSN_CLASS Image
{
public:
    enum class Format
    {
        None, 
        //not compressed
        UINT8,  // 8 bit,  alpha
        UINT24, // 24 bit, RGB
        UINT32, // 32 bit, RGBA
        // compressed
        DXT1,
        DXT2,
        DXT3,
    };
public:
    /** \brief \ru Конструктор по умолчанию.
               \en Default constructor. \~
    */
    Image();

    /** \brief \ru Конструктор перемещения.
               \en Move constructor. \~
    */
    Image(Image &&);

    /** \brief \ru Конструктор копирования.
               \en copy constructor. \~
    */
    Image(const Image & image);

    /** \brief \ru Конструктор для чтения из потока.
               \en NO TRANSLATION. \~
        \param[in] in - \ru Поток для чтения изображения.
                        \en NO TRANSLATION. \~
    */
    Image( std::istream & in );

    /** \brief \ru Оператор копирования.
               \en NO TRANSLATION. \~
    */
    Image operator = (const Image &) = delete;

    /// \ru Деструктор. \en Destructor.
    virtual ~Image();
public:
    /// \ru Вернуть признак сжатого изображения. \en NOT TRANSLATED.
    bool IsCompressed() const;

    /// \ru Вернуть ширину изображения. \en NOT TRANSLATED.
    uint GetWidth() const;
    /// \ru Вернуть высоту изображения. \en NOT TRANSLATED.
    uint GetHeight() const;
    /// \ru Вернуть формат изображения. \en NOT TRANSLATED.
    Format          GetFormat() const;
    /// \ru Вернуть указатель на данные(может быть null). \en Return pointer on the bits.
    unsigned char* GetBits() const;
    /// \ru Узнать содержит ли изображение данные. \en NOT TRANSLATED.
    bool IsValid() const;
public:
    /** \brief \ru Загрузить изображение из файла.
               \en NO TRANSLATION. \~
    \param[in] fileName - \ru Имя файла.
    \en NO TRANSLATION. \~
    \en NO TRANSLATION. \~
    */
    void Init(const std::string& fileName);

    /** \brief \ru Загрузить изображение из потока.
               \en NO TRANSLATION. \~
    \param[in] in - \ru Поток для чтения изображения.
    \en NO TRANSLATION. \~
    */
    void Init(std::istream& in);

    /** \brief \ru Инициализировать Image параметрами.
               \en NO TRANSLATION. \~
    \param[in] w - \ru Ширина изображения.
    \param[in] h - \ru Высота изображения.
    \param[in] f - \ru Формат изображения.
    \param[in] mem - \ru Указатель на массив информации цветов.
    \en NO TRANSLATION. \~
    */
    void Init( uint w, uint h, Format f, std::unique_ptr<unsigned char[]> mem );

    /** \brief \ru Инициализировать Image параметрами.
    \en NO TRANSLATION. \~
    \param[in] w - \ru Ширина изображения.
    \param[in] h - \ru Высота изображения.
    \param[in] f - \ru Формат изображения.
    \param[in] mem - \ru Указатель на массив информации цветов.
    \en NO TRANSLATION. \~
    */
    void Init(uint w, uint h, Format f, const unsigned char * mem );

    /** \brief \ru Инициализировать Image параметрами другого image.
               \en NO TRANSLATION. \~
    \en NO TRANSLATION. \~
    */
    void Init( const Image & );

    /** \brief \ru Сохранить изображение в поток(в формате PNG).
    \en NO TRANSLATION. \~
    \param[in] fileName - \ru Имя файла.
    \en NO TRANSLATION. \~
    */
    void SavePNG(std::ostream & out);

    /** \brief \ru Сохранить изображение в файл(в формате PNG).
               \en NO TRANSLATION. \~
    \param[in] fileName - \ru Имя файла.
    \en NO TRANSLATION. \~
    */
    void SavePNG(const std::string & fileName);

    /** \brief \ru Удалить данные изображения.
               \en NO TRANSLATION. \~
    */
    void Clear();

    /** \brief \ru Создать копию изображения с другими размерами
               \en NO TRANSLATION. \~
               \param[in] sw - \ru Нововое значение ширины.
               \param[in] sw - \ru Нововое значение высоты.
    */
    Image Scale(uint sw, uint sh) const;

    /** \brief \ru Возвращает скопированную часть изображения
               \en NO TRANSLATION. \~
               \param[in] x - \ru Позиция левой стороны нового изображения относительно левой стороны исходного
               \param[in] y - \ru Позиция нижней стороны нового изображения относительно нижней стороны исходного.
               \param[in] w - \ru ширина новго изображения
               \param[in] h - \ru высота новго изображения
    */
    Image SubImage(uint x, uint y, uint w, uint h);
private:
    VSN_DECLARE_PRIVATE(Image);
};

VSN_END_NAMESPACE

#endif // __VSN_IMAGE_H
