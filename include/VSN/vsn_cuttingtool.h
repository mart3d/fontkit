﻿#ifndef __VSN_CUTTINGTOOL_H
#define __VSN_CUTTINGTOOL_H

#include <mb_placement3d.h>

#include "vsn_global.h"
#include "vsn_objectdefs.h"
#include "vsn_object.h"

VSN_BEGIN_NAMESPACE
class Material;
class RenderContainer;
class Widget3DBasic;
class CuttingToolPrivate;

typedef uint64 SectionPlaneId;


/*SectionPlanesContainerInterface*/
class VSN_CLASS CuttingTool : public Object
{
    VSN_OBJECT(CuttingTool)
public:
     CuttingTool( RenderContainer & );
     ~CuttingTool();

     // включить режим динамического сечения
     void          Enable( bool enable );
     // выключить режим динамического сечения
     bool          IsEnabled() const;

     // добавить секущую плоскость
     SectionPlaneId AddSectionPlane( const MbPlacement3D & plane );
     // удалить секущую плоскость
     void           DeleteSectionPlane( SectionPlaneId id );
     // включить/выключить секущую плоскость
     void           Enable(SectionPlaneId id, bool);
     // признак включения плоскости
     bool           IsEnabled(SectionPlaneId id) const;
     // изменить отсекаемую часть
     void           Invert(SectionPlaneId id, bool invert);
     // признак направления отсекаемой стороны
     bool           IsInverted(SectionPlaneId id) const;
     // заполнить разрезы
     void           CloseSectionPlane(SectionPlaneId id, bool fill);
     // нужно ли зполнять разрезы
     bool           IsClosedSectionPlane(SectionPlaneId id) const;
     // включить режим отображения с манипулятором
     void           EnableInteractiveMode(SectionPlaneId idbool, bool enable );
     // проверить режим отображения с манипулятором
     bool           IsEnabledInteractiveMode(SectionPlaneId id) const;
     // получить математику плоскости
     const MbPlacement3D & GetPlacement(SectionPlaneId id) const;
     // получить параметры плоскости (угол в градусах)
     void           GetParams(SectionPlaneId id, double & angle1, double & angle2, double & offset) const;
     // материал закрашиваемой поверхности
     void           SetSurfaceMaterial(SectionPlaneId id, std::shared_ptr<Material> material); 
     // изменить параметры плоскости
     void           Init( SectionPlaneId id, const MbPlacement3D & plane);
     // изменить параметры плоскости(угол в градусах)
     void           Init( SectionPlaneId id, double angle1, double angle2, double offset );
public:
     /// сигнал об изменении позиции плоскости при помощи манипуляторов
     VSN_SIGNAL(Public, ChangePlacement, void ChangePlacement(SectionPlaneId id), id);
public:
    // вернуть список всех созданных плоскостей
    std::list<SectionPlaneId> GetSectionPlanes() const;
    // нарисовать плоскости
    void                      RenderPlanes( const MbCube & gab ) const;
    // контейнер манипуляторов
    RenderContainer & GetWidgets3D();
private:
    VSN_DECLARE_PRIVATE(CuttingTool)
};

VSN_END_NAMESPACE

#endif // __VSN_CUTTINGTOOL_H
