﻿////////////////////////////////////////////////////////////////////////////////
/**
\file
    \brief \ru Класс GraphicsScene представляет реализацию отрисовки сцены OpenGl'ем.
           \en NO TRANSLATION. \~
*/
////////////////////////////////////////////////////////////////////////////////
#ifndef __VSN_GRAPHICSSCENE_H
#define __VSN_GRAPHICSSCENE_H

#include "vsn_object.h"
#include "vsn_orientationmarkerwidget.h"
#include "vsn_absvisioncomponent.h"
#include "vsn_processmanager.h"
#include "vsn_global.h"
#include "vsn_cuttingtool.h"

VSN_BEGIN_NAMESPACE
class Camera;
class Light;
class Process;
class Viewport;
class ProcessManager;
class SceneContent;
class Widget3DBasic;
class GraphicsScenePrivate;
class OrientationMarkerWidget;
//------------------------------------------------------------------------------
/** \brief \ru Класс GraphicsScene представляет собой реализацию отрисовки сцены OpenGl'ем.
           \en NO TRANSLATION. \~
    \ingroup Vision_OpenGL
*/
// ---
class VSN_CLASS GraphicsScene : public AbsVisionComponent
{
    VSN_OBJECT(GraphicsScene)
public:
    /// \ru Конструктор по умолчанию. \en Default constructor. \~
    explicit GraphicsScene(Object* pParent = nullptr);
    /// \ru Деструктор. \en Destructor. \~
    virtual ~GraphicsScene();
public:
    /// \ru Инициализация OpenGL. \en NO TRANSLATION. \~
    void InitializeGL();
    /// \ru Вернуть режим отображения модели. \en NO TRANSLATION. \~
    RenderMode GetRenderMode() const;
    /// \ru установить новый режим отображения. \en NO TRANSLATION. \~
    void SetRenderMode(RenderMode mode);
    /// \ru Установить режим мультисэмплексное сглаживание(MSAA). \en NO TRANSLATION. \~
    void SetMultiSampleMode(bool mode);
    /// \ru Вернуть глобальный идентификатор шейдера. \en NO TRANSLATION. \~
    inline uint GetGlobalShaderId() const;
    /// \ru Установить глобальный идентификатор шейдера. \en NO TRANSLATION. \~
    void SetGlobalShaderId(uint id, const String& name);
    /// \ru Установить новое положение камеры. \en NO TRANSLATION. \~
    void SetPositionCamera(const Camera& newCameraPosition, Object* pView);
    /// \ru Установить новую ориентацию камеры. \en NO TRANSLATION. \~
    void SetOrientationCamera(Orientation orientation, Object* pView, bool bZoomToFit);

    /// \ru Вернуть true, если камера будет плавно перемещаться из одной позиции в другую. \en NO TRANSLATION. \~
    bool GetSmoothTransition() const;
    /// \ru Этот метод определяет, должна ли камера плавно перейти от предыдущей позиции к новой. \en NO TRANSLATION. \~
    void SetSmoothTransition(bool bSmooth);

    /// \ru Вернуть продолжительность плавного перехода в секундах. \en NO TRANSLATION. \~
    float GetSmoothTransitionDuration() const;
    /// \ru Этот метод определяет продолжительность плавного перехода в секундах. \en NO TRANSLATION. \~
    void SetSmoothTransitionDuration(float duration);

    /// \ru Вернуть указатель на маркер ориентации для управления. \en NO TRANSLATION. \~
    OrientationMarkerWidget* GetOrientationMarker() const;
    /// \ru Вернуть указатель на содержание сцены. \en NO TRANSLATION.\~
    SceneContent* GetSceneContent();
    /// \ru Вернуть указатель контейнер временных объектов. \en NO TRANSLATION.\~
    std::shared_ptr<RenderContainer> GetTempObjsContainer() const;
    /// \ru Вернуть указатель на Viewport. \en NO TRANSLATION. \~
    Viewport* GetViewport();
    /// \ru Вернуть указатель на управления процессами. \en NO TRANSLATION. \~
    ProcessManager* GetProcessManager();

    /// \ru Вернуть указатель на основной источник света. \en NO TRANSLATION. \~
    Light* GetMainLight() const;
    /// \ru Установить двустороннее освещение. \en NO TRANSLATION. \~
    void SetDoubleSidedLighting(bool flag);

    /// \ru Вернуть список указателей на дополнительные источники освещения. \en NO TRANSLATION. \~
    std::list<Light*> GetExtraLights() const;
    /// \ru Создать дополнительный источник освещения. \en NO TRANSLATION.
    Light* CreateExtraLight();
    /// \ru Разрушить дополнительный источник освещения. \en NO TRANSLATION.
    void DestroyExtraLight(Light* pLight);

    /// \ru Вернуть и обновление глобальный ограничивающий куб.  \en NO TRANSLATION. \~
    MbCube GetGlobalBoundingBox();
    /// \ru Вернуть указатель на маркер ориентации. \en NO TRANSLATION. \~
    OrientationMarkerWidget* GetOrientationMarkerWidget() const;
    /// \ru Вернуть ссылку на инструмент динамического сечения. \en NO TRANSLATION. \~
    CuttingTool* GetCuttingTool();
public:
    /** \brief \ru Добавить интерактивный процесс.
               \en Adds interactive process. \~
        \param[in] scene - \ru Идентификатор интерактивного процесса.
                           \en Interactive process identifier. \~
        \param[in] id - \ru Идентификатор интерактивного процесса.
                        \en Interactive process identifier. \~
    */
    void AddProcess(ProcessManager::IdProcess id);

    /** \brief \ru Добавить интерактивный процесс.
               \en Adds interactive process. \~
        \param[in] scene - \ru Идентификатор интерактивного процесса.
                            \en Interactive process identifier. \~
        \param[in] id - \ru Идентификатор интерактивного процесса.
                        \en Interactive process identifier. \~
    */
    void AddProcess(Process* pProcess, ProcessManager::IdProcess id);

    /** \brief \ru Удалить интерактивный процесс.
               \en Removes interactive process. \~
       \param[in] id - \ru Идентификатор интерактивного процесса.
                       \en Interactive process identifier. \~
    */
    void RemoveProcess(ProcessManager::IdProcess id);

    /** \brief \ru Активировать процесс и установить данные.
               \en Activates process and sets data. \~
        \param[in] id - \ru Идентификатор интерактивного процесса.
                        \en Interactive process identifier. \~
        \param[in] event - \ru Событие перемещения.
                           \en Movement event. \~
    */
    void ActivateProcess(ProcessManager::IdProcess id, ProcessEvent* event);

    /** \brief \ru Сбросить активный процесс.
               \en Resets active process. \~
    */
    void DeactivateProcess();

    /** \brief \ru Принять событие от инициатора.
               \en Accepts event from initiator. \~
        \param[in] event - \ru Событие перемещения.
                           \en Movement event. \~
    */
    bool MoveActivateProcess(ProcessEvent* event);

    /** \brief \ru Добавить виджет.
               \en NOT TRANSLATED. \~
        \param[in] pWidget - \ru Указатель на виджет.
                             \en NOT TRANSLATED. \~
    */
    void AddWidget(Widget3DBasic* pWidget);
public:
    VSN_SIGNAL(Public, UpdateScene, void UpdateScene());
public:
    void DrawGL();
    void ResizeGL(const Size& size);
protected:
    virtual void OnEngineStarting() override;
    virtual void OnInstalled() override;
    virtual void OnUninstalled() override;
    virtual std::vector<VisionWorkPtr> WorksToExecute(int64 time) override;
private:
    VSN_DISABLE_COPY(GraphicsScene);
    VSN_DECLARE_EX_PRIVATE(GraphicsScene);
    friend class PickLimitingVolumeWork;
};

VSN_END_NAMESPACE

#endif // __VSN_GRAPHICSSCENE_H
