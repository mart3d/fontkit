﻿////////////////////////////////////////////////////////////////////////////////
/**
\file
    \brief \ru Класс OpenGLFramebufferObject предназначен для упрощения работы с FBO, MRT, PBO
           \en NO TRANSLATION. \~
*/
////////////////////////////////////////////////////////////////////////////////
#ifndef __VSN_OPENGLFRAMEBUFFER_H
#define __VSN_OPENGLFRAMEBUFFER_H

#include "vsn_global.h"
#include "vsn_size.h"
#include "vsn_image.h"


typedef unsigned int GLuint;

VSN_BEGIN_NAMESPACE

class OpenGLFramebufferObjectPrivate;
//------------------------------------------------------------------------------
/** \brief \ru Создает объект framebuffer OpenGL.
           \en NO TRANSLATION. \~
    \details \ru NO TRANSLATION. \n.
             \en NO TRANSLATION. \n \~
    \ingroup Vision_OpenGL
*/
// ---
class VSN_CLASS OpenGLFramebufferObject
{
public:
    /** \brief \ru Конструктор.
               \en Constructor. \~
        \param[in] width - \ru Ширина.
                           \en NO TRANSLATION. \~
        \param[in] height - \ru Высота.
                            \en NO TRANSLATION. \~
        \param[in] multisample - \ru Уровень сглаживания.
                                 \en NO TRANSLATION. \~
        \param[in] depth - \ru Создавать буфер глубины или нет.
                           \en NO TRANSLATION. \~
        \param[in] stencil - \ru Создавать буфер трафорета или нет.
                             \en NO TRANSLATION. \~
    */
    OpenGLFramebufferObject(int width, int height, int multisample = 0, bool depth = false, bool stencil = false);
    /** \brief \ru Конструктор.
               \en Constructor. \~
        \param[in] size - \ru Размеры буфера.
                           \en NO TRANSLATION. \~
        \param[in] multisample - \ru Уровень сглаживания.
                                 \en NO TRANSLATION. \~
        \param[in] depth - \ru Создавать буфер глубины или нет.
                           \en NO TRANSLATION. \~
        \param[in] stencil - \ru Создавать буфер трафорета или нет.
                             \en NO TRANSLATION. \~
    */
    OpenGLFramebufferObject(const Size& size, int multisample = 0, bool depth = false, bool stencil = false);
    /// \ru Деструктор. \en Destructor. 
    virtual ~OpenGLFramebufferObject();
public:
    /// \ru Привязать Framebuffer. \en NO TRANSLATION.
    void Bind();
    /// \ru Oтвязать Framebuffer. \en NO TRANSLATION.
    void Release();
    /// \ru Изменить размеры. \en NO TRANSLATION.
    void Update(int width, int height);
    /// \ru Получить ширину Framebuffer. \en NO TRANSLATION.
    int  GetWidth() const;
    /// \ru Получить высоту Framebuffer. \en NO TRANSLATION.
    int  GetHeight() const;
    /// \ru Проверить состояние буфера. \en NO TRANSLATION.
    bool IsValid() const;
    /// \ru Сохранить в изображение. \en NO TRANSLATION.
    Image MakeImage();
    /// \ru Перекладывает содержимое буфера в текстуру. (текстуру удалать не нужно). \en NO TRANSLATION
    GLuint MakeTexture() const;
    /// \ru Отобразить содержимое буфера в заданном прямоугольнике. \en NO TRANSLATION
    void RenderImage(float left, float bottom, float width, float height);
private:
    VSN_DECLARE_PRIVATE(OpenGLFramebufferObject)
    VSN_DISABLE_COPY(OpenGLFramebufferObject)
};

VSN_END_NAMESPACE

#endif /*__VSN_OPENGLFRAMEBUFFER_H*/