﻿////////////////////////////////////////////////////////////////////////////////
/** 
  \file
  \brief \ru NO TRANSLATION.
         \en NO TRANSLATION. \~

*/
////////////////////////////////////////////////////////////////////////////////

#ifndef __VSN_BOUNDINGBOXGEOMETRY_H
#define __VSN_BOUNDINGBOXGEOMETRY_H

#include "vsn_wireframegeometry.h"
#include "vsn_global.h"

VSN_BEGIN_NAMESPACE
class SceneSegment;
class BoundingBoxGeometryPrivate;
//------------------------------------------------------------------------------
/** \brief \ru NO TRANSLATION.
           \en NO TRANSLATION. \~
  \details \ru NO TRANSLATION. \n.
           \en NO TRANSLATION. \n \~
  \ingroup NO GROUP
*/
// ---
class VSN_CLASS BoundingBoxGeometry : public WireframeGeometry
{
    VSN_OBJECT(BoundingBoxGeometry);
public:
    /// \ru Конструктор по умолчанию. \en Default constructor.
    BoundingBoxGeometry(Node* pParent = nullptr);
    /// \ru Деструктор. \en Destructor. 
    virtual ~BoundingBoxGeometry();
public:
    /// \ru Вернуть размеры сегмента. \en NO TRANSLATION.
    MbCube GetSegmentBoundingBox() const;

    /// \ru NO TRANSLATION. \en NO TRANSLATION.
    const SceneSegment* GetSceneSegment() const;
    /// \ru NO TRANSLATION. \en NO TRANSLATION.
    void SetSceneSegment(const SceneSegment* pSegment);

    /// \ru Вернуть цвет оси. \en NO TRANSLATION.
    Color GetColorAxis(AxisOrientation orientation) const;
    /// \ru Установить цвет оси. \en NO TRANSLATION.
    void SetColorAxis(AxisOrientation orientation, const Color& color);
public:
    /// \ru Сделать копию объекта. \en Create a copy of the object.
    virtual WireframeGeometry* Duplicate() const;
    /// \ru Выдать габаритный куб. \en Get a bounding box.
    virtual const MbCube& GetBoundingBox();
    /// \ru Блокировать PixelCulling для этой геометрии. \en NO TRANSLATION.
    virtual bool IsIgnorePixelCulling() const override;
    /// \ru Обновить геометрию по новым данным. \en NO TRANSLATION.
    virtual void UpdateGeometry() override;
public:
    VSN_SIGNAL(Public, GeometryModified, void GeometryModified())
private:
    /// \ru Отрисовать параллелепипед с помощью OpenGL. \en Draw a box with OpenGL.
    virtual void OpenGLDraw(const RenderState& state) override;
private:
    void UpdateBoundingBox();
    /// \ru Создать каркасное представление. \en Create a wireframe of a box.
    void BuildWireFrameBox();
private:
    VSN_DECLARE_EX_PRIVATE(BoundingBoxGeometry);
};

VSN_END_NAMESPACE

#endif /* __VSN_BOUNDINGBOXGEOMETRY_H */
