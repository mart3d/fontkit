﻿////////////////////////////////////////////////////////////////////////////////
/**
  \file
  \brief \ru Класс Size описывает размер и используя целочисленную точность.
         \en NO TRANSLATION. \~
*/
////////////////////////////////////////////////////////////////////////////////

#ifndef __VSN_SIZE_H
#define __VSN_SIZE_H

#include "vsn_mathfunctions.h"
#include "vsn_global.h"

VSN_BEGIN_NAMESPACE


//----------------------------------------------------------------------------
//
/** \brief \ru Класс Size описывает размер и используя целочисленную точность.
           \en  NO TRANSLATION. \~
    \details \ru Size определяет ширину и высоту. Он может быть инициализирован в конструкторе или изменен с помощью функций SetWidth(), SetHeight(), а также используя арифметические операторы. 
                 Size также можно управлять непосредственно изменив его значения с помощью получения ссылок на ширину и высоту. Наконец, ширину и высоту можно инициализировать функции преобразования. 
                 Метод IsValid() определяет правильного размера(у правильного размер ширина и высота должны быть больше нуля). Функция IsEmpty() возвращает true, если ширина и высота меньше или равно нулю, 
                 а функция IsNull() возвращает true, если ширина и высота равны нулю.\n
             \en NO TRANSLATION. \n \~
    \ingroup Vision_Common
*/
// ---
class VSN_CLASS Size
{
public:
    /// \ru Конструктор по умолчанию. Создает недопустимый размер(т. е. IsValid() возвращает false). \en NO TRANSLATION.
    Size();
    /// \ru Конструктор с заданием ширины и высоты. \en NO TRANSLATION.
    Size(int w, int h);
public:
    /// \ru Возвращает true, если ширина и высота равна 0 в противном случае возвращает false. \en NO TRANSLATION.
    bool IsNull() const; 
    /// \ru Возвращает true, если ширина и высота меньше или равна 0 в противном случае возвращает false.\en NO TRANSLATION.
    bool IsEmpty() const;
    /// \ru Возвращает true, если ширина и высота равна или больше 0 в противном случае возвращает false. \en NO TRANSLATION.
    bool IsValid() const;

    /// \ru Возвращает ширину. \en NO TRANSLATION.
    int GetWidth() const;
    /// \ru Возвращает высоту. \en NO TRANSLATION.
    int GetHeight() const;
  
    /// \ru Установить ширину. \en NO TRANSLATION.
    void SetWidth(int w);
    /// \ru Установить высоту. \en NO TRANSLATION.
    void SetHeight(int h);

    /// \ru Возвращает ссылку на ширину. \en NO TRANSLATION.
    int& InitWidth();
    /// \ru Возвращает ссылку на высоту. \en NO TRANSLATION.
    int& InitHeight();

    /// \ru Добавляет указанный размер к этому размеру и вернуть ссылку на этот размер. \en NO TRANSLATION.
    Size& operator += (const Size&);
    /// \ru Вычитание заданного размера из этого размера и вернуть ссылку на этот размер. \en NO TRANSLATION.
    Size& operator -= (const Size&);
    /// \ru NO TRANSLATION. \en NO TRANSLATION.
    const Size operator *= (double);

    /// \ru Возвращает true, если указанный размер и этот равны в противном случае возвращает false. \en NO TRANSLATION.
    bool operator == (const Size&);
    /// \ru Возвращает true, если указанный размер и этот отличны в противном случае возвращает false. \en NO TRANSLATION.
    bool operator != (const Size&);

    /// \ru Возвращает сумму указанного размера и этого, каждый компонент добавляется отдельно. \en NO TRANSLATION.
    const Size operator + (const Size&);
    /// \ru Возвращает разность из этого размера указанного, каждый компонент вырезаются отдельно. \en NO TRANSLATION.
    const Size operator - (const Size&);
    /// \ru NO TRANSLATION. \en NO TRANSLATION.
    //  const Size operator * (double);
    friend VSN_FUNC_EX const Size operator * (const Size &s, double c);
private:
    int m_width;  ///< \ru Ширина размера. \en NO TRANSLATION.
    int m_height; ///< \ru Высота размера. \en NO TRANSLATION.
};


inline Size::Size() 
  : m_width(-1)
  , m_height(-1) 
{
}

inline Size::Size(int w, int h) 
  : m_width(w)
  , m_height(h) 
{
}

inline bool Size::IsNull() const
{ 
  return m_width == 0 && m_height == 0; 
}

inline bool Size::IsEmpty() const
{ 
  return m_width < 1 || m_height < 1; 
}

inline bool Size::IsValid() const
{ 
  return m_width >= 0 && m_height >= 0; 
}

inline int Size::GetWidth() const
{ 
  return m_width; 
}

inline int Size::GetHeight() const
{ 
  return m_height; 
}

inline void Size::SetWidth(int w)
{ 
  m_width = w; 
}

inline void Size::SetHeight(int h)
{ 
  m_height = h; 
}

inline int& Size::InitWidth()
{ 
  return m_width; 
}

inline int &Size::InitHeight()
{ 
  return m_height; 
}

inline Size &Size::operator += (const Size &s)
{ 
  m_width += s.m_width; 
  m_height += s.m_height; 
  return *this; 
}

inline Size& Size::operator-=(const Size &s)
{ 
  m_width -= s.m_width; 
  m_height -= s.m_height; 
  return *this; 
}

inline bool Size::operator == (const Size &s)
{ 
  return m_width == s.m_width && m_height == s.m_height; 
}

inline bool Size::operator != (const Size &s)
{ 
  return m_width != s.m_width || m_height != s.m_height; 
}

inline const Size Size::operator + (const Size & s)
{ 
  return Size(m_width + s.m_width, m_height + s.m_height); 
}

inline const Size Size::operator - (const Size& s)
{ 
  return Size(m_width - s.m_width, m_height - s.m_height); 
}



//----------------------------------------------------------------------------
//
/** \brief \ru Класс Size описывает размер и используя с плавающей точкой точность.
           \en  NO TRANSLATION. \~
    \details \ru Size определяет ширину и высоту. Он может быть инициализирован в конструкторе или изменен с помощью функций SetWidth(), SetHeight(), а также используя арифметические операторы. 
                 Size также можно управлять непосредственно изменив его значения с помощью получения ссылок на ширину и высоту. Наконец, ширину и высоту можно инициализировать функции преобразования. 
                 Метод IsValid() определяет правильного размера(у правильного размер ширина и высота должны быть больше нуля). Функция IsEmpty() возвращает true, если ширина и высота меньше или равно нулю, 
                 а функция IsNull() возвращает true, если ширина и высота равны нулю.\n
             \en NO TRANSLATION. \n \~
    \ingroup Vision_Common
*/
// ---
class VSN_CLASS DoubleSize
{
public:
    /// \ru Конструктор по умолчанию. Создает недопустимый размер(т. е. IsValid() возвращает false). \en NO TRANSLATION.
    DoubleSize();
    /// \ru Конструктор с заданием размера ширины и высоты с целочисленной точностью. \en NO TRANSLATION.
    DoubleSize(const Size& sz);
    /// \ru Конструктор с заданием ширины и высоты с плавающей точностью. \en NO TRANSLATION.
    DoubleSize(double w, double h);
public:
    /// \ru Возвращает значение true, если ширина и высота равна 0.0 в противном случае возвращает false. \en NO TRANSLATION.
    inline bool IsNull() const;
    /// \ru Возвращает true, если ширина и высота меньше или равно 0 в противном случае возвращает false. \en NO TRANSLATION.
    inline bool IsEmpty() const;
    /// \ru Возвращает true, если ширина и высота равна или больше 0 в противном случае возвращает false. \en NO TRANSLATION.
    inline bool IsValid() const;

    /// \ru Возвращает ширину. \en NO TRANSLATION.
    inline double GetWidth() const;
    /// \ru Возвращает высоту. \en NO TRANSLATION.
    inline double GetHeight() const;

    /// \ru Установить ширину. \en NO TRANSLATION.
    inline void SetWidth(double w);
    /// \ru Установить высоту. \en NO TRANSLATION.
    inline void SetHeight(double h);

    /// \ru Возвращает ссылку на ширину. \en NO TRANSLATION.
    inline double& InitWidth();
    /// \ru Возвращает ссылку на высоту. \en NO TRANSLATION.
    inline double& InitHeight();

    /// \ru Добавляет указанный размер к этому размеру и вернуть ссылку на этот размер. \en NO TRANSLATION.
    inline DoubleSize& operator+=(const DoubleSize&);
    /// \ru Вычитание заданного размера из этого размера и вернуть ссылку на этот размер. \en NO TRANSLATION.
    inline DoubleSize& operator-=(const DoubleSize&);
    /// \ru Умножить ширину и высоту на заданную величену и вернуть ссылку на размер. \en NO TRANSLATION.
    inline DoubleSize& operator*=(double c);
    /// \ru Делит ширину и высоту на заданный делитель и вернуть ссылку на размер. \en NO TRANSLATION.
    inline DoubleSize& operator/=(double c);

    /// \ru Возвращает true, если указанный размер и этот равны в противном случае возвращает false. \en NO TRANSLATION.
    friend inline bool operator==(const DoubleSize&, const DoubleSize&);
    /// \ru Возвращает true, если указанный размер и этот отличны в противном случае возвращает false. \en NO TRANSLATION.
    friend inline bool operator!=(const DoubleSize&, const DoubleSize&);
    /// \ru Возвращает сумму указанного размера и этого, каждый компонент добавляется отдельно. \en NO TRANSLATION.
    friend inline const DoubleSize operator+(const DoubleSize&, const DoubleSize&);
    /// \ru Возвращает разность из этого размера указанного, каждый компонент вырезаются отдельно. \en NO TRANSLATION.
    friend inline const DoubleSize operator-(const DoubleSize&, const DoubleSize&);
    /// \ru Умножить данный размер на заданное значение и возвратить результат. \en NO TRANSLATION.
    friend inline const DoubleSize operator*(const DoubleSize&, double);
    /// \ru Умножить данный размер на заданное значение и возвратить результат. \en NO TRANSLATION. 
    friend inline const DoubleSize operator*(double, const DoubleSize&);
    /// \ru Делит заданный размер на заданный делитель и возвратить результат. \en NO TRANSLATION. 
    friend inline const DoubleSize operator/(const DoubleSize&, double);
private:
  double m_dbWidth;   ///< \ru Ширина размера. \en NO TRANSLATION.
  double m_dbHeight;  ///< \ru Высота размера. \en NO TRANSLATION.
};

inline DoubleSize::DoubleSize() : m_dbWidth(-1.0), m_dbHeight(-1.0) {}

inline DoubleSize::DoubleSize(const Size& sz) : m_dbWidth(sz.GetWidth()), m_dbHeight(sz.GetHeight()) {}

inline DoubleSize::DoubleSize(double w, double h) : m_dbWidth(w), m_dbHeight(h) {}

inline bool DoubleSize::IsNull() const
{ return IsEqual(m_dbWidth, 0.0, 0.00001) && IsEqual(m_dbHeight, 0.0, 0.00001); }

inline bool DoubleSize::IsEmpty() const
{ return m_dbWidth <= 0.0 || m_dbHeight <= 0.0; }

inline bool DoubleSize::IsValid() const
{ return m_dbWidth >= 0.0 && m_dbHeight >= 0.0; }

inline double DoubleSize::GetWidth() const 
{ return m_dbWidth; }

inline double DoubleSize::GetHeight() const
{ return m_dbHeight; }

inline void DoubleSize::SetWidth(double w)
{ m_dbWidth = w; }

inline void DoubleSize::SetHeight(double h)
{ m_dbHeight = h; }

inline double& DoubleSize::InitWidth()
{ return m_dbWidth; }

inline double& DoubleSize::InitHeight()
{ return m_dbHeight; }

inline DoubleSize &DoubleSize::operator+=(const DoubleSize &s)
{ m_dbWidth += s.m_dbWidth; m_dbHeight += s.m_dbHeight; return *this; }

inline DoubleSize &DoubleSize::operator-=(const DoubleSize &s)
{ m_dbWidth -= s.m_dbWidth; m_dbHeight -= s.m_dbHeight; return *this; }

inline DoubleSize &DoubleSize::operator *= (double c)
{ m_dbWidth *= c; m_dbHeight *= c; return *this; }

inline bool operator==(const DoubleSize& s1, const DoubleSize& s2)
{ return IsEqual(s1.m_dbWidth, s2.m_dbWidth, 0.00001) && IsEqual(s1.m_dbHeight, s2.m_dbHeight, 0.00001); }

inline bool operator!=(const DoubleSize &s1, const DoubleSize &s2)
{ return !IsEqual(s1.m_dbWidth, s2.m_dbWidth, 0.00001) || !IsEqual(s1.m_dbHeight, s2.m_dbHeight, 0.00001); }

inline const DoubleSize operator+(const DoubleSize& s1, const DoubleSize& s2)
{ return DoubleSize(s1.m_dbWidth + s2.m_dbWidth, s1.m_dbHeight + s2.m_dbHeight); }

inline const DoubleSize operator-(const DoubleSize& s1, const DoubleSize& s2)
{ return DoubleSize(s1.m_dbWidth-s2.m_dbWidth, s1.m_dbHeight-s2.m_dbHeight); }

inline const DoubleSize operator*(const DoubleSize &s, double c)
{ return DoubleSize(s.m_dbWidth*c, s.m_dbHeight*c); }

inline const DoubleSize operator*(double c, const DoubleSize &s)
{ return DoubleSize(s.m_dbWidth * c, s.m_dbHeight * c); }

inline DoubleSize &DoubleSize::operator/=(double c)
{
  PRECONDITION(!IsEqual(c, 0.0, 0.00001));
  m_dbWidth = m_dbWidth / c; m_dbHeight = m_dbHeight / c;
  return *this;
}

inline const DoubleSize operator/(const DoubleSize& s, double c)
{
  PRECONDITION(!IsEqual(c, 0.0, 0.00001));
  return DoubleSize(s.m_dbWidth / c, s.m_dbHeight / c);
}

VSN_END_NAMESPACE

#endif // __VSN_SIZE_H
