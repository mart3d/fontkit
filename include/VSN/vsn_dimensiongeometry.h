﻿////////////////////////////////////////////////////////////////////////////////
/** 
  \file
  \brief \ru NO TRANSLATION.
         \en NO TRANSLATION. \~

*/
////////////////////////////////////////////////////////////////////////////////

#ifndef __VSN_DIMENSIONGEOMETRY_H
#define __VSN_DIMENSIONGEOMETRY_H

#include <mb_cart_point3d.h>
#include <surf_plane.h>
#include "vsn_meshgeometry.h"
#include "vsn_global.h"

VSN_BEGIN_NAMESPACE

#define UNITS_LENGTH_MM  "mm"
#define UNITS_LENGTH_M   "m"
#define UNITS_ANGLE_RAD  "rad"
#define UNITS_ANGLE_GRAD "grad"

class DimensionGeometryPrivate;
//------------------------------------------------------------------------------
/** \brief \ru NO TRANSLATION.
           \en NO TRANSLATION. \~
  \details \ru NO TRANSLATION. \n.
           \en NO TRANSLATION. \n \~
  \ingroup NO GROUP
*/
// ---
class VSN_CLASS DimensionGeometry : public MeshGeometry
{
    VSN_OBJECT(DimensionGeometry)
    VSN_ENUM(DimensionGeometry::TextOrientation)
    VSN_ENUM(DimensionGeometry::TextVPosition)
    VSN_ENUM(DimensionGeometry::TextHPosition)
    VSN_PROP_READ_WRITE_NOTIFY(orientation,  GetTextOrientation, SetTextOrientation, TextOrientationModified)
    VSN_PROP_READ_WRITE_NOTIFY(vertPosition, GetTextVPosition,   SetTextVPosition,   TextVPositionModified)
    VSN_PROP_READ_WRITE_NOTIFY(horzPosition, GetTextHPosition,   SetTextHPosition,   TextHPositionModified)
protected:
    /// \ru Конструктор по умолчанию. \en Default constructor. \~
    DimensionGeometry();
public:
    /// \ru Деструктор. \en Destructor. \~
    virtual ~DimensionGeometry();
public:
    enum ValueType
    {
        vt_Calculated,
        vt_UserDouble,
        vt_UserText
    };

    enum TextOrientation
    {
        to_Freeze,     ///< \ru В плоскости размера. \en NO TRANSLATION. \~
        to_ScreenOnly, ///< \ru В плоскости экрана. \en NO TRANSLATION. \~
        to_Readable,   ///< \ru В плоскости размера с доворотом по вектору взгляда. \en NO TRANSLATION. \~
    };

    ///< \ru Положение текста для горизонтали. \en NO TRANSLATION. \~
    enum TextHPosition
    {
        hpos_Left,
        hpos_Right,
        hpos_Center,
        hpos_Auto
    };
    ///< \ru Положение текста по вертикали. \en NO TRANSLATION. \~
    enum TextVPosition 
    {
        vpos_Above,
        vpos_Below,
        vpos_Center
    };
    ///< \ru Ориентация стрелок размера. \en NO TRANSLATION. \~
    enum ArrowOrientation
    {
        orient_Internal,
        orient_External,
        orient_Auto
    };
public:
    /// \ru Вернуть ориентацию текста на размерной линии. \en NO TRANSLATION. \~
    DimensionGeometry::TextOrientation GetTextOrientation() const;
    /// \ru Получает вертикальное выравнивание текста. \en NO TRANSLATION. \~
    TextVPosition GetTextVPosition() const;
    /// \ru Получает горизонтальное выравнивание текста. \en NO TRANSLATION. \~
    TextHPosition GetTextHPosition() const;

    /// \ru Вернуть true, если размер полностью определен. \en NO TRANSLATION. \~
    bool IsValid() const;
    /// \ru Вернуть цвет текста размера. \en NO TRANSLATION. \~
    Color GetColorLabel() const;
    /// \ru Установить цвет текста размера. \en NO TRANSLATION. \~
    void SetColorLabel(const Color& color);

    /// \ru Вернуть высоту выносной линии. \en NO TRANSLATION. \~
    double GetExtensionLineHeight() const;
    /// \ru Установить высоту выносной линии. \en NO TRANSLATION. \~
    void SetExtensionLineHeight(double extHeight);

    /// \ru NO TRANSLATION. \en NO TRANSLATION. \~
    double GetValue() const;

    /// \ru NO TRANSLATION. \en NO TRANSLATION. \~
    static void SetDefaultFont(const String& str);
public:
    /// \ru Установить ориентацию текста на размерной линии. \en NO TRANSLATION. \~
    VSN_SLOT(Public, SetTextOrientation, void SetTextOrientation(TextOrientation orientation))
    /// \ru Устанавливает вертикальное выравнивание текста. \en NO TRANSLATION. \~
    VSN_SLOT(Public, SetTextVPosition,   void SetTextVPosition(TextVPosition vertPosition))
    /// \ru Устанавливает горизонтальное выравнивание текста. \en NO TRANSLATION. \~
    VSN_SLOT(Public, SetTextHPosition, void SetTextHPosition(TextHPosition horzPosition))
public:
    VSN_SIGNAL(Public, TextOrientationModified, void TextOrientationModified(TextOrientation orientation), orientation)
    VSN_SIGNAL(Public, TextVPositionModified,   void TextVPositionModified(TextVPosition vertPosition),    vertPosition)
    VSN_SIGNAL(Public, TextHPositionModified,   void TextHPositionModified(TextHPosition horzPosition),    horzPosition)
public:
    /// \ru Получить плоскость размера в которой вычисляется представление 2D - размера. \en NO TRANSLATION. \~
    const MbPlane& GetPlane() const;
    /// \ru Установить плоскость размера в которой будет вычисляется представление 2D - размера. \en NO TRANSLATION. \~
    virtual void SetPlane(const MbPlane& plane);

    /// \ru Выдать габаритный куб. \en Get a bounding box. \~
    virtual const MbCube& GetBoundingBox() override;

    /// \ru Вернуть единицы измерения по умолчанию.(единицы являются модельные для правильной конвертации) \en NO TRANSLATION. \~
    virtual String GetDefaultUnits() const;
    /// \ru Установить единицы измерения по умолчанию. (единицы являются модельные для правильной конвертации) \en NO TRANSLATION. \~
    virtual void SetDefaultUnits(const String& units);

    /// \ru Вернуть единицы измерения для вывода. \en NO TRANSLATION. \~
    virtual String GetOutputUnits() const;
    /// \ru Установить единицы измерения для вывода. \en NO TRANSLATION. \~
    virtual void SetOutputUnits(const String& units);

    /// \ru Вернуть true, если положение текста задано пользователем с помощью функции SetTextPosition. \en NO TRANSLATION. \~
    bool IsTextPositionUser() const;
    /// \ru Получить позицию текста 2D - размера. \en NO TRANSLATION. \~
    virtual const MbCartPoint3D GetTextPosition() const;
    /// \ru Установить позицию текста 2D - размера. \en NO TRANSLATION. \~
    virtual void SetTextPosition(const MbCartPoint3D& textPos);
    /// \ru Вернуть вычисленное значение измерения. \en NO TRANSLATION. v
    virtual double CalculateValue() const;
protected:
    /// \ru Конструктор с параметрами. \en NO TRANSLATION. \~
    DimensionGeometry(DimensionGeometryPrivate& dd, Node* pParent);
private:
    /// \ru Отрисовать с помощью OpenGL. \en Draw a box with OpenGL. \~
    virtual void OpenGLDraw(const RenderState& state) override;
protected:
    VSN_DECLARE_EX_PRIVATE(DimensionGeometry);
};

VSN_END_NAMESPACE

#endif /* __VSN_DIMENSIONGEOMETRY_H */
