﻿////////////////////////////////////////////////////////////////////////////////
/**
\file
    \brief \ru NO TRANSLATION
           \en NO TRANSLATION. \~
*/
////////////////////////////////////////////////////////////////////////////////
#ifndef __VSN_OBJECTPICKERSELECTION_H
#define __VSN_OBJECTPICKERSELECTION_H

#include "vsn_absvisioncomponent.h"
#include "vsn_processevent.h"
#include <name_item.h>

VSN_BEGIN_NAMESPACE
//------------------------------------------------------------------------------
/** \brief   \ru NO TRANSLATION.
             \en NO TRANSLATION. \~
    \details \ru NO TRANSLATION. \n
             \en NO TRANSLATION. \n \~
    \ingroup 
*/
// ---
class VSN_CLASS PickSelectionEvent
{
public:
    enum TypeObject { None, Vertex, Edge, Face };
public:
    /// \ru Конструктор по умолчанию. \en Default constructor.
    PickSelectionEvent();
    /// \ru Конструктор с параметрами. \en NO TRANSLATION. \~
    PickSelectionEvent(const NodeKey& key, int geometryIdx, NodeKey geometryKey, uint primitiveId);
    /// \ru Конструктор копирования. \en NO TRANSLATION. \~
    PickSelectionEvent(const PickSelectionEvent& other);
public:
    /// \ru Вернуть тип объекта. \en NO TRANSLATION. \~
    TypeObject GetTypeObject() const;
    /// \ru Установить тип объекта. \en NO TRANSLATION. \~
    void SetTypeObject(TypeObject type);

    /// \ru Вернуть уникальный ключь сегмента сцены. \en NO TRANSLATION. \~
    NodeKey GetEssenceKey() const;
    /// \ru Вернуть индекс геометрии. \en NO TRANSLATION. \~
    uint GetGeometryIdx() const;
    /// \ru Вернуть идентификатор геометрии. \en NO TRANSLATION. \~
    NodeKey GetGeometryKey() const;
    /// \ru Вернуть уникальный идентификатор примитива. \en NO TRANSLATION. \~
    uint GetPrimitiveId() const;

    /// \ru Вернуть уникальный путь до математического объекта. \en NO TRANSLATION. \~
    MbPath GetMathPath() const;
    /// \ru Установить уникальный путь до математического объекта. \en NO TRANSLATION. \~
    void SetMathPath(const MbPath& path);
    /// \ru Вернуть уникальный идентификатор математического объекта. \en NO TRANSLATION. \~
    uint32 GetMathPrimitive() const;
    /// \ru Установить уникальный идентификатор математического объекта. \en NO TRANSLATION. \~
    void SetMathPrimitive(uint32 id);
    /// \ru Оператор копирования. \en Copy operator.
    PickSelectionEvent & operator = (const PickSelectionEvent& other);

    /// \ru True, если клавиша Ctrl сейчас нажата. \en True if Ctrl key is currently pressed.
    bool Control() const { return(m_flags & PR_CONTROL) != 0; }
    /// \ru True, если клавиша Shift сейчас нажата. \en True if Shift key is currently pressed.
    bool Shift() const { return(m_flags & PR_SHIFT) != 0; }
    /// \ru True, если клавиша Alt сейчас нажата. \en True if Alt key is currently pressed.
    bool Alt() const { return(m_flags & PR_ALT) != 0; }
    /// \ru True, если левая клавиша Alt нажата. \en True if the left Alt key is pressed.
    bool LeftAlt() const { return(m_flags & PR_LEFT_ALT) != 0; }
    /// \ru True, если левая клавиша Ctrl нажата. \en True if the left Ctrl key is pressed.
    bool LeftControl() const { return(m_flags & PR_LEFT_CONTROL) != 0; }
    /// \ru True, если левая клавиша Shift нажата. \en True if the left Shift key is pressed.
    bool LeftShift() const { return(m_flags & PR_LEFT_SHIFT) != 0; }
    /// \ru True, если правая клавиша Alt нажата. \en True if the right Alt key is pressed.
    bool RightAlt() const { return(m_flags & PR_RIGHT_ALT) != 0; }
    /// \ru True, если правая клавиша Ctrl нажата. \en True if the right Ctrl key is pressed.
    bool RightControl() const { return(m_flags & PR_RIGHT_CONTROL) != 0; }
    /// \ru True, если правая клавиша Shift нажата. \en True if the right Shift key is pressed.
    bool RightShift() const { return(m_flags & PR_RIGHT_SHIFT) != 0; }

    /// \ru True, если левая кнопка мыши нажата. \en True if the left mouse button is pressed.
    bool LButton() const { return(m_flags & PR_LBUTTON) != 0; }
    /// \ru True, если средняя кнопка мыши нажата. \en True if the middle mouse button is pressed.
    bool MButton() const { return(m_flags & PR_MBUTTON) != 0; }
    /// \ru True, если правая кнопка мыши нажата. \en True if the right mouse button is pressed.
    bool RButton() const { return(m_flags & PR_RBUTTON) != 0; }

    /// \ru Возвращает бит ProcessEvent флагов, таких как PR_LBUTTON, для специальных событий клавиш Shift, Ctrl и левая, средняя и правая кнопки мыши. \en Returns ProcessEvent bit flags like PR_LBUTTON for special events of Shift and Ctrl keys as well as left, middle and right mouse buttons. \~
    unsigned int GetFlags() const { return m_flags; }
    /** \brief \ru Этот метод устанавливает побитовые флаги, которые показывают состояние кнопок мыши, клавиш Shift и Ctrl.
               \en This method sets bitwise flags that shows the state of mouse buttons, Shift and Ctrl keys. \~
        \param[in] flags - \ru Побитовые флаги для состояния мыши и клавиш. Например, если и клавиша Shift, и левая кнопка мыши нажаты, пропустить PR_SHIFT|PR_LBUTTON.
                           \en Bitwise flags for mouse and key states. For example, if both Shift key and left mouse button are pressed, pass PR_SHIFT|PR_LBUTTON. \~
    */
    void SetFlags(int flags) { m_flags = flags; }

    /// \ru Вернуть координаты курсора. \en NO TRANSLATION. \~
    IntPoint GetCursorPos() const;
    /// \ru Установить координаты курсора. \en NO TRANSLATION. \~
    void SetCursorPos(const IntPoint& pnt);
protected:
    IntPoint m_cursorPos;       ///< \ru Кординаты курсора. \en NO TRANSLATION.
    TypeObject m_typeObject;    ///< \ru Тип объекта. \en NO TRANSLATION.
    NodeKey m_essenceKey;       ///< \ru Уникальный ключь сегмента сцены. \en NO TRANSLATION.
    uint m_geometryIdx;         ///< \ru Индекс геометрии. \en NO TRANSLATION.
    NodeKey m_geometryKey;      ///< \ru Идентификатор геометрии. \en NO TRANSLATION.
    uint m_primitiveId;         ///< \ru Уникальный идентификатор примитива. \en NO TRANSLATION. 

    /// \ru Данные для математического объекта. \en NO TRANSLATION.
    MbPath m_mathPath;          ///< \ru Уникальный путь до объекта. \en NO TRANSLATION. 
    uint32 m_mathPrimitiveId;   ///< \ru Уникальный идентифакатор примитива. \en NO TRANSLATION. 
    unsigned int m_flags;       ///< \ru Флаги показывают, какая из кнопок мыши нажата и нажаты ли клавиши Shift и Ctrl на клавиатуре. \en Flags indicate which of mouse buttons is pressed and if Shift and Ctrl keyboard keys are pressed.
};

typedef std::shared_ptr<PickSelectionEvent> PickSelectionEventPtr;


class ObjectPickSelectionPrivate;
//------------------------------------------------------------------------------
/** \brief   \ru NO TRANSLATION.
             \en NO TRANSLATION. \~
    \details \ru NO TRANSLATION. \n
             \en NO TRANSLATION. \n \~
    \ingroup 
*/
// ---
class VSN_CLASS ObjectPickSelection : public AbsVisionComponent
{
    VSN_OBJECT(ObjectPickSelection);
    VSN_PROP_READ_WRITE_NOTIFY(hoverEnabled, IsHoverEnabled, SetHoverEnabled, HoverEnabledModified)
public:
    /// \ru Конструктор по умолчанию. \en Default constructor. \~
    explicit ObjectPickSelection(Object* pParent = nullptr);
    /// \ru Деструктор. \en Destructor. \~
    virtual ~ObjectPickSelection();
public:
    bool IsHoverEnabled() const;
public:
    VSN_SLOT(Public, SetHoverEnabled, void SetHoverEnabled(bool hoverEnabled))
public:
    /// \ru NO TRANSLATION. \en NO TRANSLATION. \~
    VSN_SIGNAL(Public, ObjectPressed, void ObjectPressed(std::shared_ptr<PickSelectionEvent> pPick), pPick)
    /// \ru NO TRANSLATION. \en NO TRANSLATION. \~
    VSN_SIGNAL(Public, ObjectHoverMove, void ObjectHoverMove(std::shared_ptr<PickSelectionEvent> pPick), pPick)
    /// \ru NO TRANSLATION. \en NO TRANSLATION. \~
    VSN_SIGNAL(Public, ObjectReleased, void ObjectReleased(std::shared_ptr<PickSelectionEvent> pPick), pPick)
public:
    VSN_SIGNAL(Public, HoverEnabledModified, void HoverEnabledModified(bool bHoverEnabled), bHoverEnabled)
protected:
    virtual void OnInstalled() final;
    virtual void OnUninstalled() final;
private:
    VSN_DISABLE_COPY(ObjectPickSelection);
    VSN_DECLARE_EX_PRIVATE(ObjectPickSelection);
};

VSN_END_NAMESPACE

#endif // __VSN_OBJECTPICKERSELECTION_H
