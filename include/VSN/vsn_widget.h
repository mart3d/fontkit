﻿#ifndef __WIDGET_H
#define __WIDGET_H


#include "vsn_events.h"
#include "vsn_node.h"
#include "vsn_color.h"

#include "vsn_global.h"

class MbMesh;

VSN_BEGIN_NAMESPACE

class RenderObject;
class EventHandler;
///////////////////////////////////////////////////////////////////////////////
// 
// базовый класс для реализации виджитов
// 
///////////////////////////////////////////////////////////////////////////////
class VSN_CLASS Widget : public Node
{
public:
    // конструктор по умолчанию
    Widget( EventHandler* pEventHandler = V_NULL );
    // деструктор
    virtual ~Widget();

public:
    // установить указатель на менеджер виджетов
    void SetWidgetManager( EventHandler* pEventHandler );
    // обновить предсавление виджета
    virtual void UpdateWidgetRep();
    // показать/погасить виджит
    void SetVisible( bool bVisible );

protected:
    // создать отрисовочный объект представления виджита
    virtual void CreateViewObject3D() = 0;

    // вернуть указатель на компонент виджита по индексу
    RenderObject* GetViewObject3D( int index );

    // добавить отрисовочный объект
    void AddViewObject3D( const RenderObject& object );
    // удалить отрисовочный объект
    void RemoveViewObject3D();

    // сбросить состояние виджита
    virtual void ResetViewObjectState() = 0;

protected:
    EventHandler* m_pEventHandler; /// указатель на менеджер
    std::vector<NodeKey> m_objectsId; /// массив идентификаторов объектов этого виджита
};

///////////////////////////////////////////////////////////////////////////////
// 
// класс Placement3D представляет виджет местоположения
// 
///////////////////////////////////////////////////////////////////////////////
class VSN_CLASS Placement3D : public Widget
{
public:
    // конструктор по умолчанию
    Placement3D( EventHandler* pEventHandler = V_NULL );
    // деструктор
    virtual ~Placement3D();

public:
    // установить местоположение виджита
    void SetPosition( const MbCartPoint3D& origin );
    // вернуть местоположение виджита
    const MbCartPoint3D& GetPosition() const; 
    public:
    // обновить предсавление виджета
    virtual void UpdateWidgetRep();

protected:
    // создать отрисовочный объект представления виджета
    virtual void CreateViewObject3D();
    // сбросить состояние виджита
    virtual void ResetViewObjectState(){}

private:
    bool BuildAxisSymbol( RenderObject& mesh, Color& color, char symbol );
    // переместить представление виджета
    void MoveObjectRep( const MbCartPoint3D& pos );

protected:
    MbCartPoint3D m_origin; /// местоположения
    double m_scaleFactor;   /// масштабный коэффициент
    double m_axisLength;    /// длина оси
    double m_axisRadius;    /// радиус оси
};

VSN_END_NAMESPACE

#endif /* __WIDGET_H */
