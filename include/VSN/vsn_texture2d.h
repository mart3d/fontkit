﻿#ifndef __VSN_TEXTURE2D_H
#define __VSN_TEXTURE2D_H

#include <vsn_string.h>
#include <vsn_global.h>
#include <vsn_size.h>
#include <vsn_image.h>

#include <string>



VSN_BEGIN_NAMESPACE

// Texture filter
enum class TextureFilter
{
    Nearest,
    // bilinear filtering
    Linear,
    //Trilinear filtering using mipmaps
    NearestNearest,
    NearestLinear,
    LinearNearest,
    LinearLinear,
};

// Texture wrap value
enum class TextureWrap
{
    Repeat,
    ClampToEdge
};

// a texel's format
enum class TextureFormat
{
    BGR,
    BGRA,
};

class OpenGLContextInterface;
class Texture2DPrivate;
class Image;
///////////////////////////////////////////////////////////////////////////////
//
// класс Texture2D представляет текстуру изображения
// текстура изображения определяют текстурной картой в 2D системе координат
// 
///////////////////////////////////////////////////////////////////////////////
class VSN_CLASS Texture2D
{
public:
    // конструктор по умолчанию
    Texture2D();
    // конструктор копирования
    Texture2D(const Texture2D& other);
    // конструктор для загрузки текструы 
    Texture2D(const Image &);
    // деструктор
    virtual ~Texture2D();

public:
    void OGLBindTexture();

public:
    void  Init(const Image &);
    void  Init(TextureFormat format, Size size, const unsigned char * pBits);

    uint  GetWidth() const;
    uint  GetHeight() const;
    // вернуть идентификатор текстуры
    uint  GetUniqueKey() const;

    bool HasAlphaChannel() const;
    // вернуть true, если текстура загружена
    bool IsLoaded() const;

    Image ToImage() const;

public: // texture parameters
    void SetFilters(TextureFilter min, TextureFilter max);
    void SetAnisotropic(float value);
    void SetWrap(TextureWrap s, TextureWrap t);

public:
    // return maximum value of anisotropic filter
    static float GetMaxAnisotropic();
    static Size  GetMaxTextureSize();
public:
    // оператор копирования
    Texture2D& operator = (const Texture2D& other);
    // оператор тождества
    bool operator == (const Texture2D& other) const;
private:
    VSN_DECLARE_PRIVATE(Texture2D)
};
VSN_END_NAMESPACE



#endif /* __VSN_TEXTURE2D_H */
