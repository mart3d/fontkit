﻿#ifndef __VSN_MIDVECTOR_H
#define __VSN_MIDVECTOR_H

#include <vector>
#include "vsn_global.h"


VSN_BEGIN_NAMESPACE

namespace Private
{
	enum CutResult
	{
		Null,
		Empty,
		Full,
		Subset,
	};

	static CutResult cutMid(int originalLength, int *_position, int *_length)
	{
		int &position = *_position;
		int &length = *_length;
		if (position > originalLength)
			return Null;

		if (position < 0) {
			if (length < 0 || length + position >= originalLength)
				return Full;
			if (length + position <= 0)
				return Null;
			length += position;
			position = 0;
		}
		else if (uint(length) > uint(originalLength - position)) {
			length = originalLength - position;
		}

		if (position == 0 && length == originalLength)
			return Full;

		return length > 0 ? Subset : Empty;
	}

	template <typename T>
	void copyConstruct(const T *srcFrom, const T *srcTo, T *dstFrom)
	{
		::memcpy(static_cast<void *>(dstFrom), static_cast<const void *>(srcFrom), (srcTo - srcFrom) * sizeof(T));
	}

	template <typename T>
	extern std::vector<T> midVector(const std::vector<T>& d, int pos, int len)
	{
		switch (cutMid(static_cast<int>(d.size()), &pos, &len))
		{
		case Null:
		case Empty:
			return std::vector<T>();
		case Full:
			return d;
		case Subset:
			break;
		}

		std::vector<T> midResult;
		midResult.resize(len);
    typename std::vector<T>::const_iterator itFrom = d.cbegin() + pos;
    typename std::vector<T>::const_iterator itTo = d.cbegin() + pos + len;
		T srcFrom = (*itFrom);
		T srcTo = (*itTo);
		copyConstruct(&srcFrom, &srcTo, midResult.data());
		//		midResult.resize(len);
		return midResult;
	}
}

VSN_END_NAMESPACE

#endif // __VSN_MIDVECTOR_H
