﻿////////////////////////////////////////////////////////////////////////////////
/**
\file
    \brief \ru AbsInputDevice - это базовый класс используемый Vision для взаимодействия с произвольными устройствами ввода.
           \en NOT TRANSLATED. \~
*/
////////////////////////////////////////////////////////////////////////////////
#ifndef __VSN_ABSPHYSICALDEVICE_H
#define __VSN_ABSPHYSICALDEVICE_H

#include "vsn_node.h"
#include "vsn_global.h"

VSN_BEGIN_NAMESPACE

class CoordTuning;
class InputDevicesVision;
class AbsInputDevicePrivate;

//------------------------------------------------------------------------------
/** \brief  \ru Базовый класс используемый Vision для взаимодействия с произвольными устройствами ввода.
            \en NOT TRANSLATED. \~
    \ingroup Vision_Input
*/
// ---
class VSN_CLASS AbsInputDevice : public Node
{
    VSN_OBJECT(AbsInputDevice);
public:
    /// \ru Конструктор по умолчанию. \en NOT TRANSLATED.
    explicit AbsInputDevice(Node* pParent = nullptr);
    /// \ru Деструктор. \en NOT TRANSLATED.
    ~AbsInputDevice();
public:
    /// \ru Вернуть количество координат, которое имеет это устройство. \en NOT TRANSLATED. \~
    virtual int GetCoordinateCount() const;
    /// \ru Вернуть количество кнопок, которые есть у этого устройства. \en NOT TRANSLATED. \~
    virtual int GetButtonCount() const;
    /// \ru Вернуть список имен координат устройства. \en NOT TRANSLATED. \~
    virtual std::vector<String> GetCoordinateNames() const;
    /// \ru Вернуть список имен кнопок устройства. \en NOT TRANSLATED. \~
    virtual std::vector<String> GetButtonNames() const;

    /// \ru Вернуть целочисленный идентификатор координаты или - 1, если он не существует на этом устройстве. \en NOT TRANSLATED. \~
    virtual int GetCoordinateId(const String& name) const;
    /// \ru Вернуть целочисленный идентификатор имени кнопки или -1, если он не существует на этом устройстве. \en NOT TRANSLATED. \~
    virtual int GetButtonId(const String& name) const;
public:
    /// \ru Добавьте ось axisSetting к этому устройству. \en NOT TRANSLATED. \~
    void AddCoordinateSetting(CoordTuning* pCoordSetting);
    /// \ru Удалите функцию axisSetting для этого устройства. \en NOT TRANSLATED. \~
    void RemoveCoordinateSetting(CoordTuning* pCoordSetting);
    /// \ru Вернуть привязки axisSettings связанные с этим устройством. \en NOT TRANSLATED. \~
    std::vector<CoordTuning*> GetCoordinateSettings() const;
protected:
    /// \ru Конструктор для внутреннего использования. \en NOT TRANSLATED. \~
    AbsInputDevice(AbsInputDevicePrivate& dd, Node* pParent = nullptr);
    virtual NCreatedModificationBasicPtr CreateNodeModification() const override;
protected:
    VSN_DECLARE_EX_PRIVATE(AbsInputDevice);
};

VSN_END_NAMESPACE


#endif // __VSN_ABSPHYSICALDEVICE_H

