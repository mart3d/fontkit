﻿////////////////////////////////////////////////////////////////////////////////
/** 
  \file
  \brief \ru NOT TRANSLATED.
         \en NOT TRANSLATED. \~

*/
////////////////////////////////////////////////////////////////////////////////
#ifndef __VSN_GEOMETRY_H
#define __VSN_GEOMETRY_H

#include "vsn_node.h"
#include "vsn_global.h"

VSN_BEGIN_NAMESPACE

class GeometryTraits;
class GeometryPrivate;
//------------------------------------------------------------------------------
/** \brief \ru NOT TRANSLATED.
           \en NOT TRANSLATED. \~
  \details \ru NOT TRANSLATED.
           \en NOT TRANSLATED. 

    \ingroup Vision_Geometry
*/
// ---
class VSN_CLASS Geometry : public Node
{
    VSN_OBJECT(Geometry);
public:
    /// \ru Конструктор по умолчанию. \en Default constructor. 
    Geometry(Node* pParent = nullptr);
    /// \ru Конструктор. \en Constructor.
    Geometry(NodeKey id, const String& name);
    /// \ru Деструктор. \en Destructor. 
    virtual ~Geometry();
public:
    std::vector<GeometryTraits*> GetGeometryTraits() const;
    void AddGeometryTraits(GeometryTraits* pTraits);
    void RemoveGeometryTraits(GeometryTraits* pTraits);
public:
    /// \ru Обновить число прозрачных материалов после операций с таблицей. \en Updates transparent material count after table operations.
    virtual void UpdateTransparentMaterialCount();
    /// \ru Обновить геометрию по новым данным. \en NO TRANSLATION.
    virtual void UpdateGeometry();
public:
    VSN_SIGNAL(Public, GeometryDestroyed, void GeometryDestroyed(Geometry* pGeometry), pGeometry)
protected:
    /// \ru Конструктор для внутреннего использования. \en NO TRANSLATION.
    Geometry(GeometryPrivate& dd, Node* pParent);
private:
    virtual NCreatedModificationBasicPtr CreateNodeModification() const override;
private:
    VSN_DISABLE_COPY(Geometry);
    VSN_DECLARE_EX_PRIVATE(Geometry);
};

VSN_END_NAMESPACE

#endif /* __VSN_GEOMETRY_H */
