﻿////////////////////////////////////////////////////////////////////////////////
/** 
  \file
  \brief \ru Класс MathRepresentation является вспомогательным классом и представляет 
             API для управления геометрией.
         \en NO TRANSLATION. \~

*/
////////////////////////////////////////////////////////////////////////////////

#ifndef __VSN_MATHREPRESENTATION_H
#define __VSN_MATHREPRESENTATION_H

#include "vsn_geometryrep.h"
#include "vsn_global.h"

class MbItem;

VSN_BEGIN_NAMESPACE
class MathRepresentationPrivate;
//------------------------------------------------------------------------------
/** \brief \ru Класс MathRepresentation является вспомогательным классом и представляет API для управления геометрией.
           \en NO TRANSLATION. \~
  \details \ru MathRepresentation создает MathGeometry и содержит его указатель на протяжении всей своей жизни.
               MathRepresentation представляет ряд полезных функций для управления своеей геометрией. \n.
           \en NO TRANSLATION. \n \~
  \ingroup Vision_Geometry
*/
// ---
class VSN_CLASS MathRepresentation : public GeometryRep
{
    VSN_OBJECT(MathRepresentation);
    VSN_PROP_READ_WRITE_NOTIFY(visualSag, GetVisualSag, SetVisualSag, VisualSagModified);
public:
    /// \ru Конструктор по умолчанию. \en Default constructor. \~
    MathRepresentation(Node* pParent = nullptr);
    /// \ru Деструктор. \en Destructor. 
    virtual ~MathRepresentation();
public:
    /// \ru Вернуть точность для вычисления шага при триангуляции поверхностей и граней. \en NO TRANSLATION. \~
    double GetVisualSag() const;

    /// \ru Вернуть  указатель на математическое точное представление MbItem. \en NO TRANSLATION. \~ 
    const MbItem* GetMathItem() const;
    /// \ru Установить указатель на математическое точное представление MbItem. \en NO TRANSLATION. \~ 
    void SetMathItem(const MbItem* pItem, double sag = Math::visualSag);
public:
    /// \ru Установить точность для вычисления шага при триангуляции поверхностей и граней. \en NO TRANSLATION. \~ 
    VSN_SLOT(Public, SetVisualSag, void SetVisualSag(double sag))
public:
    /// \ru Сигнал модификации шага расчета триангуляции. \en NO TRANSLATION. \~
    VSN_SIGNAL(Public, VisualSagModified, void VisualSagModified(double sag), sag)
    /// \ru Сигнал об окончании перестроения всей геометрии. \en NO TRANSLATION. \~
    VSN_SIGNAL(Public, BuildCompleted, void BuildCompleted())
protected:
    /// \ru Эта функция вызывается, если произошли какие-либо изменения в сцене (для внутреннего использования). \en The function is called in case of scene being modified. \~
    virtual void SceneModificationEvent(const std::shared_ptr<SceneModification>& modification) override;
private:
    VSN_DECLARE_EX_PRIVATE(MathRepresentation);
};
VSN_END_NAMESPACE

#endif /* __VSN_MATHREPRESENTATION_H */
