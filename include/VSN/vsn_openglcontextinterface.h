﻿////////////////////////////////////////////////////////////////////////////////
/**
  \file
  \brief \ru NO TRANSLATION.
         \en NO TRANSLATION. \~
*/
////////////////////////////////////////////////////////////////////////////////
#ifndef __VSN_OPENGLCONTEXTINTERFACE_H
#define __VSN_OPENGLCONTEXTINTERFACE_H

#include <list>
#include <mb_matrix3d.h>
#include "vsn_renderingareaformat.h"
#include "vsn_global.h"

VSN_BEGIN_NAMESPACE

class OpenGLContextInterface;
class OpenGLContextContainerPrivate;
//------------------------------------------------------------------------------
/** \brief \ru NO TRANSLATION.
           \en NO TRANSLATION. \~
    \details \ru .
             \en NO TRANSLATION. \n \~
    \ingroup Vision_OpenGL
*/
// ---
class VSN_CLASS OpenGLContextContainer
{
public:
    OpenGLContextContainer();
    ~OpenGLContextContainer();
public:
    /// \ru Вернуть указатель на OpenGLContextContainer соответствующий текущему контексту. \en NO TRANSLATION.
    static OpenGLContextContainer* GetCurrentContextContainer();
public:
    std::list<OpenGLContextInterface*> GetContextList() const;
    void AddContext(OpenGLContextInterface* pCtx);
    void RemoveContext(OpenGLContextInterface* pCtx);
private:
    VSN_DECLARE_PRIVATE(OpenGLContextContainer)
};

class RenderingArea;
class OpenGLFunctionListInterface;
class OpenGLExtraFunctionListInterface;
class OpenGLContextInterfacePrivate;
//------------------------------------------------------------------------------
/** \brief \ru NO TRANSLATION.
            \en  NO TRANSLATION. \~
    \details \ru .
             \en NO TRANSLATION. \n \~
    \ingroup Vision_OpenGL
*/
// ---
class VSN_CLASS OpenGLContextInterface
{
public:
    /// \ru Деструктор. \en Destructor.
    virtual ~OpenGLContextInterface();
protected:
    OpenGLContextInterface();
public:
    /// \ru Создать OpenGL контекст и вернуть указатель на его интерфейс. \en NO TRANSLATION.
    static OpenGLContextInterface* CreateOpenGLContext(const RenderingAreaFormat& format);
public:
    /// \ru Делает текущий контекст указанной поверхности. Возвращает true, если успешно. \en NO TRANSLATION.
    virtual bool MakeCurrent();
    /// \ru Делает текущий контекст указанной поверхности. Возвращает true, если успешно. \en NO TRANSLATION.
    virtual bool MakeCurrent(RenderingArea* pFrame);
    /// \ru Функция для удобства делает вызов makeCurrent с 0 поверхностью. \en NO TRANSLATION.
    virtual void DoneCurrent();
    /// \ru Поменять задний и передний буферы указанной поверхности. \en NO TRANSLATION.
    virtual void SwapBuffers();
    /// \ru Поменять задний и передний буферы указанной поверхности. \en NO TRANSLATION.
    virtual void SwapBuffers(RenderingArea* pFrame);

    /// \ru Возвращает формат контекста, если кортекст был создан с помощью функции Create(). \en NO TRANSLATION.
    virtual RenderingAreaFormat GetFormat() const;
    /// \ru Установить формат контекста, если кортекст был создан с помощью функции Create(). \en NO TRANSLATION.
    virtual void SetFormat(const RenderingAreaFormat& format);

    /// \ru Это перегруженная функция. Возвращает указатель на указанную функцию. \en NO TRANSLATION.
    virtual OpenGLFunctionListInterface* GetFunctionList() const = 0;
    /// \ru Получить дополнительный экземпляр OpenGLExFunctionList для этого контекста. \en NO TRANSLATION.
    virtual OpenGLExtraFunctionListInterface* GetExtraFunctionList() const = 0;
    /// \ru Это перегруженная функция. Возвращает указатель на указанную функцию. \en NO TRANSLATION.
    virtual FunctionPtr GetProcAddress(const std::string& procName) const = 0;
    /// \ru Возвращает true, если этот контекст является действительным, т.е. был успешно создан. \en NO TRANSLATION.
    virtual bool IsValid() const = 0;
    /// \ru Возвращает объект фреймбуфера по умолчанию для текущей поверхности. \en NO TRANSLATION.
    virtual uint GetDefaultFrameBufferObject() const = 0;
    /// \ru Возвращает указатель на экран для отрисовки OpenGL. \en NO TRANSLATION.
    virtual RenderingArea* GetSurface() const;
    /// \ru Возвращает список расшаренных контекстов этого контекста. \en NO TRANSLATION.
    virtual OpenGLContextContainer* GetContextContainer() const = 0;
    /// \ru Возвращает true, если контекст является контекстом OpenGL ES. \en NO TRANSLATION.
    virtual bool IsOpenGLES() const = 0;
    /// \ru Вернуть true, если контекст разделяется с другими GL контекстами. \en NO TRANSLATION.
    virtual bool IsSharing() const;
    /// \ru Проверить потдерживает ли устройство конкретное расширение. \en NO TRANSLATION.
           bool IsSupportExtansion(const std::string & name) const;
    /// \ru Являются ли одинаковыми. \en NO TRANSLATION.
    static bool IsSame(const OpenGLContextInterface* pFirst, const OpenGLContextInterface* pSecond);
public:
    /// \ru Разрешено ли освещение. \en NO TRANSLATION.
    bool IsAllowLighting() const;

    MbMatrix3D GetModelViewMatrix() const;
    MbMatrix3D GetProjectionMatrix() const;

    virtual void oglMatrixMode(uint mode);
    virtual void oglLoadIdentity();
    virtual void oglPushMatrix();
    virtual void oglPopMatrix();
    virtual void oglLoadMatrix(const MbMatrix3D& matrix);
    virtual void oglMultMatrix(const MbMatrix3D& matrix);
    virtual void oglOrtho(double left, double right, double bottom, double top, double nearVal, double farVal);
    virtual void oglFrustum(double left, double right, double bottom, double top, double nearVal, double farVal);
    virtual bool oglAllowLighting(bool bAllow);
    virtual void UpdateVariables();
public:
    /// \ru Возвращает последний контекст, который был установлен при вызове MakeCurrent или 0, если контекст не установлен.  \en NO TRANSLATION.
    static OpenGLContextInterface* GetCurrentContext();
public:
    static OpenGLContextInterface* m_pContextInterface;
protected:
    VSN_DECLARE_PRIVATE(OpenGLContextInterface)
};

VSN_END_NAMESPACE

#endif /* __VSN_OPENGLCONTEXTINTERFACE_H */
