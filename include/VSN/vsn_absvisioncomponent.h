﻿////////////////////////////////////////////////////////////////////////////////
/**
\file
    \brief \ru NO TRANSLATION.
           \en NO TRANSLATION. \~
*/
////////////////////////////////////////////////////////////////////////////////
#ifndef __VSN_ABSVISIONCOMPONENT_H
#define __VSN_ABSVISIONCOMPONENT_H

#include "vsn_object.h"
#include "vsn_visionwork.h"
#include "vsn_nodekey.h"
#include "vsn_global.h"

VSN_BEGIN_NAMESPACE

class InternalNodeMaps;
typedef std::shared_ptr<InternalNodeMaps> InternalNodeMapsPtr;

class AbsVisionComponentPrivate;
//------------------------------------------------------------------------------
/** \brief   \ru NO TRANSLATION.
             \en NO TRANSLATION. \~
    \details \ru NO TRANSLATION. \n
             \en NO TRANSLATION. \n \~
    \ingroup NO GROUP
*/
class VSN_CLASS AbsVisionComponent : public Object
{
    VSN_OBJECT(AbsVisionComponent);
public:
    /// \ru Конструктор по умолчанию. \en NO TRANSLATION.
    explicit AbsVisionComponent(Object* pParent = nullptr);
    /// \ru Деструктор. \en NO TRANSLATION.
    virtual ~AbsVisionComponent();
protected:
    /// \ru Вернуть уникальный ключь Essence. \en NO TRANSLATION.
    NodeKey GetTopEssenceKey() const;
    virtual std::vector<VisionWorkPtr> WorksToExecute(int64 time);

    virtual void OnInstalled();
    virtual void OnUninstalled();

    /// \ru Вызывается в контексте Engine. \en NO TRANSLATION.
    virtual void OnEngineStarting() {}
    /// \ru Вызывается в контексте Engine. \en NO TRANSLATION.
    virtual void OnEngineClosing() {}
protected:
    template<class Frontend>
    void InstallInternalKind(const InternalNodeMapsPtr& functor);
    void InstallInternalKind(const MetaObject&, const InternalNodeMapsPtr& functor);
protected:
    AbsVisionComponent(AbsVisionComponentPrivate& d, Object* parent);
private:
    friend class VisionEngine;
	friend class VisionManager;
    friend class GraphicsSceneEngine;
    VSN_DECLARE_EX_PRIVATE(AbsVisionComponent);
};

template<class Frontend>
void AbsVisionComponent::InstallInternalKind(const InternalNodeMapsPtr& functor)
{
    InstallInternalKind(Frontend::GetStaticMetaObject(), functor);
}

VSN_END_NAMESPACE

#endif /* __VSN_ABSVISIONCOMPONENT_H */
