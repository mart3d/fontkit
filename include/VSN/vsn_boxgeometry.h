﻿#ifndef __VSN_BOXGEOMETRY_H
#define __VSN_BOXGEOMETRY_H

#include "vsn_meshgeometry.h"

VSN_BEGIN_NAMESPACE

class BoxGeometryPrivate;
//------------------------------------------------------------------------------
/** \brief \ru Кубик.
           \en . \~
    \ingroup Vision_Geometry
*/
// ---
class VSN_CLASS BoxGeometry : public MeshGeometry
{
    VSN_OBJECT(BoxGeometry);
    VSN_PROP_READ_WRITE_NOTIFY(width,  GetWidth,  SetWidth,  WidthModified);
    VSN_PROP_READ_WRITE_NOTIFY(length, GetLength, SetLength, LengthModified);
    VSN_PROP_READ_WRITE_NOTIFY(height, GetHeight, SetHeight, HeightModified);
public:
    /// \ru Конструктор по умолчанию. \en Default constructor.
    BoxGeometry(Node* pParent = nullptr);
    /// \ru Конструктор по ширине, длине и высоте. \en Сonstructor by the width, the length and height.
    BoxGeometry(double width, double length, double height, Node* pParent = nullptr);
    /// \ru Деструктор. \en Destructor. 
    virtual ~BoxGeometry();
public:
    /// \ru Выдать ширину параллелепипеда. \en Get width.
    double GetWidth() const;
    /// \ru Выдать длину параллелепипеда. \en Get length.
    double GetLength() const;
    /// \ru Выдать высоту параллелепипеда. \en Get height.
    double GetHeight() const;
public:
    /// \ru Задать ширину параллелепипеда. \en Set width.
    VSN_SLOT(Public, SetWidth, void SetWidth(double width))
    /// \ru Задать длину параллелепипеда. \en Set width.
    VSN_SLOT(Public, SetLength, void SetLength(double length))
    /// \ru Задать высоту параллелепипеда. \en Set height.
    VSN_SLOT(Public, SetHeight, void SetHeight(double height))
public:
    /// \ru Модификация ширины параллелепипеда. \en NO TRANSLATION.
    VSN_SIGNAL(Public, WidthModified, void WidthModified(double width), width)
    /// \ru Модификация длины параллелепипеда. \en NO TRANSLATION.
    VSN_SIGNAL(Public, LengthModified, void LengthModified(double length), length)
    /// \ru Модификация высоты параллелепипеда. \en NO TRANSLATION.
    VSN_SIGNAL(Public, HeightModified, void HeightModified(double height), height)
public:
    /// \ru Выдать габаритный куб. \en Get a bounding box.
    virtual const MbCube& GetBoundingBox() override;
    /// \ru Обновить геометрию по новым данным. \en NO TRANSLATION.
    virtual void UpdateGeometry() override;
private:
    /// \ru Отрисовать сферу с помощью OpenGL. \en Draw a sphere with OpenGL.
    virtual void OpenGLDraw(const RenderState& state) override;
private:
    VSN_DISABLE_COPY(BoxGeometry);
    VSN_DECLARE_EX_PRIVATE(BoxGeometry);
};

VSN_END_NAMESPACE

#endif /* __VSN_BOXGEOMETRY_H */
