﻿////////////////////////////////////////////////////////////////////////////////
/** 
  \file
  \brief \ru Класс MathGeometry представляет геометрию для последующей отрисовки, 
             которая была сгенерирована с помощью математического представления.
         \en NO TRANSLATION. \~

*/
////////////////////////////////////////////////////////////////////////////////

#ifndef __VSN_MATHGEOMETRY_H
#define __VSN_MATHGEOMETRY_H

#include <model_item.h>
#include "vsn_meshgeometry.h"
#include "vsn_global.h"

VSN_BEGIN_NAMESPACE
class MathGeometryPrivate;
//------------------------------------------------------------------------------
/** \brief \ru Класс MathGeometry представляет геометрию для последующей отрисовки, которая была сгенерирована с помощью математического представления.
           \en NO TRANSLATION. \~
    \details \ru Класс MathGeometry принимает указатель на математическое представление MbItem для последующей генерации визуального представления.
                 MbItem может представлять собой такие объекты, как MbSolid, MbMesh, MbСontour, ... которые способны генерировать самостоятельно
                 полигональную модель. Класс MathGeometry представляет ряд полезных функций. Для вычисления шага триангуляции поверхностей и граней,
                 вы можете воспользоваться функцией SetVisualSag и вызвать перегенерацию триангуляции в реальном времени, что влияет на качество 
                 отображения геометрии. С помощью функций QueryMathByGeometry и  QueryGeomByMath вы можете получить идентификацию примитивов из 
                 визуального представления в математическое и наоборот. Необходимо знать, что идентификаторы математического представления являются
                 хеш'ом реальных идентификаторов и пути к этим примитивам. \n.
             \en NO TRANSLATION. \n \~
    \ingroup Vision_Geometry
*/
// ---
class VSN_CLASS MathGeometry : public MeshGeometry
{
    VSN_OBJECT(MathGeometry);
    VSN_PROP_READ_WRITE_NOTIFY(visualSag, GetVisualSag, SetVisualSag, VisualSagModified);
public:
    /// \ru Структура данных при запросе индентификаторов соответствия. \en Default constructor. \~
    struct InfoPrimitive
    {
        enum TypeGeometry { None, Vertex, Edge, Face };
        InfoPrimitive() : m_type(None) , m_id(-1) {}
        InfoPrimitive(TypeGeometry type, uint id) : m_type(type) , m_id(id) {}
        TypeGeometry m_type; ///< \ru Тип примитива. \en NO TRANSLATION. \~
        uint32       m_id;   ///< \ru Идентификатор примитива. \en NO TRANSLATION. \~
    };
    /// \ru Конструктор по умолчанию. \en Default constructor. \~
    MathGeometry(Node* pParent = nullptr);
    /// \ru Деструктор. \en Destructor. \~
    virtual ~MathGeometry();
public: 
    /// \ru Вернуть mаксимально допустимый прогиб кривой или поверхности в соседних точках на расстоянии шага. \en The maximum permissible sag of the curve or surface at adjacent points away step. \~
    double GetVisualSag() const;

    /// \ru Вернуть  указатель на математическое точное представление MbItem. \en NO TRANSLATION. \~
    const MbItem* GetMathItem() const;
    /// \ru Установить указатель на математическое точное представление MbItem. \en NO TRANSLATION. \~
    void SetMathItem(const MbItem* pItem, double sag = Math::visualSag);
    /// \ru Очистить в этой геометрии все примитивы. \en NO TRANSLATION. \~
    void Clear() override;

    /// \ru Вернуть математический идентификатор примитива по уникальному идентификатору этой геометрии. \en NO TRANSLATION. \~
    InfoPrimitive QueryMathByGeometry(uint geomId) const;
    /// \ru Вернуть геометрический идентификатор примитива по уникальному идентификатору математического примитива. \en NO TRANSLATION. \~
    InfoPrimitive QueryGeomByMath(uint mathId) const;
public:
    /// \ru Получить габаритный куб объекта. \en Returns object bounding box. \~
    virtual const MbCube & GetBoundingBox() override;
    /// \ru Обновить геометрию по новым данным для внутреннего использования \en NO TRANSLATION. \~
    virtual void UpdateGeometry() override;
    /// \ru True, если буфер вершин используется. \en True if vertex buffer is used. \~
    virtual void SetUseVertexBufferObjects(bool usage);
public:
    /// \ru Установить максимально допустимый прогиб кривой или поверхности в соседних точках на расстоянии шага. \en The maximum permissible sag of the curve or surface at adjacent points away step. \~
    VSN_SLOT(Public, SetVisualSag, void SetVisualSag(double sag))
public:
    /// \ru Сигнал модификации шага расчета триангуляции. \en NO TRANSLATION. \~
    VSN_SIGNAL(Public, VisualSagModified, void VisualSagModified(double sag), sag)
    /// \ru Сигнал об окончании перестроения всей геометрии. \en NO TRANSLATION. \~
    VSN_SIGNAL(Public, BuildCompleted, void BuildCompleted())
private:
    /// \ru Перестроить тело. \en NO TRANSLATION. \~
    void slotDataModified(const std::vector<float>& bytes);
    /// \ru Отрисовка тела. \en Renders solid body. \~
    virtual void OpenGLDraw(const RenderState& state) override;
private:
    VSN_DISABLE_COPY(MathGeometry);
    VSN_DECLARE_EX_PRIVATE(MathGeometry);
    friend class MathGeometryBuilder;
};
VSN_END_NAMESPACE

#endif /* __VSN_MATHGEOMETRY_H */
