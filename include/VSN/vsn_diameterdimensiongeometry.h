﻿////////////////////////////////////////////////////////////////////////////////
/** 
  \file
  \brief \ru NO TRANSLATION.
         \en NO TRANSLATION. \~

*/
////////////////////////////////////////////////////////////////////////////////

#ifndef __VSN_DIAMETERDIMENSIONGEOMETRY_H
#define __VSN_DIAMETERDIMENSIONGEOMETRY_H

#include "vsn_dimensiongeometry.h"
#include "vsn_global.h"

class MbArc3D;
VSN_BEGIN_NAMESPACE
class DiameterDimensionGeometryPrivate;
//------------------------------------------------------------------------------
/** \brief \ru NO TRANSLATION.
           \en NO TRANSLATION. \~
  \details \ru NO TRANSLATION. \n.
           \en NO TRANSLATION. \n \~
  \ingroup NO GROUP
*/
// ---
class VSN_CLASS DiameterDimensionGeometry : public DimensionGeometry
{
    VSN_OBJECT(DiameterDimensionGeometry)
public:
    /// \ru Конструктор по умолчанию. \en Default constructor.
    DiameterDimensionGeometry(Node* pParent = nullptr);
    /// \ru Деструктор. \en Destructor. 
    virtual ~DiameterDimensionGeometry();
public:
    /// \ru NO TRANSLATION. \en NO TRANSLATION.
    void SetMeasuredGeometry(const MbArc3D& circle);
public:
    /// \ru Получить точку фиксации на круге для размера диаметра. \en NO TRANSLATION.
    MbCartPoint3D GetFixPoint() const;
public:
    /// \ru Вернуть единицы измерения по умолчанию.(единицы являются модельные для правильной конвертации) \en NO TRANSLATION. \~
    virtual String GetDefaultUnits() const override;
    /// \ru Установить единицы измерения по умолчанию. (единицы являются модельные для правильной конвертации) \en NO TRANSLATION. \~
    virtual void SetDefaultUnits(const String& units) override;

    /// \ru Вернуть единицы измерения для вывода. \en NO TRANSLATION. \~
    virtual String GetOutputUnits() const override;
    /// \ru Установить единицы измерения для вывода. \en NO TRANSLATION. \~
    virtual void SetOutputUnits(const String& units) override;

    /// \ru Получить позицию текста 2D - размера. \en NO TRANSLATION.
    virtual const MbCartPoint3D GetTextPosition() const override;
    /// \ru Установить позицию текста 2D - размера. \en NO TRANSLATION.
    virtual void SetTextPosition(const MbCartPoint3D& textPos) override;
public:
    /// \ru Выдать габаритный куб. \en Get a bounding box.
    virtual const MbCube& GetBoundingBox() override;
protected:
    /// \ru Вернуть вычисленное значение измерения. \en NO TRANSLATION.
    double CalculateValue() const override;
protected:
    VSN_DECLARE_EX_PRIVATE(DiameterDimensionGeometry)
};

VSN_END_NAMESPACE

#endif /* __VSN_DIAMETERDIMENSIONGEOMETRY_H */
