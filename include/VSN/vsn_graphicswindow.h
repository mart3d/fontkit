﻿////////////////////////////////////////////////////////////////////////////////
/**
\file
    \brief \ru Класс Window представляет собой реализацию окна для отрисовки OpenGl'ем.
           \en Class Window is a implementation of the window rendering using OpenGL. \~
*/
////////////////////////////////////////////////////////////////////////////////
#ifndef __VSN_GRAPHICSWINDOW_H
#define __VSN_GRAPHICSWINDOW_H

#include "vsn_openglwindow.h"
#include "vsn_processmanager.h"
#include "vsn_global.h"

VSN_BEGIN_NAMESPACE
class Process;
class Viewport;
class SceneContent;
class GraphicsScene;
class GraphicsWindowPrivate;
//------------------------------------------------------------------------------
/** \brief \ru Класс Window представляет собой реализацию окна для отрисовки OpenGl'ем.
           \en Class Window is a implementation of the window rendering using OpenGL. \~
    \ingroup Vision_OpenGL
*/
// ---
class VSN_CLASS GraphicsWindow : public OpenGLWindow
{
    VSN_OBJECT(GraphicsWindow)
public:
    explicit GraphicsWindow(Window* pParent = V_NULL);
    virtual ~GraphicsWindow();
public:
    /// \ru Вернуть указатель графическую сцену. \en NO TRANSLATION.
    GraphicsScene* GetGraphicsScene() const;
    /// \ru Вернуть указатель на сцену. \en NO TRANSLATION.
    SceneContent* GetSceneContent();
    /// \ru Вернуть указатель на Viewport. \en NO TRANSLATION.
    Viewport* GetViewport() const;
    // Вернуть и обновление глобальный ограничивающий куб
    MbCube GetGlobalBoundingBox();
public:
    /** \brief \ru Добавить интерактивный процесс.
               \en Adds interactive process. \~
        \param[in] scene - \ru Идентификатор интерактивного процесса.
                           \en Interactive process identifier. \~
        \param[in] id - \ru Идентификатор интерактивного процесса.
                        \en Interactive process identifier. \~
    */
    void AddProcess(ProcessManager::IdProcess id);

    /** \brief \ru Добавить интерактивный процесс.
               \en Adds interactive process. \~
        \param[in] scene - \ru Идентификатор интерактивного процесса.
                           \en Interactive process identifier. \~
        \param[in] id - \ru Идентификатор интерактивного процесса.
                        \en Interactive process identifier. \~
    */
    void AddProcess(Process* pProcess, ProcessManager::IdProcess id);

    /** \brief \ru Удалить интерактивный процесс.
               \en Removes interactive process. \~
        \param[in] id - \ru Идентификатор интерактивного процесса.
                        \en Interactive process identifier. \~
    */
    void RemoveProcess(ProcessManager::IdProcess id);
protected:
    virtual void DrawOpenGL();
protected:
    virtual void ResizeOpenGL(const Size& sz);
protected:
    virtual void OnMouseLButtonDownEvent(MouseEvent* event) override;
    virtual void OnMouseLButtonUpEvent(MouseEvent* event) override;
    virtual void OnMouseMButtonDownEvent(MouseEvent* event) override;
    virtual void OnMouseMButtonUpEvent(MouseEvent* event) override;
    virtual void OnMouseMoveEvent(MouseEvent* event) override;
private:
    OBVIOUS_PRIVATE_COPY(GraphicsWindow)
    VSN_DECLARE_EX_PRIVATE(GraphicsWindow)
};

VSN_END_NAMESPACE

#endif // __VSN_GRAPHICSWINDOW_H
