﻿#ifndef __VSN_ANGLEDIMENSIONREP_H
#define __VSN_ANGLEDIMENSIONREP_H

#include "vsn_dimensionrep.h"

VSN_BEGIN_NAMESPACE
//------------------------------------------------------------------------------
/** \brief \ru NO TRANSLATION.
           \en NO TRANSLATION. \~
    \ingroup Vision_Geometry
*/
// ---
class VSN_CLASS AngleDimensionRep : public DimensionRep
{
    VSN_OBJECT(AngleDimensionRep)
public:
    /// \ru Конструктор по умолчанию. \en Default constructor.
    explicit AngleDimensionRep(Node* pParent = nullptr);
    /// \ru Деструктор. \en Destructor. 
    virtual ~AngleDimensionRep();
public:
    /// \ru Вернуть единицы измерения по умолчанию.(единицы являются модельные для правильной конвертации) \en NO TRANSLATION. \~
    String GetDefaultUnits() const;
    /// \ru Установить единицы измерения по умолчанию. (единицы являются модельные для правильной конвертации) \en NO TRANSLATION. \~
    void SetDefaultUnits(const String& units);

    /// \ru Вернуть единицы измерения для вывода. \en NO TRANSLATION. \~
    String GetOutputUnits() const;
    /// \ru Установить единицы измерения для вывода. \en NO TRANSLATION. \~
    void SetOutputUnits(const String& units);
public:
    /// \ru Измерить угол по трем точкам. \en NO TRANSLATION.
    void SetMeasuredGeometry(const MbCartPoint3D& center, const MbCartPoint3D& pnt1, const MbCartPoint3D& pnt2);
private:
    VSN_DISABLE_COPY(AngleDimensionRep)
};

VSN_END_NAMESPACE

#endif /* __VSN_ANGLEDIMENSIONREP_H */
