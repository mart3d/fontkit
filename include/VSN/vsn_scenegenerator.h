﻿////////////////////////////////////////////////////////////////////////////////
/** 
  \file
  \brief \ru Класс SceneGenerator представляет фабрику представлений геометрических 
             объектов, а также генерацию готовой сцены для последующей отрисовки.
         \en NO TRANSLATION. \~

*/
////////////////////////////////////////////////////////////////////////////////

#ifndef __VSN_SCENEGENERATOR_H
#define __VSN_SCENEGENERATOR_H

#include "model.h"
#include "model_item.h"
#include "vsn_object.h"
#include "vsn_sphererep.h"
#include "vsn_conerep.h"
#include "vsn_boxrep.h"
#include "vsn_cylinderrep.h"
#include "vsn_rectanglerep.h"
#include "vsn_lineardimensionrep.h"
#include "vsn_angledimensionrep.h"
#include "vsn_commentrep.h"
#include "vsn_diameterdimensionrep.h"

#include "vsn_global.h"

VSN_BEGIN_NAMESPACE
class ProgressBuildPrivate;
//------------------------------------------------------------------------------
/** \brief \ru Класс ProgressBuild служит для определения конца и шага генерации полигональной модели.
           \en NO TRANSLATION. \~
    \details \ru Класс ProgressBuild имеет ряд функций, а также сигналов, с помощью которых пользователь 
                 может узнать состояние генерации тел и примитивов, а также ее окончание. Важно знать, что
                 ProgressBuild применяется только при генерации математических обектов. \n.
             \en NO TRANSLATION. \n \~
    \ingroup Vision_Node
*/
// ---
class VSN_CLASS ProgressBuild : public Object
{
    VSN_OBJECT(ProgressBuild);
    VSN_PROP_READ(minimum, GetMinimum);
    VSN_PROP_READ(maximum, GetMaximum);
    VSN_PROP_READ_NOTIFY(value, GetValue, ValueModified);
protected:
    /// \ru Конструктор по умолчанию. \en Default constructor. \~
    explicit ProgressBuild();
    /// \ru Деструктор. \en Destructor. \~ 
    virtual ~ProgressBuild();
public:
    /// \ru Вернуть текущее значение перестраиваемой геометрии. \en NO TRANSLATION. \~
    int GetValue() const;
    /// \ru Вернуть минимальное значение перестраиваемой геометрии. \en NO TRANSLATION. \~
    int GetMinimum() const;
    /// \ru Вернуть максимальное значение количества перестраиваемой геометрии. \en NO TRANSLATION. \~
    int GetMaximum() const;
public:
    /// \ru Сигнал об окончании перестроения конкретной геометрии. \en NO TRANSLATION. \~
    VSN_SIGNAL(Public, ValueModified, void ValueModified(int val), val)
    /// \ru Сигнал об окончании перестроения всей геометрии. \en NO TRANSLATION. \~
    VSN_SIGNAL(Public, BuildAllCompleted, void BuildAllCompleted())
private:
    VSN_DISABLE_COPY(ProgressBuild)
    VSN_DECLARE_EX_PRIVATE(ProgressBuild)
    friend class SceneGenerator;
    friend class SceneGeneratorPrivate;
};

class SceneSegment;
class SceneGeneratorPrivate;
//------------------------------------------------------------------------------
/** \brief \ru Класс SceneGenerator представляет фабрику представлений геометрических
               объектов, а также генерацию готовой сцены для последующей отрисовки.
           \en NO TRANSLATION. \~
  \details \ru SceneGenerator имеет две группы функций. Первая группа работает непосредственно с математическим 
               ядром и тем самым обеспечивает глубокую интеграцию с математическими объектами. Вторая группа 
               генерирует представления простых объектов, таких как куб, цилиндр, сфера и др., т.е. где 
               полигональная модель рассчитывается не с помощью математического ядра, а вручную. \n.
           \en NO TRANSLATION. \n \~
    \ingroup Vision_Node
*/
// ---
class VSN_CLASS SceneGenerator : public Object
{
public:
    /// \ru Получить экземпляр фабрики. \en NO TRANSLATION. \~
    static SceneGenerator* Instance();
    /// \ru Деструктор. \en Destructor. \~ 
    virtual ~SceneGenerator();

protected:
    /// \ru Конструктор по умолчанию. \en Default constructor. \~
    explicit SceneGenerator();

public: /// \ru Группа функций, работающая с математическим ядром. \en NO TRANSLATION. \~
    /// \ru Создать прогресс построения геометрии. \en NO TRANSLATION. \~
    ProgressBuild* CreateProgressBuild();
    /// \ru Прочитать модель из памяти. \en NO TRANSLATION. \~
    MbModel* LoadModel(const char* pMemory);
    /// \ru Создать граф сцены из MbModel. \en NO TRANSLATION. \~
    SceneSegment* CreateSceneSegment(const MbModel* pModel, SceneSegment* pParent = nullptr);
    /// \ru Получить графическое представление с MbItem. \en NO TRANSLATION. \~
    GeometryRep* CreateMathRep(const MbItem* pItem, SceneSegment* pParent = nullptr, double sag = Math::visualSag);

public: /// \ru Группа функций, генерирущая простые представления вручную. \en NO TRANSLATION. \~
    /// \ru Создать представление сферы. \en NO TRANSLATION. \~
    SphereRep* CreateSphere(double radius, Node* pParent = nullptr) const;
    /// \ru Создать представление конуса. \en NO TRANSLATION. \~
    ConeRep* CreateCone(double radius, double height,  Node* pParent = nullptr) const;
    /// \ru Создать представление цилиндра. \en NO TRANSLATION. \~
    CylinderRep* CreateCylinder(double radius, double height, Node* pParent = nullptr) const;
    /// \ru Создать представление параллелепипеда. \en NO TRANSLATION. \~
    BoxRep* CreateBox(double width, double length, double height, Node* pParent = nullptr) const;
    /// \ru Создать представление прямоугольника. \en NO TRANSLATION. \~
    RectangleRep* CreateRectangle(double width, double height, Node* pParent = nullptr) const;
    /// \ru Создать линейный размер. \en NO TRANSLATION. \~
    LinearDimensionRep* CreateLinearDimension(Node* pParent = nullptr) const;
    /// \ru Создать угловой размер. \en NO TRANSLATION. \~
    AngleDimensionRep* CreateAngleDimension(Node* pParent = nullptr) const;
    /// \ru Создать угловой размер. \en NO TRANSLATION. \~
    CommentRep * CreateComment( const std::wstring & text, Node* pParent = nullptr ) const;
    /// \ru Создать диаметральный размер. \en NO TRANSLATION. \~
    DiameterDimensionRep* CreateDiameterDimension(Node* pParent = nullptr) const;
private:
    VSN_DISABLE_COPY(SceneGenerator)
    VSN_DECLARE_PRIVATE(SceneGenerator)
    friend class MathRepresentation;
};
VSN_END_NAMESPACE

#endif /* __VSN_SCENEGENERATOR_H */
