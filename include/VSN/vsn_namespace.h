﻿////////////////////////////////////////////////////////////////////////////////
/** 
    \file
    \brief \ru
           \en
*/
////////////////////////////////////////////////////////////////////////////////
#ifndef __VSN_NAMESPACE_H
#define __VSN_NAMESPACE_H

#include <mb_vector3d.h>
#include <system_types.h>

#include "vsn_flags.h"
#include "vsn_mathdefs.h"
#include "vsn_global.h"


VSN_BEGIN_NAMESPACE

// настройки осей по умолчанию
// вектор X
const MbVector3D X_AXIS(1.0, 0.0, 0.0);
// вектор Y
const MbVector3D Y_AXIS(0.0, 1.0, 0.0);
// вектор Z
const MbVector3D Z_AXIS(0.0, 0.0, 1.0);

// буфер смещения используется для вершинного буфера
#define BUFFER_OFFSET(i) ((char*)nullptr + (i))

namespace Public { typedef void* HANDLE; };

enum ConnectionType 
{
	AutoConnection,
	DirectConnection,
	QueuedConnection,
	BlockingQueuedConnection,
	UniqueConnection = 0x80
};

// ---
enum WindowType
{
    wt_Control  = 0x00000000,
    wt_Window   = 0x00000001,
    wt_Dialog   = 0x00000002 | wt_Window,
    wt_Modal    = 0x00000004 | wt_Window,
    wt_Popup    = 0x00000008 | wt_Window,
    wt_Tool     = wt_Popup | wt_Dialog,
    wt_ToolTip  = wt_Popup | wt_Modal,
    wt_SplashScreen  = wt_ToolTip | wt_Dialog,
    wt_Desktop       = 0x00000010 | wt_Window,
    wt_ForeignWindow = 0x00000020 | wt_Window,
    wt_Mask          = 0x000000ff,
    /// \ru Флаги, которые можно использовать для настройки внешнего вида окон верхнего уровня. \en NO TRANSLATION.
    // Hint
    wh_MSFixedSizeDialogHint = 0x00000100,
    wh_MSOwnDC               = 0x00000200,
    wh_FramelessHint         = 0x00000800,
    wh_TitleHint             = 0x00001000,
    wh_SystemMenuHint        = 0x00002000,
    wh_MinButtonHint         = 0x00004000,
    wh_MaxButtonHint         = 0x00008000,
    wh_MinMaxButtonsHint     = wh_MinButtonHint | wh_MaxButtonHint,
    wh_ContextHelpButtonHint = 0x00010000,
    wh_ShadeButtonHint       = 0x00020000,
    wh_StaysOnTopHint        = 0x00040000,
    wh_TransparentForInput   = 0x00080000,

    wh_CustomizeHint         = 0x02000000,
    wh_StaysOnBottomHint     = 0x04000000,

    wh_CloseButtonHint       = 0x08000000,
    wh_FullscreenButtonHint  = 0x80000000,
};
VSN_DECLARE_FLAGS(WindowFlags, WindowType)

enum WindowState 
{
    ws_NoState    = 0x00000000,
    ws_Minimized  = 0x00000001,
    ws_Maximized  = 0x00000002,
    ws_FullScreen = 0x00000004,
    ws_Active     = 0x00000008
};
VSN_DECLARE_FLAGS(WindowStates, WindowState)

enum AppAttribute
{
	aa_appDesktopOpenGL     = 0,
	aa_appOpenGLES          = 1,
	aa_appSoftwareOpenGL    = 2,
    aa_RegisteredWindows    = 3,
};

enum TimerType 
{
    tt_PreciseTimer,
    tt_CoarseTimer,
    tt_VeryCoarseTimer
};

enum FocusReason
{
    MouseFocusReason,
    TabFocusReason,
    BacktabFocusReason,
    ActiveWindowFocusReason,
    PopupFocusReason,
    ShortcutFocusReason,
    MenuBarFocusReason,
    OtherFocusReason,
    NoFocusReason
};

enum EventPriority 
{
    HighEventPriority = 1,
    NormalEventPriority = 0,
    LowEventPriority = -1
};

enum ClippingPlaneId
{
    LeftPlane   = 0, // левая плоскость отсечения
    RightPlane  = 1, // правая плоскость отсечения
    TopPlane    = 2, // верхняя плоскость отсечения
    BottomPlane = 3, // нижняя плоскость отсечения
    NearPlane   = 4, // ближайшая плоскость отсечения
    FarPlane    = 5  // дальняя плоскость отсечения
};

enum FrustumClassification
{
    InFrustum = 0,
    IntersectFrustum = 1,
    OutFrustum  = 2
};

// стандартные типы ориентаций отображения
enum Orientation
{
    Front,    // Спереди  - Фронтальная плоскость
    Rear,     // Сзади
    Up,       // Сверху   - Горизонтальная плоскость
    Down,     // Снизу
    Left,     // Слева    - Профильная плоскость
    Right,    // Справа
    IsoXYZ,   // Изометрия XYZ
};

enum AxisOrientation
{
    AxisOrientationNone = 0,
    AxisOrientationX    = 1,
    AxisOrientationY    = 2,
    AxisOrientationZ    = 4
};

// состояние визуализации геометрии
enum RenderingState
{
    NormalRenderState = 0,
    SpecialMaterialState,
    SpecialTransparencyState,
    SpecialMaterialTransparencyState,
    SpecialMaterialPrimitiveState,
    PrimitiveSelectedState,
    BodySelectionState,
    PrimitiveSelectionState
};

// флаги отображения геометрии
enum RenderMode
{
    rm_Shaded,
    rm_ShadedWithEdges,
    rm_Wireframe,
    rm_HiddenLinesRemoved,
    rm_Transparent,
    rm_OutlineSilhouette
};

// применяемый тип Vertex Buffer Object
enum VertexBufferObjectType
{
    buf_Vertex = 10,
    buf_Color,
    buf_Index,
    buf_Normal,
    buf_Texel,
};

enum TypeTriangulation
{
    Triangles,
    TrianglesStrip,
    TrianglesFan,
};

// тип представления
enum TypeRep
{
    Geometry_VBO = 1
};


VSN_END_NAMESPACE

#endif /* __VSN_NAMESPACE_H */


