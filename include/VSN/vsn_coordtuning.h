﻿////////////////////////////////////////////////////////////////////////////////
/**
  \file
    \brief \ru CoordTuning этот класс хранит настройки для указанного спска кооринат.
           \en NO TRANSLATION. \~
*/
////////////////////////////////////////////////////////////////////////////////
#ifndef __VSN_COORDTUNING_H
#define __VSN_COORDTUNING_H

#include "vsn_node.h"

VSN_BEGIN_NAMESPACE

class CoordTuningPrivate;
/* CoordTuning */
//------------------------------------------------------------------------------
/** \brief   \ru CoordTuning этот класс хранит настройки для указанного спска кооринат.
             \en NO TRANSLATION. \~
    \details \ru CoordTuning cохраняет веичену мертвой зоны связанную с этой осью и определяет включено ли сглаживание. \n
             \en NO TRANSLATION. \n \~
    \ingroup Vision_Interaction
*/
// ---
class CoordTuning : public Node
{
	VSN_OBJECT(CoordTuning)
    VSN_PROP_READ_WRITE_NOTIFY(deadband, GetDeadband, SetDeadband, DeadbandModified)
    VSN_PROP_READ_WRITE_NOTIFY(coords, GetCoords, SetCoords, CoordsModified)
    VSN_PROP_READ_WRITE_NOTIFY(smooth, IsSmoothEnabled, SetSmoothEnabled, SmoothModified)
public:
    /// \ru Конструктор по умолчанию. \en Default constructor. \~
    explicit CoordTuning(Node* pParent = nullptr);
    /// \ru Деструктор. \en Destructor. \~
    ~CoordTuning();
public:
    /// \ru Вернуть радиус мертвой зоны. \en NO TRANSLATION. \~
    float GetDeadband() const;
    /// \ru Вернуть текущий список внутренних координат к которым применяются эти настройки. \en NO TRANSLATION. \~
    std::vector<int> GetCoords() const;
    /// \ru Вернуть true, если если сглаживание включено. \en NO TRANSLATION. \~
    bool IsSmoothEnabled() const;
public:
    /// \ru Установить радиус мертвой зоны. \en NO TRANSLATION. \~
    VSN_SLOT(Public, SetDeadband, void SetDeadband(float deadband))
    /// \ru Установить текущие координаты. \en NO TRANSLATION. \~
    VSN_SLOT(Public, SetCoords, void SetCoords(const std::vector<int>& coords))
    /// \ru Установить текущее состояние сглаживания координат. \en NO TRANSLATION. \~
    VSN_SLOT(Public, SetSmoothEnabled, void SetSmoothEnabled(bool enabled))
public:
    VSN_SIGNAL(Public, DeadbandModified, void DeadbandModified(float deadband), deadband)
    VSN_SIGNAL(Public, CoordsModified, void CoordsModified(const std::vector<int>& coords), coords)
    VSN_SIGNAL(Public, SmoothModified, void SmoothModified(bool smooth), smooth)
private:
	NCreatedModificationBasicPtr  CreateNodeModification() const override;
private:
	VSN_DECLARE_EX_PRIVATE(CoordTuning);
};


VSN_END_NAMESPACE

#endif // __VSN_COORDTUNING_H
