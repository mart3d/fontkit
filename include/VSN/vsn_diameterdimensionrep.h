﻿#ifndef __VSN_DIAMETERDIMENSIONREP_H
#define __VSN_DIAMETERDIMENSIONREP_H

#include <cur_arc3d.h>
#include "vsn_dimensionrep.h"

VSN_BEGIN_NAMESPACE
//------------------------------------------------------------------------------
/** \brief \ru NO TRANSLATION.
           \en NO TRANSLATION. \~
    \ingroup Vision_Geometry
*/
// ---
class VSN_CLASS DiameterDimensionRep : public DimensionRep
{
    VSN_OBJECT(DiameterDimensionRep)
public:
    /// \ru Конструктор по умолчанию. \en Default constructor.
    explicit DiameterDimensionRep(Node* pParent = nullptr);
    /// \ru Деструктор. \en Destructor. 
    virtual ~DiameterDimensionRep();
public:
    /// \ru Вернуть единицы измерения по умолчанию.(единицы являются модельные для правильной конвертации) \en NO TRANSLATION. \~
    String GetDefaultUnits() const;
    /// \ru Установить единицы измерения по умолчанию. (единицы являются модельные для правильной конвертации) \en NO TRANSLATION. \~
    void SetDefaultUnits(const String& units);

    /// \ru Вернуть единицы измерения для вывода. \en NO TRANSLATION. \~
    String GetOutputUnits() const;
    /// \ru Установить единицы измерения для вывода. \en NO TRANSLATION. \~
    void SetOutputUnits(const String& units);
public:
    /// \ru NO TRANSLATION. \en NO TRANSLATION.
    void SetMeasuredGeometry(const MbArc3D& circle);
private:
    VSN_DISABLE_COPY(DiameterDimensionRep)
};

VSN_END_NAMESPACE

#endif /* __VSN_DIAMETERDIMENSIONREP_H */
