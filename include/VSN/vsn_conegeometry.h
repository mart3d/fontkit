﻿#ifndef __VSN_CONEGEOMETRY_H
#define __VSN_CONEGEOMETRY_H

#include "vsn_meshgeometry.h"

VSN_BEGIN_NAMESPACE

class ConeGeometryPrivate;
//------------------------------------------------------------------------------
/** \brief \ru Конус.
           \en A cone. \~
    \ingroup Vision_Geometry
*/
// ---
class VSN_CLASS ConeGeometry : public MeshGeometry
{
    VSN_OBJECT(ConeGeometry);
    VSN_PROP_READ_WRITE(polyStep, GetPolygonStep, SetPolygonStep);
    VSN_PROP_READ_WRITE_NOTIFY(radius, GetRadius, SetRadius, RadiusModified);
    VSN_PROP_READ_WRITE_NOTIFY(height, GetHeight, SetHeight, HeightModified);
public:
    /// \ru Конструктор по умолчанию. \en Default constructor.
    ConeGeometry(Node* pParent = nullptr);
    /// \ru Конструктор с параметрами. \en Default constructor.
    ConeGeometry(double radius, double height, int sag = 60, Node* pParent = nullptr);
    /// \ru Деструктор. \en Destructor. 
    virtual ~ConeGeometry();
public:
    /// \ru Выдать высоту конуса. \en Get height.
    double GetHeight() const;
    /// \ru Вернуть радиус конуса. \en Set radius.
    double GetRadius() const;
    /// \ru Выдать шаг полигона. \en Get polygon step.
    int GetPolygonStep() const;
    /// \ru Задать шаг полигона (должен быть больше нуля). \en Set the polygon step (must be greater than zero).
    // устновить шаг поигона должен быть больше 0
    void SetPolygonStep(int polyStep);
public:
    /// \ru Задать радиус конуса (должен быть больше нуля). \en Set the radius (must be greater than zero).
    VSN_SLOT(Public, SetRadius, void SetRadius(double radius))
        /// \ru Задать высоту конуса (должна быть больше нуля). \en Set the height (must be greater than zero).
    VSN_SLOT(Public, SetHeight, void SetHeight(double height))
public:
    VSN_SIGNAL(Public, RadiusModified, void RadiusModified(double radius), radius)
    VSN_SIGNAL(Public, HeightModified, void HeightModified(double height), height)
public:
    /// \ru Выдать габаритный куб. \en Get a bounding box.
    virtual const MbCube& GetBoundingBox() override;
    /// \ru Обновить геометрию по новым данным. \en NO TRANSLATION.
    virtual void UpdateGeometry() override;
private:
    /// \ru Отрисовать сферу с помощью OpenGL. \en Draw a sphere with OpenGL.
    virtual void OpenGLDraw(const RenderState& state) override;
private:
    VSN_DISABLE_COPY(ConeGeometry);
    VSN_DECLARE_EX_PRIVATE(ConeGeometry);
};

VSN_END_NAMESPACE

#endif /* __VSN_CONEGEOMETRY_H */
