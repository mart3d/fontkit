﻿#ifndef __VSN_BOXREP_H
#define __VSN_BOXREP_H

#include "vsn_geometryrep.h"

VSN_BEGIN_NAMESPACE
//------------------------------------------------------------------------------
/** \brief \ru Параллелепипед.
           \en A box. \~
    \ingroup Vision_Geometry
*/
// ---
class VSN_CLASS BoxRep : public GeometryRep
{
    VSN_OBJECT(BoxRep);
    VSN_PROP_READ_WRITE_NOTIFY(width,  GetWidth,  SetWidth,  WidthModified);
    VSN_PROP_READ_WRITE_NOTIFY(length, GetLength, SetLength, LengthModified);
    VSN_PROP_READ_WRITE_NOTIFY(height, GetHeight, SetHeight, HeightModified);
public:
    explicit BoxRep(Node* pParent = nullptr);
    virtual ~BoxRep();
public:
    /// \ru Выдать ширину параллелепипеда. \en Get width.
    double GetWidth() const;
    /// \ru Выдать длину параллелепипеда. \en Get length.
    double GetLength() const;
    /// \ru Выдать высоту параллелепипеда. \en Get height.
    double GetHeight() const;
public:
    /// \ru Задать ширину параллелепипеда. \en Set width.
    VSN_SLOT(Public, SetWidth, void SetWidth(double width))
    /// \ru Задать длину параллелепипеда. \en Set width.
    VSN_SLOT(Public, SetLength, void SetLength(double length))
    /// \ru Задать высоту параллелепипеда. \en Set height.
    VSN_SLOT(Public, SetHeight, void SetHeight(double height))
public:
    /// \ru Модификация ширины параллелепипеда. \en NO TRANSLATION.
    VSN_SIGNAL(Public, WidthModified,  void WidthModified(double width), width)
    /// \ru Модификация длины параллелепипеда. \en NO TRANSLATION.
    VSN_SIGNAL(Public, LengthModified, void LengthModified(double length), length)
    /// \ru Модификация высоты параллелепипеда. \en NO TRANSLATION.
    VSN_SIGNAL(Public, HeightModified, void HeightModified(double height), height)
private:
    VSN_DISABLE_COPY(BoxRep);
};

VSN_END_NAMESPACE

#endif /* __VSN_BOXREP_H */

