﻿////////////////////////////////////////////////////////////////////////////////
/**
\file
    \brief \ru Класс Variant имеет реализацию объединения для наиболее распространенных 
               типов данных в Vision и поддерживает фиксированный набор типов.
           \en NOT TRANSLATED. \~
*/
////////////////////////////////////////////////////////////////////////////////
#include "vsn_object.h"

#ifndef __VSN_VARIANT_H
#define __VSN_VARIANT_H

#include <map>
#include <mb_cart_point.h>
#include <mb_matrix3d.h>
#include <mb_vector3d.h>
#include "vsn_size.h"
#include "vsn_nodekey.h"
#include "vsn_rect.h"
#include "vsn_color.h"
#include "vsn_string.h"
#include "vsn_global.h"


VSN_BEGIN_NAMESPACE
class Object;
///< \ru Поддерживаемые типы Variant. \en NOT TRANSLATED.
enum VariantType
{
	VAR_NONE = 0,
	VAR_INT,
	VAR_BOOL,
	VAR_FLOAT,
	VAR_VECTOR2,
	VAR_VECTOR3,
	VAR_VECTOR4,
	VAR_COLOR,
	VAR_STRING,
	VAR_VOIDPTR,
    VAR_OBJECTPTR,
    VAR_VARIANTVECTOR,
    VAR_FLOATVECTOR,
    VAR_INTRECT,
	VAR_PTR,
    VAR_SHAREDPTR,
	VAR_MATRIX4,
	VAR_DOUBLE,
	VAR_STRINGVECTOR,
	VAR_NODE_KEY,
	MAX_VAR_TYPES
};

//----------------------------------------------------------------------------
//
/** \brief \ru VariantValue - объединение возможных значений.
           \en NOT TRANSLATED. \~
    \ingroup Vision_Common
*/
// ---
struct VariantValue
{
	union
	{
		int int_;
		bool bool_;
		float float_;
		double double_;
		void* ptr_;
	};

	union
	{
		int int2_;
		float float2_;
		double double2_;
		void* ptr2_;
	};

	union
	{
		int int3_;
		float float3_;
		double double3_;
		void* ptr3_;
	};

	union
	{
		int int4_;
		float float4_;
		double double4_;
		void* ptr4_;
	};
};

class Variant;
typedef std::vector<Variant> VariantVector; ///< \ru Вектор вариантов. \en NOT TRANSLATED.
typedef std::vector<String>  StringVector;  ///< \ru Вектор strings.   \en NOT TRANSLATED.
typedef std::vector<float>   FloatVector;   ///< \ru Вектор Float.     \en NOT TRANSLATED.

//----------------------------------------------------------------------------
//
/** \brief \ru Класс Variant имеет реализацию объединения для наиболее распространенных 
               типов данных C3D Vision и поддерживает фиксированный набор типов.
           \en NOT TRANSLATED. \~
    \ingroup Vision_Common
*/
// ---
class VSN_CLASS Variant
{
public:
    /// \ru Конструктор по умолчанию. \en Default constructor. \~
    Variant() : m_type(VAR_NONE) {}
    /// \ru Конструктор с integer значением. \en NOT TRANSLATED. \~
    Variant(int value) : m_type(VAR_NONE) { *this = value; }
    /// \ru Конструктор с unsigned integer значением. \en NOT TRANSLATED. \~
    Variant(unsigned value) : m_type(VAR_NONE) { *this = (int)value; }
    /// \ru Конструктор с bool значением. \en NOT TRANSLATED. \~
    Variant(bool value) : m_type(VAR_NONE) { *this = value; }
    /// \ru Конструктор с float значением. \en NOT TRANSLATED. \~
    Variant(float value) : m_type(VAR_NONE) { *this = value; }
    /// \ru Конструктор с double значением. \en NOT TRANSLATED. \~
    Variant(double value) : m_type(VAR_NONE) { *this = value; }
    /// \ru Конструктор с MbVector3D значением. \en NOT TRANSLATED. \~
    Variant(const MbVector3D& value) : m_type(VAR_NONE) { *this = value; }
    /// \ru Конструктор с NodeKey значением. \en NOT TRANSLATED. \~
    Variant(const NodeKey& value) : m_type(VAR_NONE) { *this = value; }
    /// \ru Конструктор с MbHomogeneous3D значением. \en NOT TRANSLATED. \~
    Variant(const MbHomogeneous3D& value) : m_type(VAR_NONE) { *this = value; }
    /// \ru Конструктор с Color значением. \en NOT TRANSLATED. \~
    Variant(const Color& value) : m_type(VAR_NONE) { *this = value; }
    /// \ru Конструктор с String значением. \en NOT TRANSLATED.
    Variant(const String& value) : m_type(VAR_NONE) { *this = value; }
    /// \ru Конструктор с указателем на char значением. \en NOT TRANSLATED. \~
    Variant(const char* value) : m_type(VAR_NONE) { *this = value; }
    /// \ru Конструктор с указателем на void значением. \en NOT TRANSLATED. \~
    Variant(void* value) : m_type(VAR_NONE) { *this = value; }
    /// \ru Конструктор с VariantVector значением. \en NOT TRANSLATED. \~
    Variant(const VariantVector& value) : m_type(VAR_NONE) { *this = value; }
    /// \ru Конструктор с StringVector значением. \en NOT TRANSLATED. \~
    Variant(const StringVector& value) : m_type(VAR_NONE) { *this = value; }
    /// \ru Конструктор с FloatVector значением. \en NOT TRANSLATED. \~
    Variant(const FloatVector& value) : m_type(VAR_NONE) { *this = value; }
    /// \ru Конструктор с integer rect значением. \en NOT TRANSLATED. \~
    Variant(const IntRect& value) : m_type(VAR_NONE) { *this = value; }
    /// \ru Конструктор с MbMatrix3D значением. \en NOT TRANSLATED. \~
    Variant(const MbMatrix3D& value) : m_type(VAR_NONE) { *this = value; }
    /// \ru Конструктор с заданием типа и инициализации значения. \en NOT TRANSLATED. \~
    Variant(const String& type, const String& value) : m_type(VAR_NONE) { FromString(type, value); }
    /// \ru Конструктор с заданием типа и инициализации значения. \en NOT TRANSLATED. \~
    Variant(VariantType type, const String& value) : m_type(VAR_NONE) { FromString(type, value); }
    /// \ru Конструктор с заданием типа и инициализации значения. \en NOT TRANSLATED. \~
    Variant(const char* type, const char* value) : m_type(VAR_NONE) { FromString(type, value); }
	/// \ru Конструктор с заданием типа и инициализации значения. \en NOT TRANSLATED. \~
    Variant(VariantType type, const char* value) : m_type(VAR_NONE) { FromString(type, value); }
    /// \ru Конструктор копирования по другому варианту. \en NOT TRANSLATED. \~
    Variant(const Variant& value) : m_type(VAR_NONE) { *this = value; }
    /// \ru Конструктор с указателем на std::shared_ptr<T> значением. \en NOT TRANSLATED. \~
    template<typename T>
    Variant(std::shared_ptr<T> value) : m_type(VAR_SHAREDPTR) { *this = value; }
	/// \ru Деструктор. \en Destructor. \~
    ~Variant() { SetType(VAR_NONE); }

public:
    /// \ru Сбросить вариант от значения и типа. \en NOT TRANSLATED. \~
    void Clear() { SetType(VAR_NONE); }

    /// \ru Присвоить значение и тип из другого варианта. \en NOT TRANSLATED. \~
    Variant& operator =(const Variant& other);
    /// \ru Присвоить значение из integer. \en NOT TRANSLATED. \~
    Variant& operator =(int other) { SetType(VAR_INT); m_value.int_ = other; return *this; }
    /// \ru Присвоить значение из целых чисел без знака. \en NOT TRANSLATED. \~
    Variant& operator =(unsigned other) { SetType(VAR_INT); m_value.int_ = (int)other; return *this; }
    /// \ru Присвоить значение из bool. \en NOT TRANSLATED. \~
    Variant& operator =(bool other) { SetType(VAR_BOOL); m_value.bool_ = other; return *this; }
    /// \ru Присвоить значение из float. \en NOT TRANSLATED. \~
    Variant& operator =(float other) { SetType(VAR_FLOAT); m_value.float_ = other; return *this; }
    /// \ru Присвоить значение из double. \en NOT TRANSLATED. \~
    Variant& operator = (double other) { SetType(VAR_DOUBLE); *(reinterpret_cast<double*>(&m_value)) = other; return *this; }
    /// \ru Присвоить значение из MbVector3D. \en NOT TRANSLATED. \~
    Variant& operator =(const MbVector3D& other) { SetType(VAR_VECTOR3); *(reinterpret_cast<MbVector3D*>(&m_value)) = other; return *this; }
    /// \ru Присвоить значение из NodeKey. \en NOT TRANSLATED. \~
	Variant& operator =(const NodeKey& other) { SetType(VAR_NODE_KEY); *(reinterpret_cast<NodeKey*>(&m_value)) = other; return *this; }
    /// \ru Присвоить значение из MbHomogeneous3D. \en NOT TRANSLATED. \~
    Variant& operator =(const MbHomogeneous3D& other) { SetType(VAR_VECTOR4); *(reinterpret_cast<MbHomogeneous3D*>(&m_value)) = other; return *this; }
    /// \ru Присвоить значение из color. \en NOT TRANSLATED. \~
    Variant& operator =(const Color& other) { SetType(VAR_COLOR); *(reinterpret_cast<Color*>(m_value.ptr_)) = other; return *this; }
    /// \ru Присвоить значение из string. \en NOT TRANSLATED. \~
    Variant& operator =(const String& other) { SetType(VAR_STRING); *(reinterpret_cast<String*>(&m_value)) = other; return *this; }
    /// \ru Присвоить значение из указателя на char. \en NOT TRANSLATED. \~
    Variant& operator =(const char* other) { SetType(VAR_STRING); *(reinterpret_cast<String*>(&m_value)) = String(other); return *this; }
    /// \ru Назначить значение из адреса указателя на void. \en NOT TRANSLATED. \~
    Variant& operator =(void* other) { SetType(VAR_VOIDPTR); m_value.ptr_ = other; return *this; }
    /// \ru Назначить значение из адреса указателя на Object. \en NOT TRANSLATED. \~
    Variant& operator =(Object* other) { SetType(VAR_OBJECTPTR); m_value.ptr_ = other; return *this; }
    /// \ru Назначить значение из вектора вариантов. \en NOT TRANSLATED. \~
    Variant& operator =(const VariantVector& other) { SetType(VAR_VARIANTVECTOR); *(reinterpret_cast<VariantVector*>(&m_value)) = other; return *this; }
    /// \ru Назначить значение из вектора string. \en NOT TRANSLATED. \~
    Variant& operator =(const StringVector& other) { SetType(VAR_STRINGVECTOR); *(reinterpret_cast<StringVector*>(&m_value)) = other; return *this; }
    /// \ru Назначить значение из вектора вариантов. \en NOT TRANSLATED. \~
    Variant& operator =(const FloatVector& other) { SetType(VAR_FLOATVECTOR); *(reinterpret_cast<FloatVector*>(&m_value)) = other; return *this; }
    /// \ru Присвоить значение из целочисленного прямоугольника. \en NOT TRANSLATED. \~
    Variant& operator =(const IntRect& other) { SetType(VAR_INTRECT); *(reinterpret_cast<IntRect*>(&m_value)) = other; return *this;}
    /// \ru Присвоить значение из MbMatrix3D. \en NOT TRANSLATED. \~
    Variant& operator =(const MbMatrix3D& other) { SetType(VAR_MATRIX4); *(reinterpret_cast<MbMatrix3D*>(m_value.ptr_)) = other; return *this; }
    /// \ru Присвоить значение из std::shared_ptr<T>. \en NOT TRANSLATED. \~
    template<typename T> Variant& operator =(std::shared_ptr<T> other) { SetType(VAR_SHAREDPTR); sptr_ = std::static_pointer_cast<void>(other); return *this; }

    /// \ru Проверить равенство с другим вариантом. Вернет true, если вариант равен этому.\en NOT TRANSLATED. \~
    bool operator ==(const Variant& other) const;
    /// \ru Проверить равенство с целым числом. Вернет true, если есть совпадение по типу и значению. \en NOT TRANSLATED. \~
    bool operator ==(int other) const { return m_type == VAR_INT ? m_value.int_ == other : false; }
    /// \ru Проверить равенство с целым числом без знака. Вернет true, если есть совпадение по типу и значению. \en NOT TRANSLATED. \~
    bool operator ==(unsigned other) const { return m_type == VAR_INT ? m_value.int_ == (int)other : false; }
    /// \ru Проверить равенство с bool. Вернет true, если есть совпадение по типу и значению. \en NOT TRANSLATED. \~
    bool operator ==(bool other) const { return m_type == VAR_BOOL ? m_value.bool_ == other : false; }
    /// \ru Проверить равенство с float. Вернет true, если есть совпадение по типу и значению. \en NOT TRANSLATED. \~
    bool operator ==(float other) const { return m_type == VAR_FLOAT ? m_value.float_ == other : false; }
    /// \ru Проверить равенство с double. Вернет true, если есть совпадение по типу и значению. \en NOT TRANSLATED. \~
    bool operator ==(double other) const { return m_type == VAR_DOUBLE ? *(reinterpret_cast<const double*>(&m_value)) == other : false; }
    /// \ru Проверить равенство с MbVector3D. Вернет true, если есть совпадение по типу и значению. \en NOT TRANSLATED. \~
    bool operator ==(const MbVector3D& other) const { return m_type == VAR_VECTOR3 ? *(reinterpret_cast<const MbVector3D*>(&m_value)) == other : false; }
    /// \ru Проверить равенство с NodeKey. Вернет true, если есть совпадение по типу и значению. \en NOT TRANSLATED. \~
    bool operator ==(const NodeKey& other) const { return m_type == VAR_NODE_KEY ? *(reinterpret_cast<const NodeKey*>(&m_value)) == other : false; }
    /// \ru Проверить равенство с MbHomogeneous3D. Вернет true, если есть совпадение по типу и значению. \en NOT TRANSLATED. \~
    bool operator ==(const MbHomogeneous3D& other) const { return m_type == VAR_VECTOR4 ? *(reinterpret_cast<const MbHomogeneous3D*>(&m_value)) == other : false; }
    /// \ru Проверить равенство с color. Вернет true, если есть совпадение по типу и значению. \en NOT TRANSLATED. \~
    bool operator ==(const Color& other) const { return m_type == VAR_COLOR ? *(reinterpret_cast<const Color*>(&m_value.ptr_)) == other : false; }
    /// \ru Проверить равенство с string. Вернет true, если есть совпадение по типу и значению. \en NOT TRANSLATED. \~
    bool operator ==(const String& other) const { return m_type == VAR_STRING ? *(reinterpret_cast<const String*>(&m_value)) == other : false; }
    /// \ru Проверить равенство с указателем на void. Вернет true, если есть совпадение по типу и значению. \en NOT TRANSLATED. \~
    bool operator ==(void* other) const { if (m_type == VAR_VOIDPTR) return m_value.ptr_ == other; else return false; }
    /// \ru Проверить равенство с указателем на Object. Вернет true, если есть совпадение по типу и значению. \en NOT TRANSLATED. \~
    bool operator ==(Object* other) const { if (m_type == VAR_OBJECTPTR) return m_value.ptr_ == other; else return false; }
    /// \ru Проверка на равенство с вариантным вектором. Вернет true, если есть совпадение по типу и значению. \en NOT TRANSLATED. \~
    bool operator ==(const VariantVector& other) const
	{ return m_type == VAR_VARIANTVECTOR ? *(reinterpret_cast<const VariantVector*>(&m_value)) == other : false; }
    /// \ru Проверка на равенство со строковым вектором. Вернет true, если есть совпадение по типу и значению. \en NOT TRANSLATED. \~
    bool operator ==(const StringVector& other) const
	{ return m_type == VAR_STRINGVECTOR ? *(reinterpret_cast<const StringVector*>(&m_value)) == other : false; }
    /// \ru Проверка на равенство с Float вектором. Вернет true, если есть совпадение по типу и значению. \en NOT TRANSLATED. \~
    bool operator ==(const FloatVector& other) const { return m_type == VAR_FLOATVECTOR ? *(reinterpret_cast<const FloatVector*>(&m_value)) == other : false; }
    /// \ru Проверка на равенство с целочисленным прямоугольником. Вернет true, если есть совпадение по типу и значению. \en NOT TRANSLATED. \~
    bool operator ==(const IntRect& other) const { return m_type == VAR_INTRECT ? *(reinterpret_cast<const IntRect*>(&m_value)) == other : false; }
    /// \ru Проверка на равенство с MbMatrix3D. Вернет true, если есть совпадение по типу и значению. \en NOT TRANSLATED. \~
    bool operator ==(const MbMatrix3D& other) const { return m_type == VAR_MATRIX4 ? *(reinterpret_cast<const MbMatrix3D*>(m_value.ptr_)) == other : false; }

    /// \ru Проверка неравенства с другим вариантом. \en NOT TRANSLATED. \~
    bool operator !=(const Variant& other) const { return !(*this == other); }
    /// \ru Проверка неравенства с целым числом. \en NOT TRANSLATED. \~
    bool operator !=(int other) const { return !(*this == other); }
    /// \ru Проверка неравенства с целым числом без знака. \en NOT TRANSLATED. \~
    bool operator !=(unsigned other) const { return !(*this == other); }
    /// \ru Проверка неравенства с bool. \en NOT TRANSLATED. \~
    bool operator !=(bool other) const { return !(*this == other); }
    /// \ru Проверка неравенства с float. \en NOT TRANSLATED. \~
    bool operator !=(float other) const { return !(*this == other); }
    /// \ru Проверка неравенства с double. \en NOT TRANSLATED. \~
    bool operator !=(double other) const { return !(*this == other); }
    /// \ru Проверка неравенства с MbVector3D. \en NOT TRANSLATED. \~
    bool operator !=(const MbVector3D& other) const { return !(*this == other); }
    /// \ru Проверка неравенства с NodeKey. \en NOT TRANSLATED. \~
    bool operator !=(const NodeKey& other) const { return !(*this == other); }
    /// \ru Проверка неравенства с MbHomogeneous3D. \en NOT TRANSLATED. \~
    bool operator !=(const MbHomogeneous3D& other) const { return !(*this == other); }
    /// \ru Проверка неравенства с string. \en NOT TRANSLATED. \~
    bool operator !=(const String& other) const { return !(*this == other); }
    /// \ru Проверка неравенства с указателем на void. \en NOT TRANSLATED. \~
    bool operator !=(void* other) const { return !(*this == other); }
    /// \ru Проверка неравенства с вектором вариантов. \en NOT TRANSLATED. \~
	bool operator !=(const VariantVector& other) const { return !(*this == other); }
    /// \ru Проверка неравенства со строковым вектором. \en NOT TRANSLATED. \~
    bool operator !=(const StringVector& other) const { return !(*this == other); }
    /// \ru Проверка неравенства с float  вектором. \en NOT TRANSLATED. \~
    bool operator !=(const FloatVector& other) const { return !(*this == other); }
    /// \ru Проверка неравенства с Variant Map. \en NOT TRANSLATED. \~
//	bool operator !=(const VariantMap& other) const { return !(*this == other); }
    /// \ru Проверка неравенства с целочисленным прямоугольником. \en NOT TRANSLATED. \~
    bool operator !=(const IntRect& other) const { return !(*this == other); }
    /// \ru Проверка неравенства с MbMatrix3D. \en NOT TRANSLATED. \~
    bool operator !=(const MbMatrix3D& other) const { return !(*this == other); }

    /// \ru Установить из строки тип и значение. Указатели будут установлены в нуль, а тип VariantMap не поддерживается. \en NOT TRANSLATED. \~
    void FromString(const String& type, const String& value);
    /// \ru Установить из строковых символов и строк значений. Указатели будут установлены в нуль, а тип VariantMap не поддерживается. \en NOT TRANSLATED. \~
    void FromString(const char* type, const char* value);
    /// \ru Установить из типа и строковое значение. Указатели будут установлены в нуль, а тип VariantMap не поддерживается. \en NOT TRANSLATED. \~
    void FromString(VariantType type, const String& value);
    /// \ru Установить из типа и строковое значение. Указатели будут установлены в нуль, а тип VariantMap не поддерживается. \en NOT TRANSLATED. \~
    void FromString(VariantType type, const char* value);

    /// \ru Возвращает int или ноль при несоответствии типа. Floats и doubles конвертирует. \en NOT TRANSLATED. \~
    int GetInt() const
	{
		if (m_type == VAR_INT)
			return m_value.int_;
		else if (m_type == VAR_FLOAT)
			return (int)m_value.float_;
		else if (m_type == VAR_DOUBLE)
			return (int)*reinterpret_cast<const double*>(&m_value);
		else
			return 0;
	}

    /// \ru Возвращает целое число без знака или ноль при несоответствии типа. Floats и doubles конвертирует. \en NOT TRANSLATED. \~
    unsigned GetUInt() const
	{
		if (m_type == VAR_INT)
			return m_value.int_;
		else if (m_type == VAR_FLOAT)
			return (unsigned)m_value.float_;
		else if (m_type == VAR_DOUBLE)
			return (unsigned)*reinterpret_cast<const double*>(&m_value);
		else
			return 0;
	}

    /// \ru Возвращает bool или false при несоответствии типа. \en NOT TRANSLATED. \~
    bool GetBool() const { return m_type == VAR_BOOL ? m_value.bool_ : false; }

    /// \ru Вернуть float или ноль при несоответствии типа. \en NOT TRANSLATED. \~
    float GetFloat() const
	{
		if (m_type == VAR_FLOAT)
			return m_value.float_;
		else if (m_type == VAR_DOUBLE)
			return (float)*reinterpret_cast<const double*>(&m_value);
		else if (m_type == VAR_INT)
			return (float)m_value.int_;
		else
			return 0.0f;
	}

    /// \ru Вернуть double или ноль при несоответствии типа. \en NOT TRANSLATED. \~
    double GetDouble() const
	{
		if (m_type == VAR_DOUBLE)
			return *reinterpret_cast<const double*>(&m_value);
		else if (m_type == VAR_FLOAT)
			return (double)m_value.float_;
		else if (m_type == VAR_INT)
			return (double)m_value.int_;
		else
			return 0.0;
	}

    /// \ru Вернуть MbVector3D или ноль при несоответствии типа. \en NOT TRANSLATED. \~
    const MbVector3D& GetVector3() const
    { return m_type == VAR_VECTOR3 ? *reinterpret_cast<const MbVector3D*>(&m_value) : MbVector3D::zero; }
    /// \ru Вернуть NodeKey или ноль при несоответствии типа. \en NOT TRANSLATED. \~
    const NodeKey& GetNodeKey() const { return m_type == VAR_NODE_KEY ? *reinterpret_cast<const NodeKey*>(&m_value) : NodeKey::ZERO; }
    /// \ru Вернуть MbHomogeneous3D или ноль при несоответствии типа. \en NOT TRANSLATED. \~
    const MbHomogeneous3D& GetVector4() const { return m_type == VAR_VECTOR4 ? *reinterpret_cast<const MbHomogeneous3D*>(&m_value) : MbHomogeneous3D::zero; }
    /// \ru Вернуть color или просто белый цвет при несоответствии типа. \en NOT TRANSLATED. \~
    const Color& GetColor() const { return (m_type == VAR_COLOR || m_type == VAR_VECTOR4) ? *reinterpret_cast<const Color*>(m_value.ptr_) : Color::DEFINE_WHITE; }
    /// \ru Вернуть строку или пустую строку при несоответствии типа. \en NOT TRANSLATED. \~
    const String& GetString() const { return m_type == VAR_STRING ? *reinterpret_cast<const String*>(&m_value) : String::EMPTY; }
    /// \ru Вернуть указатель на void или на null при несоответствии типа. \en NOT TRANSLATED. \~
    void* GetVoidPtr() const { return m_type == VAR_VOIDPTR ? m_value.ptr_ : nullptr; }
    /// \ru Вернуть указатель на Object или на null при несоответствии типа. \en NOT TRANSLATED. \~
    Object* GetObjectPtr() const { return m_type == VAR_OBJECTPTR ? (Object*)m_value.ptr_ : nullptr; }
    /// \ru Вернуть вектор вариантов или пустой вектор, если тип не соответствует. \en NOT TRANSLATED. \~
    const VariantVector& GetVariantVector() const { return m_type == VAR_VARIANTVECTOR ? *reinterpret_cast<const VariantVector*>(&m_value) : emptyVariantVector; }
    /// \ru Вернуть строковый вектор или пустой вектор, если тип не соответствует. \en NOT TRANSLATED. \~
    const StringVector& GetStringVector() const { return m_type == VAR_STRINGVECTOR ? *reinterpret_cast<const StringVector*>(&m_value) : emptyStringVector; }
    /// \ru Вернуть Float вектор или пустой вектор, если тип не соответствует. \en NOT TRANSLATED. \~
    const FloatVector& GetFloatVector() const { return m_type == VAR_FLOATVECTOR ? *reinterpret_cast<const FloatVector*>(&m_value) : emptyFloatVector; }
    /// \ru Вернуть целочисленный прямоугольник или пустой прямоугольник, если тип не соответствует. \en NOT TRANSLATED. \~
    const IntRect& GetIntRect() const { return m_type == VAR_INTRECT ? *reinterpret_cast<const IntRect*>(&m_value) : IntRect::ZERO; }
    /// \ru Вернуть MbMatrix3D или единичную матрицу, если тип не соответствует. \en NOT TRANSLATED. \~
    const MbMatrix3D& GetMatrix4() const { return m_type == VAR_MATRIX4 ? *(reinterpret_cast<const MbMatrix3D*>(m_value.ptr_)) : MbMatrix3D::identity; }
    /// \ru Вернуть указатель на void или пустой указатель, если тип не соответствует. \en NOT TRANSLATED. \~
    const std::shared_ptr<void> GetSharedPtr() const { return m_type == VAR_SHAREDPTR ? sptr_ : std::shared_ptr<void>(); }

    /// \ru Вернуть тип варианта. \en NOT TRANSLATED. \~
    VariantType GetType() const { return m_type; }
    /// \ru Вернуть имя типа варианта. \en NOT TRANSLATED. \~
    String GetTypeName() const;
    /// \ru Преобразование значения в строку. Указатели возвращаются как null, а VariantMap не поддерживается и возвращается пустым. \en NOT TRANSLATED. \~
    String ToString() const;
    /// \ru Вернуть true, когда значение варианта считается нулевым в соответствии с его фактическим типом. \en NOT TRANSLATED. \~
    bool IsZero() const;
    /// \ru Вернуть true, когда вариант пуст (т. е. еще не инициализирован). \en NOT TRANSLATED. \~
    bool IsEmpty() const { return m_type == VAR_NONE; }
    /// \ru Вернуть значение шаблонной версии. \en NOT TRANSLATED. \~
    template <class T> T Get() const;

    /// \ru Возвращает указатель на изменяемый вариант вектора или null при несоответствии типов. \en NOT TRANSLATED. \~
	VariantVector* GetVariantVectorPtr() { return m_type == VAR_VARIANTVECTOR ? reinterpret_cast<VariantVector*>(&m_value) : 0; }
    /// \ru Возвращает указатель на изменяемый вектор строки или null при несоответствии типов. \en NOT TRANSLATED. \~
    StringVector* GetStringVectorPtr() { return m_type == VAR_STRINGVECTOR ? reinterpret_cast<StringVector*>(&m_value) : 0; }
    /// \ru Возвращает указатель на изменяемый вариант float или null при несоответствии типов. \en NOT TRANSLATED. \~
    FloatVector* GetFloatVectorPtr() { return m_type == VAR_FLOATVECTOR ? reinterpret_cast<FloatVector*>(&m_value) : 0; }

    /// \ru Вернуть имя для типа варианта. \en NOT TRANSLATED. \~
    static String GetTypeName(VariantType type);
    /// \ru Вернуть тип варианта по типу имени. \en NOT TRANSLATED. \~
    static VariantType GetTypeFromName(const String& typeName);
    /// \ru Вернуть тип варианта по типу имени. \en NOT TRANSLATED. \~
    static VariantType GetTypeFromName(const wchar_t* typeName);

	static const Variant EMPTY;                    ///< \ru Пустой вариант. \en NOT TRANSLATED. \~
	static const VariantVector emptyVariantVector; ///< \ru Пустой вектор варианта. \en NOT TRANSLATED. \~
	static const StringVector emptyStringVector;   ///< \ru Пустой вектор строк. \en NOT TRANSLATED. \~
    static const FloatVector emptyFloatVector;     ///< \ru Пустой вектор float. \en NOT TRANSLATED. \~
private:
    /// \ru Установить новый тип и выделить или освободить память по мере необходимости. \en NOT TRANSLATED. \~
    void SetType(VariantType newType);
private:
    std::shared_ptr<void> sptr_;
	VariantType m_type;   ///< \ru Тип варианта. \en NOT TRANSLATED.
	VariantValue m_value; ///< \ru Значение варианта. \en NOT TRANSLATED.
};

/// \ru Вернуть тип варианта. \en NOT TRANSLATED.
template <typename T> VariantType GetVariantType();

/// \ru Вернуть тип варианта по заданному конкретному типу. \en NOT TRANSLATED.
template <> inline VariantType GetVariantType<int>()                { return VAR_INT; }
template <> inline VariantType GetVariantType<unsigned>()           { return VAR_INT; }
template <> inline VariantType GetVariantType<bool>()               { return VAR_BOOL; }
template <> inline VariantType GetVariantType<float>()              { return VAR_FLOAT; }
template <> inline VariantType GetVariantType<double>()             { return VAR_DOUBLE; }
template <> inline VariantType GetVariantType<MbVector3D>()         { return VAR_VECTOR3; }
template <> inline VariantType GetVariantType<NodeKey>()            { return VAR_NODE_KEY; }
template <> inline VariantType GetVariantType<MbHomogeneous3D>()    { return VAR_VECTOR4; }
template <> inline VariantType GetVariantType<Color>()              { return VAR_COLOR; }
template <> inline VariantType GetVariantType<String>()             { return VAR_STRING; }
template <> inline VariantType GetVariantType<VariantVector>()      { return VAR_VARIANTVECTOR; }
template <> inline VariantType GetVariantType<StringVector>()       { return VAR_STRINGVECTOR; }
template <> inline VariantType GetVariantType<FloatVector>()        { return VAR_FLOATVECTOR; }
template <> inline VariantType GetVariantType<IntRect>()               { return VAR_INTRECT; }
template <> inline VariantType GetVariantType<MbMatrix3D>()         { return VAR_MATRIX4; }

// Специализация для Variant::Get<T>
template <> int                     Variant::Get<int>() const;
template <> unsigned                Variant::Get<unsigned>() const;
template <> bool                    Variant::Get<bool>() const;
template <> float                   Variant::Get<float>() const;
template <> double                  Variant::Get<double>() const;
template <> const MbVector3D&       Variant::Get<const MbVector3D&>() const;
template <> const NodeKey&          Variant::Get<const NodeKey&>() const;
template <> const MbHomogeneous3D&  Variant::Get<const MbHomogeneous3D&>() const;
template <> const Color&            Variant::Get<const Color&>() const;
template <> const String&           Variant::Get<const String&>() const;
template <> const IntRect&             Variant::Get<const IntRect&>() const;
template <> void*                   Variant::Get<void*>() const;
template <> const MbMatrix3D&       Variant::Get<const MbMatrix3D&>() const;
template <> VariantVector           Variant::Get<VariantVector>() const;
template <> StringVector            Variant::Get<StringVector>() const;
template <> FloatVector             Variant::Get<FloatVector>() const;
template <> MbVector3D              Variant::Get<MbVector3D>() const;
template <> NodeKey                 Variant::Get<NodeKey>() const;
template <> MbHomogeneous3D         Variant::Get<MbHomogeneous3D>() const;
template <> Color                   Variant::Get<Color>() const;
template <> String                  Variant::Get<String>() const;
template <> IntRect                    Variant::Get<IntRect>() const;
template <> MbMatrix3D              Variant::Get<MbMatrix3D>() const;

VSN_END_NAMESPACE

#endif // __VSN_VARIANT_H
