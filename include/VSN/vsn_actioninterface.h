﻿#ifndef __ACTIONINTERFACE_H
#define __ACTIONINTERFACE_H

#include <string>
#include "vsn_global.h"

VSN_BEGIN_NAMESPACE

  //-----------------------------------------------------------------------------
  // типы действий
  // ---
  enum ActionType 
  {
    ActNone = 0,   /// идентификатор действия недействителен
    ActDefault = 1, 
    ActCustomBase = 0xf0000000
  };

  class OKeyEvent;
  class OMouseEvent;
  class ContextEvent;
  class GraphicView;
  class ActionInterfacePrivate;
  ///////////////////////////////////////////////////////////////////////////////
  //
  // 
  //
  ///////////////////////////////////////////////////////////////////////////////
  class VSN_CLASS ActionInterface 
  {
  public:
    // конструктор по умолчанию
    ActionInterface( const std::wstring& name, GraphicView* pGraphicView );
    // деструктор
    virtual ~ActionInterface();

  public:
    // установить имя действия
    void SetName( const std::wstring& name );
    // вернуть имя действия
    const std::wstring& GetName() const;
    // вернуть идентификатор действия
    virtual ActionType GetTypeAction();

  public:
    // инициализация состояния этого действия
    virtual void Start( int status = 0 );
    // завершить это действие
    virtual void Stop( bool update = true );
    // действия, которые автоматический заканчивают свою работу и удаляются
    virtual bool IsAutoStop() const { return false; }
    // вернуть признак остановки действия
    virtual bool IsStoped();
    // прекращение действия боз очистки
    virtual void SetStoped();
    // возобновляет действие после того, как оно было приостановлено
    virtual void Resume();
    // cкрывает параметры инструмента. Реализация по умолчанию не выполняет никаких действий.
    virtual void HideOptions();
    // показывает параметры инструмента. Реализация по умолчанию не выполняет никаких действий.
    virtual void ShowOptions();
    // установить текущее состояние этого действия
    virtual void SetState( int status );
    // вернуть текущее состояние этого действия
    virtual int GetState();
    // установить предыдущее действие
    virtual void SetPrevAction( ActionInterface* pre );
    // приостанавливает действие при работающем другом действии
    virtual void Suspend();
    // признак приостановленности
    bool IsSuspend();
    // переключить действие
    virtual void Trigger();
    // функция для обновления подсказки из-под мыши
    virtual void UpdateMouseButtonHints();
    // обновить курсор мыши
    virtual void UpdateMouseCursor();
    // обновить строку состояния
    virtual void UpdateStatusBarMessage(int status);

  public:
      virtual void KeyPressEvent(OKeyEvent& ev);
      virtual void KeyReleaseEvent(OKeyEvent& ev);
    virtual void MouseMoveEvent( OMouseEvent& ev );
    virtual void MousePressEvent( OMouseEvent& ev );
    virtual void MouseReleaseEvent( OMouseEvent& ev );
    virtual void ContextMenuEvent( ContextEvent& ev );

  protected:
    // предшественник этого действия или V_NULL.
    ActionInterface* m_pPrevAction;

  private:
    bool m_finished; /// признак прекращения и удаления действия
    bool m_suspend; /// признак приостановленности процесса
    int m_status; /// номер состояния действия

  protected: // должно быть у Snapper 
    GraphicView* m_pGraphicView;

  private:
    VSN_DECLARE_PRIVATE(ActionInterface)
  };

VSN_END_NAMESPACE

#endif /* __ACTIONINTERFACE_H */
