﻿////////////////////////////////////////////////////////////////////////////////
/** 
  \file
  \brief \ru NO TRANSLATION.
         \en NO TRANSLATION. \~

*/
////////////////////////////////////////////////////////////////////////////////

#ifndef __VSN_ANGLEDIMENSIONGEOMETRY_H
#define __VSN_ANGLEDIMENSIONGEOMETRY_H

#include "vsn_dimensiongeometry.h"
#include "vsn_global.h"

VSN_BEGIN_NAMESPACE
class AngleDimensionGeometryPrivate;
//------------------------------------------------------------------------------
/** \brief \ru NO TRANSLATION.
           \en NO TRANSLATION. \~
  \details \ru NO TRANSLATION. \n.
           \en NO TRANSLATION. \n \~
  \ingroup NO GROUP
*/
// ---
class VSN_CLASS AngleDimensionGeometry : public DimensionGeometry
{
    VSN_OBJECT(AngleDimensionGeometry)
public:
    /// \ru Конструктор по умолчанию. \en Default constructor. \~
    AngleDimensionGeometry(Node* pParent = nullptr);
    /// \ru Деструктор. \en Destructor. \~
    virtual ~AngleDimensionGeometry();
public:
    /// \ru Tип угла. \en NO TRANSLATION.\~
    enum TypeAngle
    {
        ta_Internal, ///< \ru Угол между двумя линиями. \en NO TRANSLATION. \~
        ta_Exterior  ///< \ru Угол равный 2_PI минус внутренний угол. \en NO TRANSLATION. \~
    };
public:
    /// \ru Измеряет угол, определяемый тремя точками. \en NO TRANSLATION. \~
    void SetMeasuredGeometry(const MbCartPoint3D& center, const MbCartPoint3D& pnt1, const MbCartPoint3D& pnt2);
public:
    /// \ru Вернуть единицы измерения по умолчанию.(единицы являются модельные для правильной конвертации) \en NO TRANSLATION. \~
    virtual String GetDefaultUnits() const override;
    /// \ru Установить единицы измерения по умолчанию. (единицы являются модельные для правильной конвертации) \en NO TRANSLATION. \~
    virtual void SetDefaultUnits(const String& units) override;

    /// \ru Вернуть единицы измерения для вывода. \en NO TRANSLATION. \~
    virtual String GetOutputUnits() const override;
    /// \ru Установить единицы измерения для вывода. \en NO TRANSLATION. \~
    virtual void SetOutputUnits(const String& units) override;

    /// \ru Получить позицию текста 2D - размера. \en NO TRANSLATION. \~
    virtual const MbCartPoint3D GetTextPosition() const override;
    /// \ru Установить позицию текста 2D - размера. \en NO TRANSLATION. \~
    virtual void SetTextPosition(const MbCartPoint3D& textPos) override;
public:
    /// \ru Выдать габаритный куб. \en Get a bounding box. \~
    virtual const MbCube& GetBoundingBox();
protected:
    /// \ru Вернуть вычисленное значение измерения. \en NO TRANSLATION. \~
    double CalculateValue() const override;
protected:
    VSN_DECLARE_EX_PRIVATE(AngleDimensionGeometry)
};

VSN_END_NAMESPACE

#endif /* __VSN_ANGLEDIMENSIONGEOMETRY_H */
