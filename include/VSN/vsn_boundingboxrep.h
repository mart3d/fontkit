﻿////////////////////////////////////////////////////////////////////////////////
/** 
  \file
  \brief \ru NO TRANSLATION.
         \en NO TRANSLATION. \~

*/
////////////////////////////////////////////////////////////////////////////////

#ifndef __VSN_BOUNDINGBOXREP_H
#define __VSN_BOUNDINGBOXREP_H

#include "vsn_geometryrep.h"
#include "vsn_namespace.h"
#include "vsn_global.h"

VSN_BEGIN_NAMESPACE
class SceneSegment;
class BoundingBoxRepPrivate;
//------------------------------------------------------------------------------
/** \brief \ru NO TRANSLATION.
           \en NO TRANSLATION. \~
  \details \ru NO TRANSLATION. \n.
           \en NO TRANSLATION. \n \~
  \ingroup NO GROUP
*/
// ---
class VSN_CLASS BoundingBoxRep : public GeometryRep
{
    VSN_OBJECT(BoundingBoxRep)
public:
    /// \ru Конструктор по умолчанию. \en Default constructor.
    BoundingBoxRep();
    /// \ru Деструктор. \en Destructor. 
    virtual ~BoundingBoxRep();
public:
    /// \ru NO TRANSLATION. \en NO TRANSLATION.
    bool IsVisibleAxis(AxisOrientation orientation) const;
    /// \ru NO TRANSLATION. \en NO TRANSLATION.
    void SetVisibleAxis(AxisOrientation orientation, bool visible);

    /// \ru NO TRANSLATION. \en NO TRANSLATION.
    bool IsVisibleTips(AxisOrientation orientation) const;
    /// \ru NO TRANSLATION. \en NO TRANSLATION.
    void SetVisibleTips(AxisOrientation orientation, bool visible);

    /// \ru NO TRANSLATION. \en NO TRANSLATION.
    bool IsVisibleLabels(AxisOrientation orientation) const;
    /// \ru NO TRANSLATION. \en NO TRANSLATION.
    void SetVisibleLabels(AxisOrientation orientation, bool visible);

    /// \ru NO TRANSLATION. \en NO TRANSLATION.
    bool IsReversedAxis(AxisOrientation orientation) const;
    /// \ru NO TRANSLATION. \en NO TRANSLATION.
    void SetReversedAxis(AxisOrientation orientation, bool bEnabled);

    /// \ru NO TRANSLATION. \en NO TRANSLATION.
    bool IsAutoAdjustOrigin(AxisOrientation orientation) const;
    /// \ru NO TRANSLATION. \en NO TRANSLATION.
    void SetAutoAdjustOrigin(AxisOrientation orientation, bool bAuto);

    /// \ru NO TRANSLATION. \en NO TRANSLATION.
    int GetSegmentCount(AxisOrientation orientation) const;
    /// \ru NO TRANSLATION. \en NO TRANSLATION.
    void SetSegmentCount(AxisOrientation orientation, int count);

    /// \ru NO TRANSLATION. \en NO TRANSLATION.
    int GetFormatPrecision(AxisOrientation orientation) const;
    /// \ru NO TRANSLATION. \en NO TRANSLATION.
    void SetFormatPrecision(AxisOrientation orientation, int precision);

    /// \ru NO TRANSLATION. \en NO TRANSLATION.
    double GetBoundingBoxEdgeWidth() const;
    /// \ru NO TRANSLATION. \en NO TRANSLATION.
    void SetBoundingBoxEdgeWidth(double width);

    /// \ru NO TRANSLATION. \en NO TRANSLATION.
    bool IsDimensionVisible() const;
    /// \ru NO TRANSLATION. \en NO TRANSLATION.
    void SetDimensionVisible(bool bVisible);

    /// \ru Вернуть цвет размеров. \en NO TRANSLATION.
    Color GetColorDimension() const;
    /// \ru Установить цвет размеров. \en NO TRANSLATION.
    void SetColorDimension(const Color& color);

    /// \ru Вернуть цвет оси X. \en NO TRANSLATION.
    Color GetColorAxis(AxisOrientation orientation) const;
    /// \ru Установить цвет оси X. \en NO TRANSLATION.
    void SetColorAxis(AxisOrientation orientation, const Color& color);

    /// \ru NO TRANSLATION. \en NO TRANSLATION.
    Color GetColorLabels(AxisOrientation orientation) const;
    /// \ru NO TRANSLATION. \en NO TRANSLATION.
    void SetColorLabels(AxisOrientation orientation, const Color& color);

    /// \ru NO TRANSLATION. \en NO TRANSLATION.
    const SceneSegment* GetSceneSegment() const;
    /// \ru NO TRANSLATION. \en NO TRANSLATION.
    void SetSceneSegment(const SceneSegment* pSegment);
public:
    VSN_SLOT(Protected, BoundingBoxModified, void BoundingBoxModified())
private:
    VSN_DECLARE_PRIVATE(BoundingBoxRep);
};

VSN_END_NAMESPACE

#endif /* __VSN_BOUNDINGBOXREP_H */
