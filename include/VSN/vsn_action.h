﻿#ifndef __VSN_ACTION_H
#define __VSN_ACTION_H

#include "vsn_node.h"
#include "vsn_global.h"

VSN_BEGIN_NAMESPACE

class ActionPrivate;
class AbstractActionInput;
/* Action */
class VSN_CLASS Action : public Node
{
    VSN_OBJECT(Action);
    VSN_PROP_READ_NOTIFY(active, IsActive, ActiveModified)
public:
    explicit Action(Node* pParent = nullptr);
    virtual ~Action();
public:
    bool IsActive() const;
    std::vector<AbstractActionInput*> Inputs() const;
    void AddInput(AbstractActionInput* input);
    void RemoveInput(AbstractActionInput* input);
public:
    VSN_SIGNAL(Public, ActiveModified, void ActiveModified(bool bIsActive), bIsActive)
protected:
    virtual void SceneModificationEvent(const std::shared_ptr<SceneModification>& modification) override;
private:
    NCreatedModificationBasicPtr CreateNodeModification() const override;
private:
    VSN_DECLARE_EX_PRIVATE(Action)
    VSN_DISABLE_COPY(Action)
};


VSN_END_NAMESPACE

#endif // __VSN_ACTION_H
