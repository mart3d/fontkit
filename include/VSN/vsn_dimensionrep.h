﻿#ifndef __VSN_DIMENSIONREP_H
#define __VSN_DIMENSIONREP_H

#include "vsn_geometryrep.h"
#include "vsn_dimensiongeometry.h"

VSN_BEGIN_NAMESPACE
//------------------------------------------------------------------------------
/** \brief \ru NO TRANSLATION.
           \en NO TRANSLATION. \~
    \ingroup Vision_Geometry
*/
// ---
class VSN_CLASS DimensionRep : public GeometryRep
{
    VSN_OBJECT(DimensionRep)
//    VSN_PROP_READ_WRITE(polyStep, GetPolygonStep, SetPolygonStep)
    VSN_PROP_READ_WRITE_NOTIFY(orientation,  GetTextOrientation, SetTextOrientation, TextOrientationModified)
    VSN_PROP_READ_WRITE_NOTIFY(vertPosition, GetTextVPosition,   SetTextVPosition,   TextVPositionModified)
    VSN_PROP_READ_WRITE_NOTIFY(horzPosition, GetTextHPosition,   SetTextHPosition,   TextHPositionModified)
protected:
    /// \ru Конструктор по умолчанию. \en Default constructor.
    explicit DimensionRep(DimensionGeometry* pGeometry, Node* pParent);
public:
    /// \ru Деструктор. \en Destructor. 
    virtual ~DimensionRep();
public:
    /// \ru Вернуть ориентацию текста на размерной линии. \en NO TRANSLATION.
    DimensionGeometry::TextOrientation GetTextOrientation() const;
    /// \ru Получает вертикальное выравнивание текста. \en NO TRANSLATION.
    DimensionGeometry::TextVPosition GetTextVPosition() const;
    /// \ru Получает горизонтальное выравнивание текста. \en NO TRANSLATION.
    DimensionGeometry::TextHPosition GetTextHPosition() const;
public:
    /// \ru Установить ориентацию текста на размерной линии. \en NO TRANSLATION.
    VSN_SLOT(Public, SetTextOrientation, void SetTextOrientation(DimensionGeometry::TextOrientation orientation))
    /// \ru Устанавливает вертикальное выравнивание текста. \en NO TRANSLATION.
    VSN_SLOT(Public, SetTextVPosition, void SetTextVPosition(DimensionGeometry::TextVPosition vertPosition))
    /// \ru Устанавливает горизонтальное выравнивание текста. \en NO TRANSLATION.
    VSN_SLOT(Public, SetTextHPosition, void SetTextHPosition(DimensionGeometry::TextHPosition horzPosition))
public:
    VSN_SIGNAL(Public, TextOrientationModified, void TextOrientationModified(DimensionGeometry::TextOrientation orientation), orientation)
    VSN_SIGNAL(Public, TextVPositionModified,   void TextVPositionModified(DimensionGeometry::TextVPosition vertPosition), vertPosition)
    VSN_SIGNAL(Public, TextHPositionModified,   void TextHPositionModified(DimensionGeometry::TextHPosition horzPosition), horzPosition)
private:
    VSN_DISABLE_COPY(DimensionRep)
};

VSN_END_NAMESPACE

#endif /* __VSN_CYLINDERREP_H */
