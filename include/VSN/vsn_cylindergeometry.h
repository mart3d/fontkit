﻿#ifndef __VSN_CYLINDERGEOMETRY_H
#define __VSN_CYLINDERGEOMETRY_H

#include "vsn_meshgeometry.h"

VSN_BEGIN_NAMESPACE

class CylinderGeometryPrivate;
//------------------------------------------------------------------------------
/** \brief \ru Цилиндр.
           \en A cylinder. \~
    \ingroup Vision_Geometry
*/
// ---
class VSN_CLASS CylinderGeometry : public MeshGeometry
{
    VSN_OBJECT(CylinderGeometry);
    VSN_PROP_READ_WRITE(polyStep, GetSpaceStep, SetSpaceStep);
    VSN_PROP_READ_WRITE_NOTIFY(radius, GetRadius, SetRadius, RadiusModified);
    VSN_PROP_READ_WRITE_NOTIFY(height, GetHeight, SetHeight, HeightModified);
public:
    /// \ru Конструктор по умолчанию. \en Default constructor.
    CylinderGeometry(Node* pParent = nullptr);
    /// \ru Конструктор по радиусу, высоте и направлению оси Z. \en Сonstructor by the radius, the height and the direction of the axis Z.
    CylinderGeometry(double radius, double height, int sag = 60, Node* pParent = nullptr);
    /// \ru Деструктор. \en Destructor. 
    virtual ~CylinderGeometry();
public:
    /// \ru Выдать высоту цилиндра. \en Get height.
    double GetHeight() const;
    /// \ru Выдать радиус цилиндра. \en Get radius.
    double GetRadius() const;
    /// \ru Выдать шаг полигона. \en Get polygon step.
    int GetSpaceStep() const;
    /// \ru Задать шаг полигона (должен быть больше нуля). \en Set the polygon step (must be greater than zero).
    // устновить шаг поигона должен быть больше 0
    void SetSpaceStep(int polyStep);

    /// \ru Определить, замкнут ли цилиндр. \en Determine whether the cylinder is closed.
    bool IsClosed() const;
    /// \ru Установить флаг замкнутости цилиндра. \en Set the flag of the closedness of the cylinder.
    void SetClose(bool clouse);
public:
    /// \ru Задать радиус цилиндра. \en Set radius.
    VSN_SLOT(Public, SetRadius, void SetRadius(double radius))
    /// \ru Задать высоту цилиндра. \en Set height.
    VSN_SLOT(Public, SetHeight, void SetHeight(double height))
public:
    VSN_SIGNAL(Public, RadiusModified, void RadiusModified(double radius), radius)
    VSN_SIGNAL(Public, HeightModified, void HeightModified(double height), height)
public:
    /// \ru Выдать габаритный куб. \en Get a bounding box.
    virtual const MbCube& GetBoundingBox() override;
    /// \ru Обновить геометрию по новым данным. \en NO TRANSLATION.
    virtual void UpdateGeometry() override;
private:
    /// \ru Отрисовать сферу с помощью OpenGL. \en Draw a sphere with OpenGL.
    virtual void OpenGLDraw(const RenderState& state) override;
private:
    VSN_DISABLE_COPY(CylinderGeometry);
    VSN_DECLARE_EX_PRIVATE(CylinderGeometry);
};

VSN_END_NAMESPACE

#endif /* __VSN_CYLINDERGEOMETRY_H */
