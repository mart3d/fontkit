﻿////////////////////////////////////////////////////////////////////////////////
/**
\file
    \brief \ru Класс GraphicsScene представляет реализацию отрисовки сцены OpenGl'ем.
           \en NO TRANSLATION. \~
*/
////////////////////////////////////////////////////////////////////////////////
#ifndef __VSN_GRAPHICSSCENEENGINE_H
#define __VSN_GRAPHICSSCENEENGINE_H

#include "vsn_visionengine.h"
#include "vsn_global.h"

VSN_BEGIN_NAMESPACE
class GraphicsScene;
class ObjectPickSelection;
class GraphicsSceneEnginePrivate;
/* GraphicsSceneEngine */
class VSN_CLASS GraphicsSceneEngine : public Object
{
    VSN_OBJECT(GraphicsSceneEngine);
public:
    /// \ru Конструктор по умолчанию. \en NO TRANSLATION.
    explicit GraphicsSceneEngine();
    /// \ru Деструктор. \en NO TRANSLATION.
    virtual ~GraphicsSceneEngine();
public:
    /// \ru Инициализация. \en NO TRANSLATION.
    void Initialize();
    /// \ru Подготовка к отображению. \en NO TRANSLATION.
    void PreparingToDisplay();
    /// \ru Вернуть указатель на Essence. \en NO TRANSLATION.
    Essence* GetTopEssence() const;
    /// \ru Вернуть указатель на сцену отрисовки графа. \en NO TRANSLATION.
    GraphicsScene* GetGraphicsScene() const;
    /// \ru Вернуть указатель на компонент выбора объектов. \en NO TRANSLATION.
    ObjectPickSelection* GetObjectPickSelection() const;
private:
    VSN_DISABLE_COPY(GraphicsSceneEngine);
    VSN_DECLARE_PRIVATE(GraphicsSceneEngine);
};
VSN_END_NAMESPACE

#endif // __VSN_GRAPHICSSCENEENGINE_H
