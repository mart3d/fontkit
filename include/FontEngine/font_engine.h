#pragma once

#include <mb_placement3d.h>
#include "vsn_string.h"
#include "vsn_meshgeometry.h"

#ifdef FONT_ENGINE_EXPORTS
    #if defined(_WIN32) && !defined(__MINGW32__)
        #define FONT_ENGINE_API __declspec(dllexport)
    #else
        #define FONT_ENGINE_API __attribute__ ((visibility ("default")))
    #endif
#else
    #if defined(_WIN32) && !defined(__MINGW32__)
        #define FONT_ENGINE_API __declspec(dllimport)
        #pragma comment( lib, "FONT_ENGINE" )
    #else
        #define FONT_ENGINE_API __attribute__ ((visibility ("default")))
    #endif
#endif

struct Options
{        
    std::string font_family;
    bool bold = false;
    bool italic = false;
    bool underline = false;
    bool strikethrought = false;
    int font_tex_size = 100;
    bool antialiasing = false;

    float rect_height = 1;
    unsigned char font_color_rgba[4] = { 0, 0, 0, 0 };
    MbPlacement3D placement = MbPlacement3D( MbCartPoint3D( 0, 0, 0 ), MbVector3D( 0, 0, 1 ), MbVector3D( 1, 0, 0 ), false );

    bool monolithic = true;
    bool build_backrect = false;
};

struct Page
{
    std::vector<float> vertices;
    std::vector<unsigned char> texture_data;
    int texture_size[2] = { 0, 0 };
};

struct Glyph
{
    int carriage_return = 0;
    int size[2] = { 0, 0 };    // size of glyph
    int bearing[2] = { 0, 0 }; // offset from baseline to left/top of glyph
    int advance[2] = { 0, 0 }; // offset to advance to next glyph
    int kerning = 0;
    std::vector<unsigned char> buffer; // glyph texture
};

struct RenderOptions
{    
    enum TextureFilter
    {
        TF_Undefined = 0,
        TF_Nearest,
        TF_Linear
    };

    unsigned char backrect_color[4] = { 128, 128, 128, 0 };
    std::vector<float> backrect_vertices;
    TextureFilter tex_filter = TF_Nearest;
};


struct Result
{
    std::vector<Page> pages;
    std::vector<float> backrect;        
};

class IFontGeometry : public VSN::MeshGeometry {};

FONT_ENGINE_API void Build( const VSN::WString& text, Result& result, const Options* options );

