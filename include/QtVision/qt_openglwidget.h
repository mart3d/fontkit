﻿////////////////////////////////////////////////////////////////////////////////
/**
  \file
  \brief \ru Класс QtOpenGLWidget реализует интеграцию Vision c Qt. Представленные классы 
             необходимы для рендеринга графики Vision с помощью OpenGL.
         \en NO TRANSLATION. \~
*/
////////////////////////////////////////////////////////////////////////////////
#ifndef __OPENGLWIDGET_H
#define __OPENGLWIDGET_H

#include <QWidget>
#include <QGLWidget>
#include <QOpenGLWidget>
#include <QApplication>
#include <QSurfaceFormat>
#include <QDesktopWidget>

#include <vsn_vision.h>
#include <vsn_application.h>
#include <vsn_meshgeometry.h>
#include <vsn_objectpickselection.h>
#include <vsn_openglcontextinterface.h>

#include "qt_visiondef.h"

namespace VSN_NAMESPACE
{
    /** \brief \ru ВНИМАНИЕ! Этот файл не является частью API Vision. Он необходим для иллюстрации использования
                   ядра Vision с библиотекой Qt и ее классами. Этот заголовочный файл может изменяться от версии
                   к версии без предупреждения или полностью удаляться.
               \en NO TRANSLATION. \~
    */

    //------------------------------------------------------------------------------
    /** \brief \ru Класс QtConverterEventListener служит для интеграции с событийной моделью Qt и Vision.
               \en NO TRANSLATION. \~
        \details \ru В первую очередь, QtConverterEventListener необходим для обеспечения работы Vision
                     с манипуляторами, такими как мышь, клавиатура и др. Кроме того, ConverterEvent служит для
                     конвертации определенных событий Qt в понятные события для Vision.
                 \en NO TRANSLATION. \n \~
        \ingroup Vision_OpenGL
    */
    // ---
    class QT_CLASS QtConverterEventListener : public QObject
    {
    public:
        /// \ru Конструктор по умолчанию. \en NO TRANSLATION. \~
        explicit QtConverterEventListener(QObject* pParent);
        /// \ru Деструктор. \en NO TRANSLATION. \~
        virtual ~QtConverterEventListener();
    public:
        /// \ru Установить слушателя событий. \en NO TRANSLATION.
        void setListenerEvent(Object* pListenerEvent);
    protected:
        bool mouseHoverEvent(QHoverEvent* event);
        bool mousePressEvent(QMouseEvent* event);
        bool mouseMoveEvent(QMouseEvent* event);
        bool mouseReleaseEvent(QMouseEvent* event);
        bool mouseDoubleClickEvent(QMouseEvent* event);
    protected:
        /// \ru Перегрузка для внутренних работ. \en NO TRANSLATION.
        virtual bool eventFilter(QObject* watched, QEvent* event);
    protected:
        Object* m_pListenerEvent;
    };

    class QtOpenGLWidgetPrivate;
    //------------------------------------------------------------------------------
    /** \brief \ru Класс QtOpenGLWidget реализует интеграцию Vision c Qt.
               \en NO TRANSLATION. \~
        \details \ru QtOpenGLWidget необходим для работы Vision с другими классами библиотеки Qt. Например,
                     как самостоятельный виджет или может быть применен в работе с цепочкой QGraphicsView
                     и QGraphicsScene. Поскольку Vision внутри работает с собственным представлением OpenGl
                     контекста (OpenGLContextInterface), класс QtOpenGLWidget содержит некоторые
                     оболочки, обеспечивающие совместную работу QOpenGLContext и OpenGLContextInterface.
                 \en NO TRANSLATION. \n \~
        \ingroup Vision_OpenGL
    */
    // ---
    class QT_CLASS QtOpenGLWidget : public QOpenGLWidget, public Object
    {
        Q_OBJECT
        VSN_OBJECT(QtOpenGLWidget)
    public:
        /// \ru Конструктор по умолчанию. \en NO TRANSLATION. \~
        explicit QtOpenGLWidget(QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
        /// \ru Деструктор OpenGLWidget освобождает созданные ресурсы. \en NO TRANSLATION. \~
        virtual ~QtOpenGLWidget();
    public:
        /**
        \brief \ru Подготовка OpenGL для рендеринга в этом виджете(установить текущий OpenGL контекст).
               \en NO TRANSLATION.
            \detaild \ru Нет необходимости вызывать эту функцию в большинстве случаев, потому что она
                         вызывается автоматически перед вызовом paintGL().
                     \en NO TRANSLATION.
        */
        using QOpenGLWidget::makeCurrent;
        void makeCurrent();
        /**
        \brief \ru Освободить OpenGL контекст.
               \en NO TRANSLATION.
            \detaild \ru Нет необходимости вызывать эту функцию в большинстве случаев, поскольку
                         в функции paintGL() контекст будет назначен и освобожден.
                     \en NO TRANSLATION.
        */
        using QOpenGLWidget::doneCurrent;
        void doneCurrent();
    public:
        /// \ru Просто вызывает функцию QWidget::update(). Служит для совместимости слотов. \en NO TRANSLATION. \~
        VSN_SLOT(Public, updateWidget, void updateWidget())
    protected:
        /// \ru Инициализация OpenGL ресурсов. \en NO TRANSLATION. \~
        virtual void initializeGL() override;
    protected:
        virtual bool event(QEvent* event) override;
    protected:
        QtOpenGLWidget(QtOpenGLWidgetPrivate& dd, QWidget* parent, Qt::WindowFlags f);
        VSN_DECLARE_PRIVATE(QtOpenGLWidget);
    private:
        Q_DISABLE_COPY(QtOpenGLWidget);
    };

    class QtOpenGLSceneWidgetPrivate;
    //------------------------------------------------------------------------------
    /** \brief \ru Класс QtOpenGLSceneWidget - это виджет для рендеринга графики Vision с помощью OpenGL.
               \en NO TRANSLATION. \~
        \details \ru Класс QtOpenGLSceneWidget полностью самостоятельный, создает и инициализирует
                     в полной мере компоненты Vision. Этот класс представляет необходимые функции для работы с Vision.
                     Класс QtOpenGLSceneWidget самостоятельно устанавливает текущий контест OpenGL, когда это необходимо.
                 \en NO TRANSLATION. \n \~
        \ingroup Vision_OpenGL
    */
    // ---
    class QT_CLASS QtOpenGLSceneWidget : public QtOpenGLWidget
    {
        Q_OBJECT
        VSN_OBJECT(QtOpenGLSceneWidget)
    public:
        /// \ru Конструктор по умолчанию. \en NO TRANSLATION. \~
        explicit QtOpenGLSceneWidget(QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
        /// \ru Деструктор OpenGLWidget освобождает созданные ресурсы. \en NO TRANSLATION. \~
        virtual ~QtOpenGLSceneWidget();
    public:
        /// \ru Вернуть указатель на источник освешения сцены. \en NO TRANSLATION. \~
        Light* mainLight() const;
        /// \ru Вернуть указатель на камеру. \en NO TRANSLATION. \~
        Camera* camera() const;
        /// \ru Вернуть указатель на Viewport для отображения сцены. \en NO TRANSLATION. \~
        Viewport* viewport() const;
        /// \ru Вернуть указатель на содержимое сцены. \en NO TRANSLATION. \~
        SceneContent* sceneContent();
        /// \ru Вернуть указатель на графическую сцену. \en NO TRANSLATION. \~
        GraphicsScene* graphicsScene() const;
        /// \ru Вернуть указатель на компонент выбора объектов. \en NO TRANSLATION. \~
        ObjectPickSelection* objectPickSelection() const;
        /// \ru Вернуть указатель на GraphicsSceneEngine. \en NO TRANSLATION. \~
        GraphicsSceneEngine* graphicsEngine() const;
    public:
        void setEnabledPan(bool enable);
        void setEnabledRotate(bool enable);
        void setEnabledZoom(bool enable);
    protected:
        virtual void resizeGL(int w, int h) override;
        virtual void paintGL() override;
        virtual bool event(QEvent *event) override;
        virtual bool OnEvent(ProcessEvent* event);
    protected:
        virtual void showEvent(QShowEvent* event) override;
        virtual void closeEvent(QCloseEvent* event) override;
        virtual void mousePressEvent(QMouseEvent* event) override;
        virtual void mouseMoveEvent(QMouseEvent* event) override;
        virtual void mouseReleaseEvent(QMouseEvent* event) override;
        virtual void wheelEvent(QWheelEvent* event) override;
    private:
        Q_DISABLE_COPY(QtOpenGLSceneWidget);
        VSN_DECLARE_EX_PRIVATE(QtOpenGLSceneWidget);
        friend class QtRenderSelection;
    };

    QT_FUNC(bool) isExistLicense();
}

#endif // __OPENGLWIDGET_H
