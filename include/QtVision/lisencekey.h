#ifndef __LISENCEKEY_H
#define __LISENCEKEY_H

#include <string>

#ifdef VSN_OS_WIN
  static const std::string strKey = "Golovanov_Svyatoslav..[cnv][mdl][slv][vsn]";
  static const std::string strSignature = "lQsflCy+Kofcr5vr2ZM41aZmUHiRnLYctuw9eofN3ImxvLbmlFCo1HlkVk1A8axQia9ntYIDew4TgfNsATmCxA==";
  //static const std::string strKey = "Vision_Samples_2018.2018020120181231.[vsn]";
  //static const std::string strSignature = "CClZsNpwUfFs+CW7py9eXZTN9bZ4EeJ051CJYHrJfN12oapM6cxUm04/Z235qWTwrOdQnbneUAopsk2K1hZOTA==";
#else
  static const std::string strKey = "Vision_Samples_Linux2018.2018010120181231.[vsn]";
  static const std::string strSignature = "PK3OawC7ZAM/VMlRuJKb28n1yk67NB8gnjFPAT/HucdNpDRMs//5ZpqshH6x2BEwPNVJKkoZ+yyxvD/Sl21nLg==";
#endif

#endif // __LISENCEKEY_H
