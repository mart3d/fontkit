﻿////////////////////////////////////////////////////////////////////////////////
/** 
  \file
  \brief \ru Поток для загрузки Qt ресурсов.
         \en Stream for loading Qt resources . \~
*/
////////////////////////////////////////////////////////////////////////////////

#ifndef __QT_RESSTREAM_H
#define __QT_RESSTREAM_H

#include <QResource>
#include <vsn_resourcestream.h>

namespace VSN_NAMESPACE
{

    /** \brief \ru ВНИМАНИЕ! Этот файл не является частью API Vision. Он необходим для иллюстрации использования
                   ядра Vision с библиотекой Qt и ее классами. Этот заголовочный файл может изменяться от версии
                   к версии без предупреждения или полностью удаляться.
               \en NO TRANSLATION. \~
    */

    //------------------------------------------------------------------------------
    /** \brief \ru Класс QResStream служит для загрузки Qt ресурсов.
               \en class QResStream is needed for loading Qt resources. \~
        \ingroup Vision_OpenGL
    */
    // ---
    class QResStream : public ResourceStream
    {
    public:
        QResStream(const QString & res)
            : ResourceStream()
            , m_resource(res)
        {
            size_t length = m_resource.size();
            Init(m_resource.data(), length);
        }

    private:
        QResource m_resource;
    };
}

#endif //__QT_RESSTREAM_H

