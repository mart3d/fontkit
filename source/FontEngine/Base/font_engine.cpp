#include <font_engine.h>

// FreeType
#include <ft2build.h>
#include FT_FREETYPE_H

#include <mb_rect.h>

using namespace VSN;
using uchar = unsigned char;

static void BuildGlyphs( const WString& text, std::vector<Glyph>& glyphs, const Options& options )
{
    if ( text.Length() == 0 )
        return;

    // FreeType
    FT_Library ft_lib;
    if ( FT_Init_FreeType( &ft_lib ) )
    {
        VSN_ASSERT( 0 );
        abort();
    }
        
    // Load font as face
    FT_Face face;
    if ( FT_New_Face( ft_lib, "../../../resources/fonts/arial.ttf", 0, &face ) )
    {
        if ( FT_New_Face( ft_lib, "../../resources/fonts/arial.ttf", 0, &face ) )
        {
            if ( FT_New_Face( ft_lib, "../resources/fonts/arial.ttf", 0, &face ) )
            {
                if ( FT_New_Face( ft_lib, "resources/fonts/arial.ttf", 0, &face ) )
                {
                    if ( FT_New_Face( ft_lib, "fonts/arial.ttf", 0, &face ) )
                    {
                        VSN_ASSERT( 0 );
                        abort();
                    }
                }
            }
        }
    }

    const bool use_kerning = FT_HAS_KERNING( face );

    // Set size to load glyphs as
    FT_Set_Pixel_Sizes( face, 0, options.font_tex_size );
    glyphs.reserve( text.Length() );
    int carriage_return = 0;
    FT_UInt prev_glyph = 0;

    for ( unsigned i = 0; i < text.Length(); ++i )
    {
        const wchar_t ch = text[i];
                
        if ( ch == wchar_t( '\n' ) )
        {
            carriage_return++;            
            continue;
        }
        
        const FT_UInt glyph_index = FT_Get_Char_Index( face, ch );
        if ( FT_Load_Glyph( face, glyph_index, FT_LOAD_DEFAULT ) )
        {
            VSN_ASSERT( 0 );
            abort();
        }
                
        if ( FT_Render_Glyph( face->glyph, FT_RENDER_MODE_NORMAL ) )
        {
            VSN_ASSERT( 0 );
            abort();
        }

        const int width = face->glyph->bitmap.width;
        const int rows = face->glyph->bitmap.rows;
        
        Glyph glyph;
        glyph.carriage_return = carriage_return;
        glyph.buffer.resize( width * rows );
        const uchar* buffer = static_cast<const uchar*>( face->glyph->bitmap.buffer );

        if ( options.antialiasing )
        {
            for ( size_t j = 0; j < glyph.buffer.size(); j++ )
                glyph.buffer[j] = buffer[j];
        }
        else
        {
            for ( size_t j = 0; j < glyph.buffer.size(); j++ )
                glyph.buffer[j] = buffer[j] > 127 ? 255 : 0;
        }

        glyph.size[0] = face->glyph->bitmap.width;
        glyph.size[1] = face->glyph->bitmap.rows;

        glyph.bearing[0] = face->glyph->bitmap_left;
        glyph.bearing[1] = face->glyph->bitmap_top;

        glyph.advance[0] = face->glyph->advance.x;
        glyph.advance[1] = face->glyph->advance.y;

        if ( use_kerning && prev_glyph )
        {
            FT_Vector delta;
            FT_Get_Kerning( face, prev_glyph, glyph_index, FT_KERNING_DEFAULT, &delta );
            glyph.kerning = delta.x;
        }

        glyphs.emplace_back( glyph );
        carriage_return = 0;
        prev_glyph = glyph_index;
    }

    // Destroy FreeType once we're finished
    FT_Done_Face( face );
    FT_Done_FreeType( ft_lib );
}

static void BuildMonolithGlyph( const std::vector<Glyph>& glyphs, int font_size, Glyph& monolith )
{
    class MonoTex
    {
    public:
        void Append( int x, int y, int w, int h )
        {
            m_tex_box[0] = m_tex_box[0] > x ? x : m_tex_box[0];
            m_tex_box[1] = m_tex_box[1] > y ? y : m_tex_box[1];
            m_tex_box[2] = m_tex_box[2] > x + w ? m_tex_box[2] : x + w;
            m_tex_box[3] = m_tex_box[3] > y + h ? m_tex_box[3] : y + h;
        }

        void BuildTexture()
        {
            m_texture.clear();
            m_texture.resize( Width() * Height() );
        }

        void MapTo( const Glyph& g, int x, int y )
        {            
            for ( int jh = y, height = g.size[1]; jh < y + height; ++jh )
            {
                for ( int iw = x, width = g.size[0]; iw < x + width; ++iw )
                {
                    const int ti = ( ( jh - y ) * width ) + ( iw - x );
                    At( iw, jh ) = g.buffer[ti];
                }
            }
        }

        int Width() const
        {
            return m_tex_box[2] - m_tex_box[0];
        }

        int Height() const
        {
            return m_tex_box[3] - m_tex_box[1];
        }

        uchar& At( int iw, int jh )
        {
            return m_texture[(( jh - m_tex_box[1] ) * Width()) + iw - m_tex_box[0]];
        }

        const uchar& At( int iw, int jh ) const
        {
            return m_texture[(jh * Width()) + iw];
        }

        const std::vector<uchar>& BuildTextureData() const
        {
            return m_texture;
        }

        const int* TexBox() const
        {
            return m_tex_box;
        }

    private:
        int m_tex_box[4] = { INT_MAX, INT_MAX, INT_MIN, INT_MIN };
        std::vector<uchar> m_texture;
    };

    MonoTex monotex;
    int pos_x = 0;
    int pos_y = 0;

    // calc texture size
    for ( size_t i = 0; i < glyphs.size(); ++i )
    {
        const Glyph& g = glyphs[i];
        pos_x += ( g.kerning / 64 );
        for ( int j = 0; j < g.carriage_return; ++j )
        {
            pos_x = 0;
            pos_y += font_size;
        }

        const int x = pos_x + g.bearing[0];
        const int y = pos_y - g.bearing[1];
        const int w = g.size[0];
        const int h = g.size[1];
        
        monotex.Append( x, y, w, h );
        
        pos_x += ( g.advance[0] / 64 );
    }

    // map glyph to monolith texture
    monotex.BuildTexture();
    pos_x = 0;
    pos_y = 0;

    for ( size_t i = 0; i < glyphs.size(); ++i )
    {
        const Glyph& g = glyphs[i];
        pos_x += ( g.kerning / 64 );
        for ( int j = 0; j < g.carriage_return; ++j )
        {
            pos_x = 0;
            pos_y += font_size;
        }
        
        const int x = pos_x + g.bearing[0];
        const int y = pos_y - g.bearing[1];
        monotex.MapTo( g, x, y );

        pos_x += ( g.advance[0] / 64 );
    }

    monolith.buffer = monotex.BuildTextureData();
    monolith.carriage_return = 0;
    monolith.size[0] = monotex.Width();
    monolith.size[1] = monotex.Height();
    monolith.bearing[0] = monotex.TexBox()[0];
    monolith.bearing[1] = -monotex.TexBox()[1];
    monolith.advance[0] = 0;
    monolith.advance[1] = 0;
    monolith.kerning = 0;
}

static void BuildPages( const std::vector<Glyph>& glyphs, Result& result, const Options& options )
{
    result.backrect.clear();
    result.pages.clear();
    result.pages.reserve( glyphs.size() );

    const float scale = float( options.rect_height ) / float( options.font_tex_size );
    float x = 0;
    float y = 0;
    const bool to_transform = options.placement != MbPlacement3D( MbCartPoint3D( 0, 0, 0 ), MbVector3D( 0, 0, 1 ), MbVector3D( 1, 0, 0 ), false );

    const MbMatrix3D matrix_from = options.placement.GetMatrixFrom();
    MbRect backrect;
    
    for ( const auto& g : glyphs )
    {
        x += ( g.kerning / 64 ) * scale;

        Page page;
        
        for ( int j = 0; j < g.carriage_return; ++j )
        {
            x = 0;
            y -= options.font_tex_size * scale;
        }

        const float xpos = x + g.bearing[0] * scale;
        const float ypos = y + ( g.bearing[1] - g.size[1] ) * scale;

        // Now advance cursors for next glyph (note that advance is number of 1/64 pixels)
        x += ( g.advance[0] / 64 ) * scale; // Bitshift by 6 to get value in pixels (2^6 = 64 (divide amount of 1/64th pixels by 64 to get amount of pixels))
        
        const float w = g.size[0] * scale;
        const float h = g.size[1] * scale;
        
        if ( options.build_backrect )
        {
            backrect.Include( xpos, ypos );
            backrect.Include( xpos + w, ypos + h );
        }
        
        page.vertices.resize( 12 );
        
        if ( to_transform )        
        {            
            MbCartPoint3D v[4];
            v[0] = MbCartPoint3D( MbCartPoint3D( xpos, ypos + h, 0 ), matrix_from );
            v[1] = MbCartPoint3D( MbCartPoint3D( xpos, ypos, 0 ), matrix_from );
            v[2] = MbCartPoint3D( MbCartPoint3D( xpos + w, ypos, 0 ), matrix_from );
            v[3] = MbCartPoint3D( MbCartPoint3D( xpos + w, ypos + h, 0 ), matrix_from );
            
            for ( size_t i = 0; i < 4; ++i )
            {
                page.vertices[3*i]   = float( v[i].x );
                page.vertices[3*i+1] = float( v[i].y );
                page.vertices[3*i+2] = float( v[i].z );
            }
        }
        else
        {
            page.vertices[0] = xpos;   page.vertices[1]  = ypos+h; page.vertices[2]  = 0;
            page.vertices[3] = xpos;   page.vertices[4]  = ypos;   page.vertices[5]  = 0;
            page.vertices[6] = xpos+w; page.vertices[7]  = ypos;   page.vertices[8]  = 0;
            page.vertices[9] = xpos+w; page.vertices[10] = ypos+h; page.vertices[11] = 0;
        }
                        
        // BGRA texture
        {
            // font_data to texture_data
            page.texture_size[0] = g.size[0];
            page.texture_size[1] = g.size[1];
        
            page.texture_data.resize( 4 * g.size[0] * g.size[1] );
        
            for ( int ih = 0, height = g.size[1]; ih < height; ++ih )
            {
                for ( int iw = 0, width = g.size[0]; iw < width; ++iw )
                {
                    const int ti = 4 * ( ( ih * width ) + iw );
        
                    const uint alpha = uint( g.buffer[ih*width + iw] ); // options.font_color_rgba[3] );
        
                    page.texture_data[ti ] = options.font_color_rgba[2];
                    page.texture_data[ti + 1] = options.font_color_rgba[1];
                    page.texture_data[ti + 2] = options.font_color_rgba[0];
                    page.texture_data[ti + 3] = uchar( alpha );
                }
            }
        }
        
        result.pages.emplace_back( page );
    }

    if ( options.build_backrect )
    {
        result.backrect.resize( 12 );
        if ( to_transform )        
        {
            MbCartPoint3D v[4];
            v[0] = MbCartPoint3D( MbCartPoint3D( backrect.GetXMin(), backrect.GetYMax(), 0 ), matrix_from );
            v[1] = MbCartPoint3D( MbCartPoint3D( backrect.GetXMin(), backrect.GetYMin(), 0 ), matrix_from );
            v[2] = MbCartPoint3D( MbCartPoint3D( backrect.GetXMax(), backrect.GetYMin(), 0 ), matrix_from );
            v[3] = MbCartPoint3D( MbCartPoint3D( backrect.GetXMax(), backrect.GetYMax(), 0 ), matrix_from );
            
            for ( size_t i = 0; i < 4; ++i )
            {
                result.backrect[3*i]   = float( v[i].x );
                result.backrect[3*i+1] = float( v[i].y );
                result.backrect[3*i+2] = float( v[i].z );
            }
        }
        else
        {            
            result.backrect[0] = float( backrect.GetXMin() ); result.backrect[1]  = float( backrect.GetYMax() ); result.backrect[2]  = 0;
            result.backrect[3] = float( backrect.GetXMin() ); result.backrect[4]  = float( backrect.GetYMin() ); result.backrect[5]  = 0;
            result.backrect[6] = float( backrect.GetXMax() ); result.backrect[7]  = float( backrect.GetYMin() ); result.backrect[8]  = 0;
            result.backrect[9] = float( backrect.GetXMax() ); result.backrect[10] = float( backrect.GetYMax() ); result.backrect[11] = 0;
        }
    }
}

FONT_ENGINE_API void Build( const VSN::WString& text, Result& result, const Options* options )
{
    Options opt;
    if ( options )
        opt = *options;

    std::vector<Glyph> glyphs;
    BuildGlyphs( text, glyphs, opt );

    if ( opt.monolithic )
    {
        Glyph monolith;
        BuildMonolithGlyph( glyphs, opt.font_tex_size, monolith );
        glyphs.clear();
        glyphs.emplace_back( monolith );
    }
        
    BuildPages( glyphs, result, opt );
}