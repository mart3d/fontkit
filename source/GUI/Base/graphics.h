#pragma once

#include "std_afx.h"
#include <qt_openglwidget.h>
#include <font_engine.h>

class Dock;

class Graphics : public QtOpenGLSceneWidget
{
public:
    Graphics();
        
    void SetDock( const Dock* dock );

    void Clear();
    void LoadFont( const std::vector<Page>& pages, const RenderOptions* render_options );
    void DrawFont( const std::vector<Page>& pages, const RenderOptions* render_options );

private:
    virtual void initializeGL();

private:
    const Dock* m_dock;
    SceneSegment* m_root_segment;
    std::vector<SceneSegment*> m_segments;
};