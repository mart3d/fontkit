#pragma once

#include <font_engine.h>
#include <mb_cube.h>
  
VSN_USE_NAMESPACE
  
class FontGeometry : public IFontGeometry
{
public:
    FontGeometry( const Page& page );
    ~FontGeometry() override;

    virtual const MbCube& GetBoundingBox() override;

private:
    virtual void OpenGLDraw( const RenderState& state ) override;

    void LoadPage( const Page& page );

private:
    MbCube m_bbox;
};