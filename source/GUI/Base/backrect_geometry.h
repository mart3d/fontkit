#pragma once

#include "vsn_meshgeometry.h"
#include <mb_cube.h>
  
VSN_USE_NAMESPACE
  
class BackRectGeometry : public MeshGeometry
{
public:
    BackRectGeometry( const std::vector<float>& vertices, Node* parent = nullptr );
    virtual ~BackRectGeometry();

    virtual const MbCube& GetBoundingBox() override;

private:
    virtual void OpenGLDraw( const RenderState& state ) override;

    void LoadRect( const std::vector<float>& vertices );

private:
    MbCube m_bbox;
};