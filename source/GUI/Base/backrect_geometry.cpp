﻿#include "backrect_geometry.h"

#include "vsn_openglcontextinterface.h"
#include "vsn_openglfunctionlistinterface.h"
#include "vsn_openglextrafunctionlistinterface.h"

#include <mb_rect.h>

BackRectGeometry::BackRectGeometry( const std::vector<float>& vertices, Node* parent )
    : MeshGeometry( parent)
{
    SetObjectName( String( "BackRectGeometry" ) );
    LoadRect( vertices );
}

BackRectGeometry::~BackRectGeometry()
{
}

const MbCube& BackRectGeometry::GetBoundingBox()
{
    return m_bbox;
}

void BackRectGeometry::OpenGLDraw( const RenderState& state )
{
    OpenGLContextInterface* gl_context = OpenGLContextInterface::GetCurrentContext();
    VSN_ASSERT( gl_context );
    const bool prev_light = gl_context->oglAllowLighting( false );
    
    OpenGLFunctionListInterface* ogl_interface = gl_context->GetFunctionList();

    ogl_interface->glEnable( GL_POLYGON_OFFSET_FILL );
    ogl_interface->glPolygonOffset( 1, 1 );

    if ( ! MeshGeometry::IsEmpty() )
        MeshGeometry::OpenGLDraw( state );
        
    ogl_interface->glPolygonOffset( 0, 0 );
    ogl_interface->glDisable( GL_POLYGON_OFFSET_FILL );

    gl_context->oglAllowLighting( prev_light );
}

void BackRectGeometry::LoadRect( const std::vector<float>& vertices )
{
    VSN_ASSERT( MeshGeometry::IsEmpty() && vertices.size() == 12 );
    // добавить массив данных и сетки
    MeshGeometry::AddVertices( vertices );
    
    std::list<uint> index( 6 );
    index.push_back(0); index.push_back(1); index.push_back(2);
    index.push_back(0); index.push_back(2); index.push_back(3);
    MeshGeometry::AddTriangles( Triangles, nullptr, index );
    
    MeshGeometry::CloseMesh();

    for ( int i = 0; i < 4; ++i )
        m_bbox.Include( vertices[3*i], vertices[3*i+1], vertices[3*i+2] );
}