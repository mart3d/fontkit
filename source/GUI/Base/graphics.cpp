#include "graphics.h"
#include "backrect_geometry.h"
#include "dock.h"
#include "font_geometry.h"

#include <vsn_texture2d.h>

Graphics::Graphics()
    : m_dock( nullptr )
    , m_root_segment( sceneContent()->GetRootSegment() )
{
}

void Graphics::SetDock( const Dock* dock )
{
    m_dock = dock;
}

void Graphics::Clear()
{
    if ( ! m_root_segment )
        return;
    
    for ( const auto& s : m_segments )
    {
        const bool ok_vo = s->RemoveViewObject();
        const bool ok_rs = m_root_segment->RemoveSegment( s );
        VSN_ASSERT( ok_vo && ok_rs );
    }

    m_segments.clear();
}

void Graphics::LoadFont( const std::vector<Page>& pages, const RenderOptions* render_options )
{
    RenderOptions rend_opt;
    if ( render_options )
        rend_opt = *render_options;

    if ( m_root_segment )
    {
        if ( ! pages.empty() )
        {
            if ( ! rend_opt.backrect_vertices.empty() )
            {
                GeometryRep* rect_rep = new GeometryRep( new BackRectGeometry( rend_opt.backrect_vertices ) );
                SceneSegment* scene_segment = new SceneSegment( rect_rep );
                m_root_segment->AddSegment( scene_segment );
                m_segments.emplace_back( scene_segment );
                
                std::set<Material*> setMaterial = scene_segment->GetMaterials();
                (*setMaterial.begin())->SetDiffuse( Color( int( rend_opt.backrect_color[0] ), int( rend_opt.backrect_color[1] ), int( rend_opt.backrect_color[2] ) ) );
            }
            
            for ( const auto& page : pages )
            {
                Image texture_image;
                texture_image.Init( page.texture_size[0], page.texture_size[1], VSN::Image::Format::UINT32, page.texture_data.data() );
            
                GeometryRep* rect_rep = new GeometryRep( new FontGeometry( page ) );
                SceneSegment* scene_segment = new SceneSegment( rect_rep, m_root_segment );
                m_root_segment->AddSegment( scene_segment );
                m_segments.emplace_back( scene_segment );
            
                std::set<Material*> setMaterial = scene_segment->GetMaterials();
                Texture2D* pTexture2D = new Texture2D( texture_image );

                if ( rend_opt.tex_filter == RenderOptions::TF_Nearest )
                    pTexture2D->SetFilters(TextureFilter::Nearest, TextureFilter::Nearest);
                else if ( rend_opt.tex_filter == RenderOptions::TF_Linear )
                    pTexture2D->SetFilters(TextureFilter::Linear, TextureFilter::Linear);
                else
                {
                    VSN_ASSERT( 0 );
                }
                
                (*setMaterial.begin())->SetTexture(pTexture2D);
            }
        }
    }
        
    update();
}

void Graphics::DrawFont( const std::vector<Page>& pages, const RenderOptions* render_options )
{
    RenderOptions rend_opt;
    if ( render_options )
        rend_opt = *render_options;

    SceneSegment* static_font_segment = nullptr;

    if ( m_root_segment )
    {
        if ( ! pages.empty() )
        {
            static_font_segment = new SceneSegment( m_root_segment );
            m_root_segment->AddSegment( static_font_segment );
            //MbMatrix3D relative_matrix;
            //MbVector3D vec_move( 0, -15, 0 );
            //relative_matrix.Move( vec_move );
            //static_font_segment->CreateRelativeMatrix( relative_matrix );

            if ( ! rend_opt.backrect_vertices.empty() )
            {
                GeometryRep* rect_rep = new GeometryRep( new BackRectGeometry( rend_opt.backrect_vertices ) );
                SceneSegment* scene_segment = new SceneSegment( rect_rep );
                static_font_segment->AddSegment( scene_segment );
                
                std::set<Material*> setMaterial = scene_segment->GetMaterials();
                (*setMaterial.begin())->SetDiffuse( Color( int( rend_opt.backrect_color[0] ), int( rend_opt.backrect_color[1] ), int( rend_opt.backrect_color[2] ) ) );
            }
            
            for ( const auto& page : pages )
            {
                Image texture_image;
                texture_image.Init( page.texture_size[0], page.texture_size[1], VSN::Image::Format::UINT32, page.texture_data.data() );
            
                GeometryRep* rect_rep = new GeometryRep( new FontGeometry( page ) );
                SceneSegment* scene_segment = new SceneSegment( rect_rep );
                static_font_segment->AddSegment( scene_segment );
            
                std::set<Material*> setMaterial = scene_segment->GetMaterials();
                Texture2D* pTexture2D = new Texture2D( texture_image );

                if ( rend_opt.tex_filter == RenderOptions::TF_Nearest )
                    pTexture2D->SetFilters(TextureFilter::Nearest, TextureFilter::Nearest);
                else if ( rend_opt.tex_filter == RenderOptions::TF_Linear )
                    pTexture2D->SetFilters(TextureFilter::Linear, TextureFilter::Linear);
                else
                {
                    VSN_ASSERT( 0 );
                }

                (*setMaterial.begin())->SetTexture(pTexture2D);
            }
        }
    }

    paintGL();
    repaint();
    
    if ( static_font_segment )
    {
        const bool ok_rs = m_root_segment->RemoveSegment( static_font_segment );
        VSN_ASSERT( ok_rs );
    }
}

void Graphics::initializeGL()
{
    QtOpenGLSceneWidget::initializeGL();
    setEnabledPan(true);
    setEnabledRotate(true);
    setEnabledZoom(true);

    mainLight()->SetDoubleSided(true);
    viewport()->SetBackgroundColour(Color(74, 74, 74));

    // set model
    {
        m_dock->UpdateFont();
        //viewport()->GetCamera()->Init( MbCartPoint3D( 37, 3, 95 ), MbCartPoint3D( 37, 3, 0 ), MbVector3D( 0, 1, 0 ) );
        viewport()->ZoomToFit(sceneContent()->GetBoundingBox());
    }    

    //const OpenGLContextInterface* gl_context = OpenGLContextInterface::GetCurrentContext();
    //if ( ! gl_context )
    //    abort();
    //
    //gl_context->GetFunctionList();
}