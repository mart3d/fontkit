#include "dock.h"
#include "color_widget.h"
#include "graphics.h"

#include <QGroupBox>
#include <QCheckBox>
#include <QLabel>
#include <QLineEdit>
#include <QTextEdit>
#include <QBoxLayout>
#include <QComboBox>
#include <QPushButton>
#include <QRadioButton>

FontItem::FontItem( const QString& title )
    : QGroupBox( title )
{
    QVBoxLayout* main_vbox = new QVBoxLayout;
    setLayout( main_vbox );

    // text
    {
        m_text_te = new QTextEdit( title );
        main_vbox->addWidget( m_text_te );
        connect( m_text_te, &QTextEdit::textChanged, this, &FontItem::textChanged );

        m_font_family_lw = new QComboBox;
        main_vbox->addWidget( m_font_family_lw );
    }

    //// modes
    //{
    //    QHBoxLayout* mode_hbox = new QHBoxLayout;
    //    main_vbox->addLayout( mode_hbox );
    //
    //    m_bold_mode_pb = new QPushButton( "Bold" );
    //    m_bold_mode_pb->setCheckable( true );
    //    mode_hbox->addWidget( m_bold_mode_pb );
    //
    //    m_italic_mode_pb = new QPushButton( "Italic" );
    //    m_italic_mode_pb->setCheckable( true );
    //    mode_hbox->addWidget( m_italic_mode_pb );
    //
    //    m_underline_mode_pb = new QPushButton( "Underline" );
    //    m_underline_mode_pb->setCheckable( true );
    //    mode_hbox->addWidget( m_underline_mode_pb );
    //
    //    m_strikethrought_mode_pb = new QPushButton( "Strikethrought" );
    //    m_strikethrought_mode_pb->setCheckable( true );
    //    mode_hbox->addWidget( m_strikethrought_mode_pb );
    //}
    
    {
        QVBoxLayout* aln_vbox = new QVBoxLayout;
        main_vbox->addLayout( aln_vbox );

        m_tex_antialiasing_cb = new QCheckBox( "Font Texture AntiAliasing" );
        aln_vbox->addWidget( m_tex_antialiasing_cb );        
        m_tex_antialiasing_cb->setChecked( true );
        connect( m_tex_antialiasing_cb, &QCheckBox::toggled, this, &FontItem::enableTextureAntialiasing );

        m_linear_tex_filter_rb = new QRadioButton( "Texture Linear" );
        aln_vbox->addWidget( m_linear_tex_filter_rb );
        m_linear_tex_filter_rb->setChecked( true );
        connect( m_linear_tex_filter_rb, &QRadioButton::toggled, this, &FontItem::textureFilterChanged );

        m_nearest_tex_filter_rb = new QRadioButton( "Texture Nearest" );
        aln_vbox->addWidget( m_nearest_tex_filter_rb );
        connect( m_nearest_tex_filter_rb, &QRadioButton::toggled, this, &FontItem::textureFilterChanged );
    }

    // size color quality
    {
        QHBoxLayout* font_sq_hbox = new QHBoxLayout;
        main_vbox->addLayout( font_sq_hbox );

        QLabel* font_size_lbl = new QLabel( "Size:" );
        font_sq_hbox->addWidget( font_size_lbl );        
        m_font_size_le = new QLineEdit( "12" );
        font_sq_hbox->addWidget( m_font_size_le );
        connect( m_font_size_le, &QLineEdit::editingFinished, this, &FontItem::fontSizeChanged );

        m_font_color = new ColorWidget( "Color:" );
        font_sq_hbox->addWidget( m_font_color );
        connect( m_font_color, &ColorWidget::colorChanged, this, &FontItem::textColorChanged );

        QLabel* font_quality_lbl = new QLabel( "Quality:" );
        font_sq_hbox->addWidget( font_quality_lbl );
        m_font_quality_le = new QLineEdit( "50" );
        font_sq_hbox->addWidget( m_font_quality_le );
        connect( m_font_quality_le, &QLineEdit::editingFinished, this, &FontItem::fontQualityChanged );
    }

    // back rect
    {
        m_backgroud_rect_cb = new QCheckBox( "Show background rectangle" );
        main_vbox->addWidget( m_backgroud_rect_cb );
        connect( m_backgroud_rect_cb, &QCheckBox::toggled, this, &FontItem::backRectToggled );

        m_backgroud_rect_color = new ColorWidget( "Background rectangle color:" );
        m_backgroud_rect_color->setColor( 255, 255, 255 );
        main_vbox->addWidget( m_backgroud_rect_color );
        connect( m_backgroud_rect_color, &ColorWidget::colorChanged, this, &FontItem::backRectColorChanged );
    }    
}

QString FontItem::Text() const
{
    return m_text_te->toPlainText();
}

void FontItem::FontColor( uchar color[4] ) const
{
    m_font_color->color( color );
}

bool FontItem::IsTextureAntialiasingEnabled() const
{
    return m_tex_antialiasing_cb->isChecked();
}

bool FontItem::IsTextureLinearFilterEnabled() const
{
    return m_linear_tex_filter_rb->isChecked();
}

bool FontItem::IsTextureNearestFilterEnabled() const
{
    return m_nearest_tex_filter_rb->isChecked();
}

int FontItem::FontSize() const
{
    bool ok_height;
    int font_height = m_font_size_le->text().toInt( &ok_height );
    if ( ! ok_height )
        font_height = 1;

    return font_height;
}

int FontItem::FontQuality() const
{
    bool ok_quality;
    int font_quality = m_font_quality_le->text().toInt( &ok_quality );
    if ( ! ok_quality )
        font_quality = 1;

    return font_quality;
}

bool FontItem::IsBackRectEnabled() const
{
    return m_backgroud_rect_cb->isChecked();
}

void FontItem::BackRectColor( uchar color[4] ) const
{
    m_backgroud_rect_color->color( color );
}

void FontItem::SetText( const QString& text ) const
{
    m_text_te->setText( text );
}

void FontItem::SetFontColor( const uchar color[4] ) const
{
    m_font_color->setColor( color[0], color[1], color[2], color[3] );
}

void FontItem::SetTextureAntialiasing( bool enable ) const
{
    m_tex_antialiasing_cb->setChecked( enable );
}

void FontItem::SetTextureLinearFilter() const
{
    m_linear_tex_filter_rb->setChecked( true );
}

void FontItem::SetTextureNearestFilter() const
{
    m_nearest_tex_filter_rb->setChecked( true );
}

void FontItem::SetFontSize( int size ) const
{
    m_font_size_le->setText( QString::number( size ) );
}

void FontItem::SetFontQuality( int quality ) const
{
    m_font_quality_le->setText( QString::number( quality ) );
}

void FontItem::EnableBackRect( bool enable ) const
{
    m_backgroud_rect_cb->setChecked( enable );
}

void FontItem::SetBackRectColor( const uchar color[4] ) const
{
    m_backgroud_rect_color->setColor( color[0], color[1], color[2], color[3] );
}

Dock::Dock( Graphics* graphics )
    : m_graphics( graphics )
{
    QVBoxLayout* main_vbox = new QVBoxLayout;
    setLayout( main_vbox );

    m_mono_font = new FontItem( "Monolith Text" );
    main_vbox->addWidget( m_mono_font, 4 );
    main_vbox->addStretch( 1 );

    connect( m_mono_font, &FontItem::textChanged, this, &Dock::UpdateFont );
    connect( m_mono_font, &FontItem::textColorChanged, this, &Dock::UpdateFont );
    connect( m_mono_font, &FontItem::enableTextureAntialiasing, this, &Dock::UpdateFont );
    connect( m_mono_font, &FontItem::textureFilterChanged, this, &Dock::UpdateFont );
    connect( m_mono_font, &FontItem::fontSizeChanged, this, &Dock::UpdateFont );
    connect( m_mono_font, &FontItem::fontQualityChanged, this, &Dock::UpdateFont );
    connect( m_mono_font, &FontItem::backRectToggled, this, &Dock::UpdateFont );
    connect( m_mono_font, &FontItem::backRectColorChanged, this, &Dock::UpdateFont );

    m_segm_font = new FontItem( "Segment Text" );
    main_vbox->addWidget( m_segm_font, 3 );
    main_vbox->addStretch( 1 );

    m_segm_font->EnableBackRect( true );
    m_segm_font->SetTextureAntialiasing( false );
    m_segm_font->SetTextureNearestFilter();
    const uchar backrect_color[4] = { 0, 0, 255, 0 };
    m_segm_font->SetBackRectColor( backrect_color );
    const uchar font_color[4] = { 255, 255, 255, 0 };
    m_segm_font->SetFontColor( font_color );

    connect( m_segm_font, &FontItem::textChanged, this, &Dock::UpdateFont );
    connect( m_segm_font, &FontItem::textColorChanged, this, &Dock::UpdateFont );
    connect( m_segm_font, &FontItem::enableTextureAntialiasing, this, &Dock::UpdateFont );
    connect( m_segm_font, &FontItem::textureFilterChanged, this, &Dock::UpdateFont );
    connect( m_segm_font, &FontItem::fontSizeChanged, this, &Dock::UpdateFont );
    connect( m_segm_font, &FontItem::fontQualityChanged, this, &Dock::UpdateFont );
    connect( m_segm_font, &FontItem::backRectToggled, this, &Dock::UpdateFont );
    connect( m_segm_font, &FontItem::backRectColorChanged, this, &Dock::UpdateFont );
}

void Dock::UpdateFont() const
{
    m_graphics->Clear();
    
    LoadFontItem( m_mono_font, false );
    LoadFontItem( m_segm_font, true );
}

void Dock::LoadFontItem( const FontItem* font_item, bool segment_draw ) const
{
    Options options;
    options.font_tex_size = font_item->FontQuality();
    options.rect_height = font_item->FontSize();
    font_item->FontColor( options.font_color_rgba );
    options.antialiasing = font_item->IsTextureAntialiasingEnabled();

    if ( segment_draw )
    {
        options.monolithic = false;
        options.placement.Init( MbCartPoint3D( 0, -15, 0 ), MbVector3D( 0, 0, 1 ), MbVector3D( 1, 0, 0 ), false );
    }

    options.build_backrect = font_item->IsBackRectEnabled();
    WString vsn_string( font_item->Text().toStdWString().c_str() );
    Result result;

    Build( vsn_string, result, &options );
        
    RenderOptions render_options;
    if ( options.build_backrect )
    {
        font_item->BackRectColor( render_options.backrect_color );
        render_options.backrect_vertices = result.backrect;
    }
    
    if ( font_item->IsTextureLinearFilterEnabled() )
        render_options.tex_filter = RenderOptions::TF_Linear;
    else if ( font_item->IsTextureNearestFilterEnabled() )
        render_options.tex_filter = RenderOptions::TF_Nearest;
    else
        abort();

    m_graphics->LoadFont( result.pages, &render_options );
}
