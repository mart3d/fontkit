﻿#include "font_geometry.h"

#include "vsn_openglcontextinterface.h"
#include "vsn_openglfunctionlistinterface.h"
#include "vsn_openglextrafunctionlistinterface.h"

FontGeometry::FontGeometry( const Page& page )
{
    SetObjectName( String( "RectangleGeometry" ) );
    LoadPage( page );

    OpenGLContextInterface* gl_context = OpenGLContextInterface::GetCurrentContext();
    OpenGLFunctionListInterface* ogl_interface = gl_context->GetFunctionList();
    ogl_interface->glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );
}

FontGeometry::~FontGeometry()
{
}

const MbCube& FontGeometry::GetBoundingBox()
{
    return m_bbox;
}

void FontGeometry::OpenGLDraw( const RenderState& state )
{
    OpenGLContextInterface* gl_context = OpenGLContextInterface::GetCurrentContext();
    VSN_ASSERT( gl_context );
    const bool prev_light = gl_context->oglAllowLighting( false );
    
    OpenGLFunctionListInterface* ogl_interface = gl_context->GetFunctionList();

    ogl_interface->glDepthMask( GL_FALSE );
    ogl_interface->glEnable( GL_BLEND );
    ogl_interface->glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

    if ( ! MeshGeometry::IsEmpty() )
        MeshGeometry::OpenGLDraw( state );

    ogl_interface->glDepthMask( GL_TRUE );
    ogl_interface->glDisable( GL_BLEND );

    gl_context->oglAllowLighting( prev_light );
}

void FontGeometry::LoadPage( const Page& page )
{    
    VSN_ASSERT( MeshGeometry::IsEmpty() );
    VSN_ASSERT( page.vertices.size() == 12 );    
        
    std::vector<float> texels( 8 );
    {
        texels[0] = 0; texels[1] = 0;
        texels[2] = 0; texels[3] = 1;
        texels[4] = 1; texels[5] = 1;
        texels[6] = 1; texels[7] = 0;
    }
    
    std::vector<float> normals;
    normals.assign( 12, 0 );

    // добавить массив данных и сетки
    MeshGeometry::AddVertices( page.vertices );
    MeshGeometry::AddNormals( normals );
    MeshGeometry::AddTexels( texels );
    
    // wireframe unused
    //// добавить первую точку прямоугольника для проволочного тела
    //vertices.push_back(-logX); vertices.push_back(-logY); vertices.push_back(0.0f);
    //WireframeGeometry::AddPoligonGroup(vertices);
    
    std::list<uint> index{ 0, 1, 2, 0, 2, 3 };
    MeshGeometry::AddTriangles( Triangles, nullptr, index );
    
    MeshGeometry::CloseMesh();

    for ( int i = 0; i < 4; ++i )
        m_bbox.Include( page.vertices[3*i], page.vertices[3*i+1], page.vertices[3*i+2] );
}