#pragma once

#include "std_afx.h"

#include <QGroupBox>

class Graphics;
class ColorWidget;
class QCheckBox;
class QLineEdit;
class QTextEdit;
class QComboBox;
class QPushButton;
class QRadioButton;

class FontItem : public QGroupBox
{
    Q_OBJECT

public:
    FontItem( const QString& title );

    QString Text() const;
    void FontColor( uchar color[4] ) const;
    bool IsTextureAntialiasingEnabled() const;
    bool IsTextureLinearFilterEnabled() const;
    bool IsTextureNearestFilterEnabled() const;
    int FontSize() const;
    int FontQuality() const;
    bool IsBackRectEnabled() const;
    void BackRectColor( uchar color[4] ) const;

    void SetText( const QString& text ) const;
    void SetFontColor( const uchar color[4] ) const;
    void SetTextureAntialiasing( bool enable ) const;
    void SetTextureLinearFilter() const;
    void SetTextureNearestFilter() const;
    void SetFontSize( int size ) const;
    void SetFontQuality( int quality ) const;
    void EnableBackRect( bool enable ) const;
    void SetBackRectColor( const uchar color[4] ) const;

signals:
    void textChanged() const;
    void enableTextureAntialiasing() const;
    void textureFilterChanged() const;
    void fontQualityChanged() const;
    void textColorChanged() const;
    void fontSizeChanged() const;
    void backRectToggled() const;
    void backRectColorChanged() const;

private:
    QTextEdit*   m_text_te;
    QComboBox*   m_font_family_lw;

    //QPushButton* m_bold_mode_pb;
    //QPushButton* m_italic_mode_pb;
    //QPushButton* m_underline_mode_pb;
    //QPushButton* m_strikethrought_mode_pb;

    QCheckBox*    m_tex_antialiasing_cb;
    QRadioButton* m_linear_tex_filter_rb;
    QRadioButton* m_nearest_tex_filter_rb;

    QLineEdit*   m_font_quality_le;
    ColorWidget* m_font_color;
    QLineEdit*   m_font_size_le;
    QCheckBox*   m_backgroud_rect_cb;
    ColorWidget* m_backgroud_rect_color;
};

class Dock : public QWidget
{
public:
    Dock( Graphics* graphics );

    void UpdateFont() const;

private:
    void LoadFontItem( const FontItem* font_item, bool static_draw ) const;

private:
    Graphics* m_graphics;

    FontItem* m_mono_font;    
    FontItem* m_segm_font;
};