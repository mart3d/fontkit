#include <vsn_application.h>

#include <QBoxLayout>
#include <qt_openglwidget.h>
#include "mainwindow.h"
#include "graphics.h"
#include "dock.h"

MainWindow::MainWindow()
{
    QHBoxLayout* hbox = new QHBoxLayout;
    hbox->setMargin( 0 );
    hbox->setSpacing( 0 );
    setLayout( hbox );

    QSurfaceFormat format;
    format.setDepthBufferSize(24);
    format.setStencilBufferSize(8);
    format.setSamples(4);
    format.setSwapInterval(0);
    QSurfaceFormat::setDefaultFormat(format);

    Graphics* graphics = new Graphics;
    hbox->addWidget( graphics, 3 );
    
    Dock* dock = new Dock( graphics );
    hbox->addWidget( dock, 1 );

    graphics->SetDock( dock );
}

int main( int argc, char** argv )
{
#if defined(WIN32) /*&& defined(HUNT_FOR_MEM_LEAKS)*/
    _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

    if ( ! isExistLicense() )
        return 1;

    Application vapp;
    QApplication app(argc, argv);
    app.setApplicationName("Vision Sample");
    app.setOrganizationName("C3DLabs");

    MainWindow mainwindow;

    //mainwindow.move(QPoint(200, 200));
    //QRect geom = QApplication::desktop()->availableGeometry();
    //mainwindow.resize(2 * geom.width() / 3, 2 * geom.height() / 3);

    mainwindow.showMaximized();

    return app.exec();
}