﻿#include "color_widget.h"

#include <QLabel>
#include <QBoxLayout>
#include <QPushButton>
#include <QColorDialog>

ColorWidget::ColorWidget( const QString& title )
    : m_color{ 0, 0, 0, 0 }
{
    m_value_lbl = new QLabel( title );
    
    QHBoxLayout* hbox = new QHBoxLayout;
    setLayout( hbox );

    hbox->setMargin( 0 );
    hbox->addWidget( m_value_lbl );
    
    m_palette_pbtn = new QPushButton;
    hbox->addWidget( m_palette_pbtn );
    connect( m_palette_pbtn, &QPushButton::clicked, this, &ColorWidget::palette );

    hbox->addStretch( 1 );

    updatePaletteButtonColor();
}

void ColorWidget::color( uchar rgba[4] ) const
{
    rgba[0] = m_color[0];
    rgba[1] = m_color[1];
    rgba[2] = m_color[2];
    rgba[3] = m_color[3];
}

void ColorWidget::setColor( uchar r, uchar g, uchar b, uchar a )
{
    m_color[0] = r;
    m_color[1] = g;
    m_color[2] = b;
    m_color[3] = a;

    updatePaletteButtonColor();
    emit colorChanged();
}

void ColorWidget::palette()
{
    QColor color = QColorDialog::getColor( QColor( m_color[0], m_color[1], m_color[2], m_color[3] ), this );
    if ( color.isValid() )
    {
        m_color[0] = color.red();
        m_color[1] = color.green();
        m_color[2] = color.blue();
        m_color[3] = color.alpha();

        updatePaletteButtonColor();
        emit colorChanged();
    }
}

void ColorWidget::updatePaletteButtonColor() const
{
    QString str = "background-color: rgb(";
    str += QString( QString::number( m_color[0] ) );
    str += QString( ", " );
    str += QString( QString::number( m_color[1] ) );
    str += QString( ", " );
    str += QString( QString::number( m_color[2] ) );
    str += QString( ")" );

    m_palette_pbtn->setStyleSheet( str );
    m_palette_pbtn->update();
}