﻿#pragma once

#include <QWidget>

class QLabel;
class QPushButton;

class ColorWidget : public QWidget
{
    Q_OBJECT
public:
    ColorWidget( const QString& title );
    void color( uchar rgba[4] ) const;
    void setColor( uchar r, uchar g, uchar b, uchar a = 0 );

signals:
    void colorChanged() const;

private slots:
    void palette();

private:
    void updatePaletteButtonColor() const;

private:
    QLabel* m_value_lbl;
    QPushButton* m_palette_pbtn;
    uchar m_color[4];
};